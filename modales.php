<!--/ Form /-->
<div class="box-collapse">
    <div class="title-box-d">
        <h3 class="title-d">Registro para clientes</h3>
    </div>

    <span class="close-box-collapse right-boxed ion-ios-close"></span>
    <div class="box-collapse-wrap form">
        <form class="form-a" action="panel/controlador/clientes.php" id="formulario_registro_cliente" method="POST">
            <div clas="col-md-12 mb-2 text-center" style="margin-bottom:0px !important;display:none;text-align:center !important;" id="cont_msj">
              <span id="txt_msj" style="color:red;text-align:center;"></span>
              <br><br>
            </div>
            <div class="row">

                <div class="col-md-3 mb-2" style="margin-bottom:0px !important;">
                    <select class="form-control form-control-lg form-control-a" name="cod1" required id="tipo_per">
                        <option selected="">V</option>
                        <option>E</option>
                        <option>J</option>
                    </select>
                </div>

                <div class="col-md-9 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg form-control-a" placeholder="Cédula/Rif" name="cod2" required minlength="3">
                    </div>
                </div>

                <div class="col-md-12 mb-2" style="margin-bottom:0px !important;display:none;" id="conte_razon">
                    <div class="form-group">
                        <label for="Correo">Razón social o empresa</label>
                        <input type="text" class="form-control form-control-lg form-control-a" placeholder="Razón social o empresa"  name="emp">
                    </div>
                </div>

                <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group">
                        <label for="Nombres">Nombres</label>
                        <input type="text" class="form-control form-control-lg form-control-a" placeholder="Nombres" required minlength="3" name="nom">
                    </div>
                </div>

                <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group">
                        <label for="Apellidos">Apellidos</label>
                        <input type="text" class="form-control form-control-lg form-control-a" placeholder="Apellidos" required minlength="3" name="ape">
                    </div>
                </div>
                <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                    <label for="Teléfono">Teléfono de contacto</label>
                  <div class="row">
                    <div class="form-group col-md-5">
                        <select name="tlf1" class="form-control" required>
                          <option value="">--</option>
                          <option>0412</option>
                          <option>0424</option>
                          <option>0414</option>
                          <option>0416</option>
                          <option>0426</option>
                        </select>
                    </div>
                    <div class="form-group col-md-7">
                        <input type="text" class="form-control form-control-lg form-control-a" placeholder="Teléfono" name="tlf" required maxlength="7" minlength="7">
                    </div>
                  </div>
                </div>
                
                <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group">
                        <label for="Correo">Correo</label>
                        <input type="email" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo" required minlength="5" name="cor">
                    </div>
                </div>

                <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group">
                        <label for="Contraseña">Nueva contraseña</label>
                        <input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" required minlength="5" name="pas">
                    </div>
                </div>

                <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group">
                        <label for="Contraseña">Confirmar contraseña</label>
                        <input type="password" class="form-control form-control-lg form-control-a" placeholder="Confirma tu contraseña" required minlength="5" name="cpa">
                    </div>
                </div>

                <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group">
                        <label for="Nombres">Twitter</label>
                        <input type="text" class="form-control form-control-lg form-control-a" placeholder="Twitter" minlength="2" name="twi" maxlength="30">
                    </div>
                </div>
                <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group">
                        <label for="Nombres">Facebook</label>
                        <input type="text" class="form-control form-control-lg form-control-a" placeholder="Facebook" minlength="2" name="face" maxlength="50">
                    </div>
                </div>
                <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group">
                        <label for="Nombres">Instagram</label>
                        <input type="text" class="form-control form-control-lg form-control-a" placeholder="Instagram" minlength="2" name="inst" maxlength="30">
                    </div>
                </div>

                <div class="col-md-12 mb-2" style="margin-bottom:0px !important;">
                  *Nota: El télefono y correo facilitado seran utilizado por nuestra plataforma para enviarle información acerca de sus solicitudes de citas.
                </div>
                <div class="col-md-12 mb-2" style="margin-bottom:0px !important;">
                    <div class="form-group text-center">
                        <button type="submit" name="btg" class="btn btn-b" style="">Registrarse</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="md-ingresar" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="title-box-d">
                    <h3 class="title-d">Acceso para clientes</h3>
                </div>
                <form class="form-a" method="POST" action="login.php">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="Cédula">Cédula</label>
                            </div>
                        </div>

                        <div class="col-md-3 mb-2" style="">
                            <select class="form-control form-control-lg form-control-a" name="ced1" id="">
                                <option selected="">V</option>
                                <option>E</option>
                                <option>J</option>
                            </select>
                        </div>

                        <div class="col-md-9 mb-2">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu cédula" required name="ced2">
                            </div>
                        </div>
                        
                        <div class="col-md-12 mb-2">
                            <div class="form-group">
                                <label for="Contraseña">Contraseña</label>
                                <input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" name="pas" required minlength="5">
                            </div>
                        </div>
                        <div class="col-md-12 mb-2" id="" style="color:#000;">
                          <a href="#" id="bt_olvido">¿ Olvido su contraseña ?</a>
                        </div>
                        <div class="col-md-12 mb-2" id="msj_login" style="color:red;">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-b" name="btl">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>

<div id="md-olvido" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="title-box-d">
                    <h3 class="title-d">¿ Olvido su contrasaña ?</h3>
                </div>
                <form class="form-a" method="POST" action="login.php">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="Cédula">Cédula</label>
                            </div>
                        </div>

                        <div class="col-md-3 mb-2" style="">
                            <select class="form-control form-control-lg form-control-a" name="ced1" id="">
                                <option selected="">V</option>
                                <option>E</option>
                                <option>J</option>
                            </select>
                        </div>

                        <div class="col-md-9 mb-2">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu cédula" required name="ced2" value="<?php echo explode("-", $_GET['ced'])[1];?>">
                            </div>
                        </div>
                        
                        <?php
                        if(isset($_GET['olvido']) && $_GET['paso'] == 2 && isset($_GET['txt'])){
                        ?>
                        <div class="col-md-12 mb-2">
                            <div class="form-group">
                                <label> Complete su correo electronico, para enviarle su contraseña de ingreso: <span style="color: red"><?php echo $_GET['txt'];?></span> </label>
                                <input type="text" class="form-control form-control-lg form-control-a" placeholder="<?php echo $_GET['txt'];?>" name="cor" required minlength="5">
                            </div>
                        </div>
                        <?php }else if(isset($_GET['olvido']) && isset($_GET['no_aplica'])){ ?>
                        <div class="col-md-12 mb-2" style="color:red;">
                          La cédula ó el correo no coinciden.
                        </div>
                        <?php } ?>
                        <div class="modal-footer">

                        <?php
                          if(isset($_GET['olvido'])&&$_GET['paso']==2&&isset($_GET['txt'])){
                            echo '<button type="submit" class="btn btn-b" name="bt_clave">Enviar</button>';
                        }else{
                            echo '<button type="submit" class="btn btn-b" name="bto">Siguiente</button>';
                          } 
                        ?>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>

<div id="md-solicitud" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
            <form class="form-a" id="formulario_solicitud">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="title-box-d">
                    <h3 class="title-d">Pregunta por el repuesto que necesitas</h3>
                </div>
                    <div class="row">
                      <div class="col-md-6 mb-2">
                          <label for="">Nombre</label>
                          <input type="text" class="form-control" id="nom_sol" required minlength="3" maxlength="12">
                      </div>
                      <div class="col-md-6" >
                          <label for="">Apellido</label>
                          <input type="text" class="form-control" id="ape_sol" required minlength="3" maxlength="12">
                      </div>
                      <div class="col-md-6 mb-2">
                          <label for="">Teléfono</label>
                          <input type="text" class="form-control" id="tel_sol" required minlength="7" maxlength="15">
                      </div>
                      <div class="col-md-6" >
                          <label for="">Correo</label>
                          <input type="mail" class="form-control" id="cor_sol" required minlength="5" maxlength="100">
                      </div>

                      <div class="col-md-12 mb-2">
                          <label for="Mensaje">Describa el repuesto que necesita.</label>
                          <textarea name="message" class="form-control" name="message" cols="45" rows="8" minlength="10" id="men_sol" placeholder="Describa el repuesto"></textarea>
                          <b>Una vez que se envie su solicitud, nos pondremos en contacto con usted.</b>
                      </div>
                    </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-b">Enviar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script>
  $(document).on('ready', function(){
    $("#tipo_per").on('change', function(){
      var v = $("#tipo_per option:selected").val();
      if(v != ""){
        if(v == "J")
          $("#conte_razon").show();
        else
          $("#conte_razon").hide();
      }
    });
    $("#bt_olvido").on('click', function(){
      $("#md-ingresar").modal("hide");
      $("#md-olvido").modal("show");
    });
    $("#bt_registro_modal").on('click', function(){
      $("#txt_msj").text("");
    });

    $("#formulario_solicitud").on('submit', function(){
      var nom = $("#nom_sol").val();
      var ape = $("#ape_sol").val();
      var tel = $("#tel_sol").val();
      var cor = $("#cor_sol").val();
      var res = $("#men_sol").val();

      var obj = {
        modulo: 'repuestos',
        tipo: 'solicitudRepuesto'
      }

      obj.nom = nom;
      obj.ape = ape;
      obj.tel = tel;
      obj.cor = cor;
      obj.res = res;

      $.post('ajax_php.php', obj, function(data){
        if(data.r){
          alert(data.msj);
          location.reload();
        }else{
          alert(data.msj);
        }
      });
      return false;
    });
  });
</script>



