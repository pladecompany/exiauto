<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	session_start();
	include_once('ruta.php');
	include_once("configuraciones.php");
	include_once("panel/modelo/Orm.php");
	include_once("panel/modelo/Modelo.php");
	if($_SERVER['SERVER_NAME']!="localhost"){   
	  if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
		  $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		  header('HTTP/1.1 301 Moved Permanently');
		  header('Location: ' . $location);
		  exit;
	  }
	}
	

	$R = $orm->consultaPersonalizada("SELECT * FROM imagenes_pagina WHERE id=1;");
	iF($IMG = $R->fetch_assoc()){
    }else{
        $IMG['banner']="static/img/banner.jpg";
    }

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="title" content="Exiauto – Su Concesionario Toyota del Este.">
	<meta name="author" content="Plade Company C.A">
	<meta name="keywords" content="Vehículo, carros, comprar, ventas, El este">
	<meta name="description" content="Servicio automotriz con 27 años de experiencia. Ofrecemos mantenimiento express, enderezado, latonería, pintura y repuestos. Agenda tu cita en línea.">

	<title>Exiauto – Su Concesionario Toyota del Este</title>

	<!-- Favicons -->
	<link href="static/img/favicon.png" rel="icon">
	<link href="static/img/apple-touch-icon.png" rel="apple-touch-icon">

	<!-- Bootstrap CSS File -->
	<link href="static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Libraries CSS Files -->
	<link href="static/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="static/css/animate.min.css" rel="stylesheet">
	<link href="static/lib/animate/animate.min.css" rel="stylesheet">
	<link href="static/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="static/lib/owlcarousel/assets/css/owl.carousel.min.css" rel="stylesheet">

	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/jquery.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/dataTables.bootstrap.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/responsive.dataTables.min.css">

	<!-- Main Stylesheet File -->
	<link href="static/css/style.css" rel="stylesheet">
	<link href="static/css/estilos.css" rel="stylesheet">
	<script src="static/lib/jquery/jquery.min.js"></script>

	<script src='fullcalendar/packages/moment.js'></script>
	<?php include_once("analytics.php");?>
</head>

<body>
	<div class="click-closed"></div>
	<nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
		<div class="container">
			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
				aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span></span>
				<span></span>
				<span></span>
			</button>
			
			<a class="navbar-brand text-brand" href="?op=inicio">
				<img src="static/img/logoexiauto-b.png" class="logo_exitauto" alt="Logo Exiauto">
			</a>

			<button type="button" class="btn btn-link nav-search navbar-toggle-box-collapse d-md-none" data-toggle="collapse"
				data-target="#navbar" aria-expanded="false">
				<span class="fa fa-user" aria-hidden="true"></span>
			</button>
			<div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link active" href="?op=inicio">Inicio</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="?op=mantenimiento">Servicios</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="?op=latoneria">Latonería</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="?op=repuestos">Repuestos</a>
					</li>

					<li class="nav-item nav_veh" id="contenedor_vehiculos_menu">
						<a class="nav-link dropdown-toggle" id="drop_vehiculos" role="button" href="?op=vehiculos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Vehículos</a>
						<div class="dropdown-menu drop_vehiculos" aria-labelledby="drop_vehiculos">
							<nav class="navbar navbar-expand-lg navbar-light bg-light text-center nav_vehiculos">
							  <div class="collapse navbar-collapse" id="navbarNavDropdown">
								<ul class="navbar-nav">
									<?php
										$modelo = new Modelo();
										$modelo->tabla = "clasificar_vehiculos";
										$rmo = $modelo->fetchAllClasi();
										while($fmo = $rmo->fetch_assoc()){
									?>
									<li class="nav-item bt_ver_vehiculos" id="<?php echo $fmo['id'];?>">
										<a class="nav-link" href="#!"><b><?php echo strtoupper($fmo['descripcion']);?></b></a>
									</li>
									<?php }?>
								</ul>
							  </div>
							</nav>

							<div class="contenedor_vehiculos row" style="margin:0px;">
							</div>
						</div>
					</li>


					<li class="nav-item nav_cons" id="contenedor_vehiculos_menu_consignacion">
						<a class="nav-link dropdown-toggle" id="drop_vehiculos_consignacion" role="button" href="?op=vehiculos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">A Consignación</a>
                    <div class="dropdown-menu drop_vehiculos_consignacion" aria-labelledby="drop_vehiculos_consignacion">
							<nav class="navbar navbar-expand-lg navbar-light bg-light text-center nav_vehiculos">
							  <div class="collapse navbar-collapse" id="navbarNavDropdown">
								<ul class="navbar-nav">
									<?php
										$modelo = new Modelo();
										$modelo->tabla = "clasificar_vehiculos";
										$rmo = $modelo->fetchAllClasi();
										while($fmo = $rmo->fetch_assoc()){
									?>
									<li class="nav-item bt_ver_vehiculos_consignacion" id="<?php echo $fmo['id'];?>">
										<a class="nav-link" href="#!"><b><?php echo strtoupper($fmo['descripcion']);?></b></a>
									</li>
									<?php }?>
								</ul>
							  </div>
							</nav>

							<div class="contenedor_vehiculos_consignacion row" style="margin:0px;">
							</div>
						</div>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="?op=noticias">Noticias</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="?op=contacto">Contacto</a>
					</li>

				  <?php
					if(isset($_SESSION['log']) && $_SESSION['log'] == true){
					}else{
				  ?>
					<li class="nav-item" style="">
						<a class="nav-link modal-trigger" data-toggle="modal" data-target="#md-ingresar" style="cursor: pointer;" id="bt_ingresar">Ingresar</a>
					</li>
					<?php }?>

					<?php
					  if($promo['est'] == "1"){
					?>
					  <li class="nav-item">
						  <a href="#" id="bt_promo" class="btn btn-b p-2 link-a">Promo</a>
					  </li>
					<?php } ?>
				</ul>
				<?php
				  if(isset($_SESSION['log'])&&$_SESSION['log'] == true){
				?>
				<button onclick="window.location='?op=inicio_log';" type="button" class="btn btn-b-n" style="color:#fff;"> <?php echo "Hola ". $_SESSION['nom'];?> </button>
				<?php
				  }else{
				?>
				<button id="bt_registro_modal" type="button" class="btn btn-b-n navbar-toggle-box-collapse d-none d-md-block" data-toggle="collapse" data-target="#navbar" aria-expanded="false" style="">
					Regístrate
				</button>
				<?php }?>
			</div>

		</div>
	</nav>

	<div class="bannerExi">
        <img id="contenedor_banner" src="<?php echo $IMG['banner'];?>">
	</div>

	<div class="cont_left"></div>

	<div class="cont_right">
		<div style="min-height: 600px;">
			<?php include_once($ruta); ?>
		</div>

		<section class="section-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-4">
						<div class="widget-a">
							<div class="w-header-a">
								<h3 class="w-title-a text-brand">Exiauto</h3>
							</div>
							<div class="w-body-a">
								<p class="w-text-a color-text-a text-justify">
									Para EXIAUTO, lo más importante es el Cliente, El Servicio y la Atención personalizada que estamos en capacidad de ofrecerle en nuestras amplias y confortables instalaciones con 11.860 m2.
								</p>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-4 section-md-t3">
						<div class="widget-a">
							<div class="w-header-a">
								<h3 class="w-title-a text-brand">Nosotros</h3>
							</div>
							<div class="w-body-a">
								<div class="w-body-a">
									<ul class="list-unstyled">
										<li class="item-list-a">
											<i class="fa fa-angle-right"></i> <a href="?op=nosotros">Acerca de nosotros</a>
										</li>
										<!--<li class="item-list-a">
											<i class="fa fa-angle-right"></i> <a href="?op=credicompra">CrediCompra</a>
										</li>-->
										<li class="item-list-a">
											<i class="fa fa-angle-right"></i> <a href="?op=tipsdeservicio">Tips de servicio</a>
										</li>
										<li class="item-list-a">
											<i class="fa fa-angle-right"></i> <a href="?op=noticias">Noticias TOYOTA</a>
										</li>
										<li class="item-list-a">
											<i class="fa fa-angle-right"></i> <a href="index.php#testimonios">Testimonios</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-md-4 section-md-t3">
						<div class="widget-a">
							<div class="w-header-a">
								<h3 class="w-title-a text-brand">Contáctanos</h3>
							</div>

							<div class="w-body-a">
								<ul class="list-unstyled">
									<li class="item-list-a">
										<a href="mailto:info@exiauto.com" style="color: #555555;"><i class="fa fa-angle-right"></i>info@exiauto.com</span></a>
									</li>
									<li class="item-list-a">
										<i class="fa fa-angle-right"></i>Teléfono MASTER: 20.30.911
									</li>
									<li class="item-list-a">
										<i class="fa fa-angle-right"></i>Horario: Lunes a Viernes de 7:30 am hasta las 4:00 pm
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include_once("modales.php");?>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="socials-a">
							<ul class="list-inline">

								<li class="list-inline-item">
									<a href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp['tlf'];?>&text=Hola me gustaria que me contacten, estoy interesado en obtener información." class="link-one" target="_blank"></a>
										<i class="fa fa-whatsapp" aria-hidden="true"></i>
									</a>
								</li>

								<li class="list-inline-item">
									<a href="https://www.facebook.com/profile.php?id=100063734011239" target="_blank">
										<i class="fa fa-facebook" aria-hidden="true"></i>
									</a>
								</li>

								<li class="list-inline-item">
									<a href="https://twitter.com/Exiauto_Toyota?s=17" target="_blank">
										<i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
								</li>
								
								<li class="list-inline-item">
									<a href="https://instagram.com/exiauto?igshid=19ik0ppx6cec5" target="_blank">
										<i class="fa fa-instagram" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="copyright-footer">
							<p class="copyright color-text-a">
								&copy; Copyright
								<span class="color-a">2022 Exiauto</span> Todos los derechos reservados.
							</p>
						</div>
						<div class="credits">
							<p>Desarrollado por <a href="http://pladecompany.com" target="_blank">Plade Company</a></p>
						</div>
					</div>
				</div>
			</div>

			<div style="background: #000;padding: 5px; color: #fff; font-size:12px;">
				<p class="m-0">Avenida Principal de Los Ruices, cruce con la 1ra. Transversal, (Calle Bernardette). Caracas - Venezuela.</p>
			</div>
		</footer>

		<div class="social-bar">
			<a href="https://twitter.com/Exiauto_Toyota?s=17" class="icon fa fa-twitter icon-twitter" target="_blank"></a>
			<a href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp['tlf'];?>&text=Hola me gustaria que me contacten, estoy interesado en obtener información." class="icon fa fa-whatsapp icon-whatsapp" target="_blank"></a>
			<a href="https://www.facebook.com/profile.php?id=100063734011239" class="icon fa fa-facebook icon-facebook" target="_blank"></a>
			<a href="https://instagram.com/exiauto?igshid=19ik0ppx6cec5" class="icon fa fa-instagram icon-instagram" target="_blank"></a>
			<!--<a href="https://instagram.com/exiauto?igshid=19ik0ppx6cec5" class="icon fa icon-instagram" target="_blank"></a>-->
		</div>

		<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
		<!--<div id="preloader"></div>-->
	</div>

	<script src="static/lib/jquery/jquery-migrate.min.js"></script>
	<script src="static/lib/popper/popper.min.js"></script>
	<script src="static/lib/bootstrap/js/bootstrap.min.js"></script>
	<script src="static/lib/easing/easing.min.js"></script>
	<script src="static/lib/owlcarousel/owl.carousel.min.js"></script>
	<script src="static/lib/scrollreveal/scrollreveal.min.js"></script>
	<script src="static/js/main.js"></script>
	<script src="static/js/registro.js"></script>
	<script src="static/js/jquery.timeago.js"></script>
	<script src="static/js/jquery.livequery.min.js"></script>

	<script type="text/javascript" src="static/lib/JTables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.responsive.min.js"></script>

	<script>
		$(document).ready(function(){
			$('.table').DataTable( {
				"language": {
					"url": "static/lib/JTables/Spanish.json"
				}
			} );
			$('.table').DataTable( {
				responsive: true
			} );
		} );
	</script>

	<script type="text/javascript">

		$('#myModal').on('shown.bs.modal', function () {
			$('#myInput').trigger('focus')
		})

		<?php
		  if(isset($_GET['reg'])){
		?>
			$("#bt_registro_modal").trigger('click');
		<?php
			if(isset($_GET['msj'])){
		?>
			$("#cont_msj").show();
			$("#txt_msj").text("** <?php echo $_GET['msj'];?> **");
		<?php
			}
		  }

		  if($promo['est'] == "1"){ // Si la promocion es visible desde el panel admin entra acá
			// Validamos la session, si existe la sesion no mostramos modal, cuando es primera vez si lo mostramos.
			if( isset($_SESSION['ok']) ){
			}else{
			  $_SESSION['ok'] = true;
		?>
			$('#md-video').modal('show');
			//$('#video_modal').trigger('play');
		<?php
			}
		  }
		?>


	  $(document).on('ready', function(){
		$("#bt_promo").on('click', function(){
			$('#md-video').modal('show');
		});

	  <?php
		if(isset($_GET['clave'])){
	  ?>
		  $("#md-ingresar").modal("show");
		  $('#msj_login').text('Su contraseña de ingreso se envio a su correo, verifiquela y vuelva ingresar');
	  <?php
		}
	  ?>
	  });

	  $(document).on('click', '#bt_reg_log', function(){
		$("#md-ingresar").modal("hide");
		$("#bt_registro_modal").trigger('click');
	  });

	  <?php
		if(isset($_GET['err']) && !isset($_GET['reg'])){
		  ?>
		  $("#bt_ingresar").trigger('click');
		  var html = "Datos invalidos, desea registrarse ? <input type='button' class='btn btn-success' value='Registrarse' id='bt_reg_log'>";
		  $("#msj_login").html(html);
		  <?php
		}
	  ?>
	  <?php
		if(isset($_GET['olvido'])){
		  ?>
		  $("#md-olvido").modal("show");
		  <?php
		}
	  ?>

		$(document).on('click', '#contenedor_vehiculos_menu .dropdown-menu', function (e) {
			e.stopPropagation();
		});


		$(document).on('click', '#contenedor_vehiculos_menu_consignacion .dropdown-menu', function (e) {
			e.stopPropagation();
		});

        var imgs_banner = [];

        <?php
            if($IMG['banner']!=""){
                echo "imgs_banner.push('".$IMG['banner']."');";
            }

            if($IMG['banner_2']!=""){
                echo "imgs_banner.push('".$IMG['banner_2']."');";
            }

            if($IMG['banner_3']!=""){
                echo "imgs_banner.push('".$IMG['banner_3']."');";
            }
        ?>
        
        var thisId=0;
        window.setInterval(function(){
            $('#contenedor_banner').attr('src', imgs_banner[thisId]);
            thisId++; //increment data array id
            if (thisId==3) thisId=0; //repeat from start
        },5000);

		$(document).on('click', '.bt_ver_vehiculos', function (e) {
			var id = $(this).attr('id');

			var obj = {
				modulo: "vehiculos",
				tipo: "obtenerVehiculosPorCategoria",
                id_categoria: id,
                tipo_vehiculo: 1
			}
			$.post('ajax_php.php', obj, function(data){
				if(data.vehiculos.length >= 0){
					$(".contenedor_vehiculos").html("");
					for(var i=0; i < data.vehiculos.length;i++){
						var html = "";
						html+="<div class='col-sm-6 col-md-3 text-center cont_veh' style='padding:0.4em;'>";
						html+= "<a href='?op=vehiculo-ver&id="+data.vehiculos[i].id+"'><img src='"+data.vehiculos[i].img1+"' style='width:100%;border-radius:0.5em;'>";
						html+= "<b>"+data.vehiculos[i].nom_veh+"</b></a>";
						html+="</div>";
						$(".contenedor_vehiculos").append(html);
					}
				}
			});
		});
		$(".bt_ver_vehiculos")[0].click();


		$(document).on('click', '.bt_ver_vehiculos_consignacion', function (e) {
			var id = $(this).attr('id');

			var obj = {
				modulo: "vehiculos",
				tipo: "obtenerVehiculosPorCategoria",
                id_categoria: id,
                tipo_vehiculo: 0
			}
			$.post('ajax_php.php', obj, function(data){
				if(data.vehiculos.length >= 0){
					$(".contenedor_vehiculos_consignacion").html("");
					for(var i=0; i < data.vehiculos.length;i++){
						var html = "";
						html+="<div class='col-sm-6 col-md-3 text-center cont_veh' style='padding:0.4em;'>";
						html+= "<a href='?op=vehiculo-ver&id="+data.vehiculos[i].id+"'><img src='"+data.vehiculos[i].img1+"' style='width:100%;border-radius:0.5em;'>";
						html+= "<b>"+data.vehiculos[i].nom_veh+"</b></a>";
						html+="</div>";
						$(".contenedor_vehiculos_consignacion").append(html);
					}
				}
			});
		});
		$(".bt_ver_vehiculos_consignacion")[0].click();
	</script>
</body>
</html>
