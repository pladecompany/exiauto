<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
    session_start();
    
    if(!isset($_SESSION) || isset($_SESSION['login']) != true){
        echo "<script>window.location = 'salir.php';</script>";
        exit(1);
    }else{
      include_once("modelo/Orm.php");
      include_once("modelo/Citas.php");
      $cn = new Cita();
    }

	include_once('ruta.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<title>Panel administrador, exiauto</title>

	<link href="../static/img/favicon.png" rel="icon">
	<link href="../static/img/apple-touch-icon.png" rel="apple-touch-icon">

	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/all.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/panel.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/css/style.css">

	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/JTables/css/jquery.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/JTables/css/dataTables.bootstrap.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="../static/lib/JTables/css/responsive.dataTables.min.css">

	<script src="../static/lib/jquery/jquery.min.js"></script>
    <script src='../fullcalendar/packages/moment.js'></script>
</head>

<body id="page-top">
	<div id="wrapper">
		<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
				<div class="sidebar-brand-text mx-3">Exiauto</div>
			</a>

			<hr class="sidebar-divider my-0">

			<li class="nav-item active">
				<a class="nav-link" href="?op=inicio">
				<i class="fas fa-fw fa-tachometer-alt"></i>
				<span>Inicio</span></a>
			</li>

			<hr class="sidebar-divider">

			<div class="sidebar-heading">
				Opciones
			</div>

            <?php 
              if($_SESSION['acceso'] == 0 || $_SESSION['acceso'] == 1){
            ?>
			<li class="nav-item">
				<a class="nav-link" href="?op=citas&ano=<?php echo date('Y');?>&mes=<?php echo date('m');?>">
				<i class="fas fa-fw fa-list"></i>
				<span>Citas</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="?op=solicitudes">
				<i class="fas fa-fw fa-list"></i>
				<span>Solicitudes ( <?php echo $cn->solicitudesPendientes();?> )</span></a>
			</li>
            <?php }?>

            <?php 
              if($_SESSION['acceso'] == 0){
            ?>
			<li class="nav-item">
				<a class="nav-link" href="?op=configuraciones">
				<i class="fas fa-fw fa-plus"></i>
				<span>Configuraciones</span></a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="?op=configuraciones_imagenes">
				<i class="fas fa-fw fa-plus"></i>
				<span>Imagenes</span></a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="?op=noticias">
				<i class="fas fa-fw fa-newspaper"></i>
				<span>Noticias</span></a>
			</li>
            <?php }?>

            <?php 
              if($_SESSION['acceso'] == 0 || $_SESSION['acceso'] == 2){
            ?>
			<li class="nav-item">
				<a class="nav-link" href="?op=vehiculos">
				<i class="fas fa-fw fa-car"></i>
				<span>Vehículos</span></a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="?op=modelos">
				<i class="fas fa-fw fa-list"></i>
				<span>Modelos</span></a>
			</li>
            <?php } ?>

            <?php 
              if($_SESSION['acceso'] == 0){
            ?>
			<li class="nav-item">
				<a class="nav-link" href="?op=clientes">
				<i class="fas fa-fw fa-user"></i>
				<span>Clientes</span></a>
			</li>
            <?php }?>

			<hr class="sidebar-divider">

			<div class="sidebar-heading">
				Administrador
			</div>

			<li class="nav-item">
				<li class="nav-item">
					<a class="nav-link" href="?op=cambiarcontraseña">
					<i class="fas fa-fw fa-user"></i>
					<span>Cambiar contraseña</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="salir.php">
					<i class="fas fa-fw fa-lock"></i>
					<span>Salir</span></a>
				</li>
			</li>

			<hr class="sidebar-divider d-none d-md-block">

			<div class="text-center d-none d-md-inline">
				<button class="rounded-circle border-0" id="sidebarToggle"></button>
			</div>

		</ul>

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
					<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
						<i class="fa fa-bars"></i>
					</button>

					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown no-arrow d-sm-none">
							<a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-search fa-fw"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
								<form class="form-inline mr-auto w-100 navbar-search">
									<div class="input-group">
										<input type="text" class="form-control border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
										<div class="input-group-append">
											<button class="btn btn-primary" type="button">
												<i class="fas fa-search fa-sm"></i>
											</button>
										</div>
									</div>
								</form>
							</div>
						</li>

                        <?php
                          if($_SESSION['acceso'] == 0||$_SESSION['acceso'] == 1){
                          ?>
						<li class="nav-item dropdown no-arrow mx-1">
							<a class="nav-link dropdown-toggle" href="?op=solicitudes" id="alertsDropdown" role="button" >
								<i class="fas fa-bell fa-fw"></i>
								<span class="badge badge-danger badge-counter"><?php echo $cn->solicitudesPendientes();?></span>
							</a>
						</li>
                        <?php }?>

						<li class="nav-item dropdown no-arrow mx-1" style="display:none;">
							<a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-envelope fa-fw"></i>
								<span class="badge badge-danger badge-counter">7</span>
							</a>

							<div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
								<h6 class="dropdown-header">
									Mensajes
								</h6>
								<a class="dropdown-item d-flex align-items-center" href="#">
									<div class="dropdown-list-image mr-3">
										<img class="rounded-circle" src="../static/img/user.png" alt="">
									</div>
									<div class="font-weight-bold">
										<div class="text-truncate">Hola buen día, necesito una ayuda.</div>
										<div class="small text-gray-500">Emily Fowler · 58m</div>
									</div>
								</a>
								<a class="dropdown-item d-flex align-items-center" href="#">
									<div class="dropdown-list-image mr-3">
										<img class="rounded-circle" src="../static/img/user.png" alt="">
									</div>
									<div>
										<div class="text-truncate">Tengo una cita para el día 12 de julio, puedo  mover la fecha?</div>
										<div class="small text-gray-500">Jae Chun · 1d</div>
									</div>
								</a>
								<a class="dropdown-item d-flex align-items-center" href="#">
									<div class="dropdown-list-image mr-3">
										<img class="rounded-circle" src="../static/img/user.png" alt="">
									</div>
									<div>
										<div class="text-truncate">Necesito un repuesto para Hilux, como pueden ayudarme?</div>
										<div class="small text-gray-500">Morgan Alvarez · 2d</div>
									</div>
								</a>
								<a class="dropdown-item d-flex align-items-center" href="#">
									<div class="dropdown-list-image mr-3">
										<img class="rounded-circle" src="../static/img/user.png" alt="">
									</div>
									<div>
										<div class="text-truncate">Hola, como puedo solicitar una cita?</div>
										<div class="small text-gray-500">Chicken the Dog · 2w</div>
									</div>
								</a>
								<a class="dropdown-item text-center small text-gray-500" href="#">Leer más</a>
							</div>
						</li>

						<div class="topbar-divider d-none d-sm-block"></div>
						<li class="nav-item dropdown no-arrow">
							<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['usuario'];?></span>
								<img class="img-profile rounded-circle" src="../static/img/user.png">
							</a>
						</li>

					</ul>
				</nav>

				<div class="container-fluid">

					<?php include_once($ruta); ?>

				</div>

			</div>

			<footer class="sticky-footer bg-white">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; Exiauto <?php echo date('Y');?></span>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>

	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="login.html">Salir</a>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="../static/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../static/js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="../static/js/sb-admin-2.min.js"></script>
	<script type="text/javascript" src="../static/lib/JTables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../static/lib/JTables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="../static/lib/JTables/js/dataTables.responsive.min.js"></script>

	<script>
		$(document).ready(function(){
			$('.table').DataTable( {
				"language": {
					"url": "../static/lib/JTables/Spanish.json"
				}
			} );
			$('.table').DataTable( {
				responsive: true
			} );
		} );
	</script>
</body>
</html>
