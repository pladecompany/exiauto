<?php
  include_once("controlador/noticias.php");
?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Noticias</h4>
		
		<div class="text-right">
			<a href="#md-noticia" data-toggle="modal" class="color-b modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Registrar noticia</b></a>
		</div>
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Título</th>
						<th>Estatus</th>
						<th>Acciones</th>
					</tr>
				</thead>

				<tbody>
                  <?php
                    $noti = new Noticia();
                    $r = $noti->fetchAll();
                    $i=0;
                    while($ff = $r->fetch_assoc()){
                      $i++;
                      echo "<tr>";
                      echo "  <td>" . $i . "</td>";
                      echo "  <td>" . $ff['titulo'] . "</td>";
                      echo "  <td>" . (($ff['est_noti']==1)?'Publicado':'Borrador') . "</td>";
                      echo "  <td>";
                      echo "<a href='".$orm->obtenerDominio()."?op=noticia-ver&id=".$ff['id']."' target='__blank'><i class='mr-2 fa fa-eye'></i></a>";
                      echo "<a href='?op=noticias&id=".$ff['id']."'><i class='mr-2 fa fa-edit'></i></a>";
                      echo "<a href='?op=noticias&el=".$ff['id']."' onclick='return confirm(\"¿ Esta seguro ?\")'><i class='mr-2 fa fa-trash'></i></a>";
                      echo "</td>";
                      echo "</tr>";
                    }
                  ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="md-noticia" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
                    <?php if(isset($F)){ ?>
					<h3 class="title-d" id="titulo_modulo">Editar noticia</h3>
                    <?php }else{?>
					<h3 class="title-d" id="titulo_modulo">Nueva noticia</h3>
                    <?php }?>
				</div>

				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_noticia">
                    <?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
					<div class="row">
						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Título">Imagén (Tamaño recomendado 700x500)</label>
								<input type="file" class="form-control " name="img">
                                <?php
                                  if(isset($F)) echo "<div class='text-center' id='cont_img'><a href='".$F['img']."' target='__blank'>Imagén adjunta de la noticia</a></div>";
                                ?>
							</div>
						</div>
						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Título">Estatus de la noticia</label>
                                <select name="est" class="form-control">
                                  <option value="">Seleccione</option>
                                  <option value="1">Publicado</option>
                                  <option value="0">Borrador</option>
                                </select>
							</div>
						</div>
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Título">Título</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Escribe el título" name="tit" value="<?php echo $F['titulo'];?>">
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Contraseña">Ingresa la descripción</label>
								<textarea class="form-control form-control-lg form-control-a" placeholder="Escribe la descripción" style="height: 200px;" name="des"><?php if(isset($F)) echo $F['descripcion'];?></textarea>
							</div>
						</div>
					</div>
                    <div class="modal-footer">
                        <button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-b"><?php echo ((isset($F))?'Guardar Cambios':'Guardar')?></button>
                    </div>
				</form>
			</div>


		</div>
	</div>
</div>

<?php
  if(isset($F)){
?>
  <script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").trigger('click');
      $("select[name='est']").val(<?php echo $F['est_noti'];?>);
    });
  </script>

<?php
  } 
?>
<script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nueva noticia");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Guardar');
        $("input[name='tit']").val('');
        $("textarea[name='des']").val('');
        $("select[name='est']").val('');
        $("#cont_img").remove();
      });
    });

</script>

