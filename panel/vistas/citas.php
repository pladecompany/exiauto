<?php

  include_once("modelo/Citas.php");
  include_once("modelo/Agente.php");
  include_once("modelo/Cliente.php");
  include_once("modelo/MiVehiculo.php");

  $cita = new Cita();

  if(isset($_GET['mes']) && isset($_GET['ano'])){
    $mes = date('m');
    $ano = date('Y');
    if(!isset($_GET['ida'])||$_GET['ida'] == "")
      $ida = null;
    else
      $ida = $_GET['ida'];;

    if(!isset($_GET['est'])||$_GET['est'] == "")
      $est = null;
    else
      $est = $_GET['est'];;
  }else{
    $mes = date('m');
    $ano = date('Y');
    $ida = null;
    $est = null;
  }

  $r_citas = $cita->citasPorMes($est, $ida, $mes, $ano);
?>
<div class="card shadow mb-4" style="">
	<div class="card-header py-3">
      <form action="">
        <input type="hidden" name="op" value="citas">
        <div class="row">
          <div class="col-md-2">
            <label>Año</label>
            <select class="form-control" name="ano">
              <option value="">Seleccione</option>
              <?php
                for($i = 2019; $i<= date('Y'); $i++){
                  if($ano == $i)
                    echo "<option selected>" . $i ."</option>";
                  else
                    echo "<option>" . $i ."</option>";
                }
              ?>
            </select>
          </div>

          <div class="col-md-2">
            <label>Mes</label>
            <select class="form-control" name="mes">
              <option value="">Seleccione</option>
              <?php
                for($i = 1; $i<= 12; $i++){
                  $val = ($i<=9)?'0'.$i:$i;
                  if($mes == $i)
                    echo "<option  selected>" . $val ."</option>";
                  else            
                    echo "<option >" . $val ."</option>";
                }
              ?>
            </select>
          </div>
          <div class="col-md-3">
            <label>Asesores</label>
            <select class="form-control" name="ida">
              <option value="">Todos</option>
              <?php
                $mod = new Agente();
                $r = $mod->fetchAll();

                while($fm = $r->fetch_assoc()){
                  if(isset($_GET['ida']) && $_GET['ida'] == $fm['id']){
              ?>
                  <option value="<?php echo $fm['id'];?>" selected><?php echo strtoupper($fm['nom_age']." ".$fm['ape_age']);?> (<?php echo strtoupper($fm['cod_age']);?>) </option>
              <?php }else{ ?>
                  <option value="<?php echo $fm['id'];?>" ><?php echo strtoupper($fm['nom_age']." ".$fm['ape_age']);?> (<?php echo strtoupper($fm['cod_age']);?>) </option>
              <?php
                    }
                }
              ?>
            </select>
          </div>
          <div class="col-md-3">
            <label>Estatus</label>
            <select class="form-control" name="est">
              <option value="">Todos</option>
              <option <?php if(isset($_GET['est']) && $_GET['est']==0) echo "selected";?>  value="0"><?php echo $cita->estatus(0)['txt'];?></option>
              <option <?php if(isset($_GET['est']) && $_GET['est']==-1) echo "selected";?>  value="-1"><?php echo $cita->estatus(-1)['txt'];?></option>
              <option <?php if(isset($_GET['est']) && $_GET['est']==-2) echo "selected";?>  value="-2"><?php echo $cita->estatus(-2)['txt'];?></option>
              <option <?php if(isset($_GET['est']) && $_GET['est']==1) echo "selected";?>  value="1"><?php echo $cita->estatus(1)['txt'];?></option>
              <option <?php if(isset($_GET['est']) && $_GET['est']==2) echo "selected";?>  value="2"><?php echo $cita->estatus(2)['txt'];?></option>
            </select>
          </div>
          <div class="col-md-2">
            <label>Filtrar</label>
            <input type="submit" class="form-control btn btn-danger" value="Filtrar">
          </div>
        </div>
      </form>
	</div>
	<div class="card-body">
      <link href= '../fullcalendar/packages/core/main.css' rel='stylesheet' />
      <link href= '../fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
      <link href= '../fullcalendar/packages/timegrid/main.css' rel='stylesheet' />
      <script src='../fullcalendar/packages/core/main.js'></script>
      <script src='../fullcalendar/packages/interaction/main.js'></script>
      <script src='../fullcalendar/packages/daygrid/main.js'></script>
      <script src='../fullcalendar/packages/timegrid/main.js'></script>
      <script src='../fullcalendar/packages/core/locales-all.js'></script>
      <script src='../fullcalendar/packages/rrule/main.js'></script>
      <script src='../fullcalendar/packages/list/main.js'></script>
      <script>
        $(document).ready(function(){
          moment.locale('es');         // en
          var calendarEl = document.getElementById('calendar');
          var initialTimeZone = 'America/Caracas';

          var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
            header: {
              left: 'prev,next today',
              center: 'title',
              right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },
            defaultView: 'timeGridWeek',
            defaultDate: '<?php echo date('Y-m-d');?>',
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectMirror: true,
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            minTime: '07:30:00',
            maxTime: '16:00:00',
            allDaySlot: false,
            defaultTimedEventDuration: '00:15:00',
            slotDuration: '00:15:00',
            select: function(arg) {
              var cita = calendar.getEventById('cita_agendada');
              if(cita!=null)
                cita.remove();
              var title = 'Pendiente'
              if (title) {
                calendar.addEvent({
                  id: 'cita_agendada',
                  title: title,
                  start: arg.start,
                  end: arg.end,
                  allDay: arg.allDay
                })
                var cita = calendar.getEventById('cita_agendada');
                $("#fecha_cita").val(moment(cita.start).format('YYYY-MM-DD H:mm'));
                $("#md-agendar").modal('show');
              }
              calendar.unselect()
            },
            selectAllow: function(selectInfo) {
                  return moment().diff(selectInfo.start) <= 0
            },
            eventRender: function(evento) {
            },
            eventClick: function(calEvent, jsEvent, view) {
              var dataCita = calEvent.event.extendedProps.data;
              calEvent.el.setAttribute('evento_' +dataCita.idc, dataCita.idc);
              $("#estatus").text(dataCita.txt).css('background', dataCita.color);
              $("#fec_cita").text(dataCita.fecha)
              $("#agente").text("("+dataCita.cod_age+") " + dataCita.nom_age + " " + dataCita.ape_age);
              $("#cliente").text("("+dataCita.ced_usu+") " + dataCita.nom_usu + " " + dataCita.ape_usu + " - " + dataCita.tel_usu);
              $("#vehiculo").text("("+dataCita.placa+") " + dataCita.serial1 + " AÑO: " + dataCita.ano + " - " + dataCita.modelo);
              $("#enviado").text(dataCita.fec_env);
              $("#kilometros").text(dataCita.kilometros);
              $("#falla").text(dataCita.falla);
              $("#observacion").text(dataCita.observacion);
              $("#motivo").text(dataCita.tipo_cita+": " + dataCita.motivo);
              $("#md-cita").modal('show');
              $("#mensajes").hide();
              $(".img_cargando").hide();

              $(".btop").show();
              if(dataCita.estatus == 0){
              }else if(dataCita.estatus == 1)
                $("#bt_aprobar").hide();
              else if(dataCita.estatus == -1)
                $("#bt_cancelar").hide();
              else if(dataCita.estatus == -2)
                $("#bt_rechazar").hide();
              else if(dataCita.estatus == 2){
                $("#bt_finalizar").hide();
                $("#bt_aprobar").hide();
              }

              $(".btop").each(function(){
                $(this).attr('id_cita', dataCita.idc);
              });
              moment.locale('es');         // en
              $(".momento").each(function(){
                $(this).text(moment($(this).text()).format('llll'));
              });
            },
            events: [
                //{
                //title: 'No disponible',
                //startTime: '00:00:00',
                //endTime: '07:30:00',
                //color: '#ff9f89',
                //daysOfWeek: [1,2,3,4,5]
                //},
                //{
                //title: 'No disponible',
                //startTime: '15:00:00',
                //endTime: '23:59:59',
                //color: '#ff9f89',
                //daysOfWeek: [1,2,3,4,5]
                //},
                {
                title: 'No disponible',
                startTime: '00:00:00',
                endTime: '23:59:59',
                color: '#ff9f89',
                daysOfWeek: [6,0]
                },
            ]
          });
          calendar.setOption('locale', 'es');
          calendar.render();

          console.log(calendar);

          <?php
            while($fcita = $r_citas->fetch_assoc()){
              $est = $cita->estatus($fcita['estatus']);
              if($fcita["reprogramado"]==1 && $fcita['estatus']==1){
                $est["txt"]="Reprogramado";
                $fcita["txt"]="Reprogramado";
              }else{
                $fcita["txt"]=$est["txt"];
              }
              $fcita["color"]=$est["color"];
          ?>
            calendar.addEvent({
                id: 'cita_<?php echo $fcita['idc'];?>',
                title: '<?php echo $est['txt'];?>',
                start: '<?php echo $fcita['fecha'];?>',
                allDay: null,
                color: '<?php echo $est["color"];?>',
                data: JSON.parse('<?php echo json_encode($fcita); ?>')
            });
          <?php
            }
          ?>

          function actualizarEvento(obj){
            var evento = calendar.getEventById("cita_" + obj.cita.id);
            var data = evento.extendedProps.data;
            data.txt = obj.cita.txt;
            data.color = obj.cita.color;
            data.estatus = obj.cita.estatus;
            calendar.addEvent({
                id: 'cita_' + obj.cita.id,
                title: obj.cita.txt,
                start: obj.cita.fecha,
                allDay: null,
                color: obj.cita.color,
                data: data
            });
            evento.remove();
            $("#estatus").text(obj.cita.txt).css('background', obj.cita.color);
          }

          $(".btop").on('click', function(){
            var idc = $(this).attr('id_cita');
            var idt = $(this).attr('id');
            
            if(confirm('Esta seguro?')){
              $(".img_cargando").show();
              $("#mensajes").removeClass("alert-success");
              $("#mensajes").removeClass("alert-danger");
              var obs = $("#observacion").val().trim();
              $("#botones").hide();
              var obj = { 
                modulo: 'citas',
                tipo: 'cambiarEstatus',
                idc: idc,
                valor: null,
                ano: '<?php echo $ano;?>',
                mes: '<?php echo $mes;?>',
                est: '<?php echo $est;?>',
                ida: '<?php echo $ida;?>',
                obs: obs
              }

              if(idt == 'bt_aprobar'){
                obj.valor = 1;
                var bt = $("#bt_aprobar");
              }else if(idt == 'bt_finalizar'){
                obj.valor = 2;
                var bt = $("#bt_finalizar");
              }else if(idt == 'bt_rechazar'){
                obj.valor = -2;
                var bt = $("#bt_rechazar");
              }else if(idt == 'bt_cancelar'){
                obj.valor = -1;
                var bt = $("#bt_cancelar");
              }

              $.post('ajax_php.php', obj, function(data){
                console.log(data);
                $(".img_cargando").hide();
                $("#mensajes").show();
                $("#botones").show();
                if(data.r == true){
                  actualizarEvento(data);
                  $("#mensajes").addClass("alert-success").text(data.msj);
                  bt.hide();
                }else
                  $("#mensajes").addClass("alert-danger").text(data.msj);
              });
            }
          });

          $("#id_cliente").on('change', function(){
            var idc = $("#id_cliente option:selected").val();
            if(idc != ""){
              $(".img_cargando").show();
              $.post('ajax_php.php', {modulo: 'clientes', tipo: 'consultarVehiculos', idc: idc}, function(data){
                  $(".img_cargando").hide();
                  if(data.vehiculos){
                    $("#id_vehiculo").html("");
                    for(var i = 0; i < data.vehiculos.length; i++){
                      var veh = data.vehiculos[i];
                      $("#id_vehiculo").append("<option value='" + veh.idv+"'> (" +veh.placa+") " + veh.serial1 +" - " +veh.ano+"</option>");
                    }
                  }
              });
            }
          });

          $("#bt_guardar_cita").on('click', function(){
            var ida = $("#id_agente option:selected").val();
            if(ida == "") {
              alert('Seleccione el agente');
              return;
            }

            var idc = $("#id_cliente option:selected").val();
            if(idc == "") {
              alert('Seleccione el cliente');
              return;
            }

            var idv = $("#id_vehiculo option:selected").val();
            if(idc == "") {
              alert('Seleccione el vehículo');
              return;
            }

            var tip = $("#tipo_cita option:selected").val();
            if(tip == "") {
              alert('Seleccione el tipo de visita');
              return;
            }

            var mot = $("#motivo_cita").val();
            if(mot == "") {
              alert('Agregue el motivo de la cita');
              return;
            }

            var kil = $("#kil option:selected").val();
            var rev = $("#rev option:selected").val();


            var fec = $("#fecha_cita").val();

            var obj = {
              modulo: 'citas',
              tipo: 'nuevaCita',
              fec: fec,
              ida: ida,
              idc: idc,
              idv: idv,
              tip: tip,
              mot: mot,
              kil: kil,
              rev: rev
            }
            console.log(obj);
            $(".img_cargando").show();
            $.post('ajax_php.php', obj, function(resp){
              console.log(resp);
              $(".img_cargando").hide();
              if(resp.r == true){
                var evento = calendar.getEventById("cita_agendada");
                evento.remove();
                var data = resp.cita;
                calendar.addEvent({
                    id: 'cita_' + data.idc,
                    title: data.txt,
                    start: data.fecha,
                    allDay: null,
                    color: data.color,
                    data: data
                });
                $("#md-agendar").modal("hide");
                $("#motivo_cita").val("");
                $("#id_cliente").val("");
                $("#id_agente").val("");
                $("#id_vehiculo").html("");
              }else{
                alert(resp.msj);
              }
            });

          });
        });

      </script>
      <style>

        #calendar {
          max-width: 100%;
          width:100%;
          margin: 0 auto;
          height:auto;
        }

        .fc-content{
          color:#fff;
        }

      </style>

      <div id='calendar'></div>
  </div>
</div>

<div id="md-agendar" class="modal modalmedium fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5>Crear una nueva solicitud</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <div class="row">
          <div class="col-md-6 mb-2">
              <div class="form-group">
                  <label for="Modelo">Seleccione asesor</label>
                  <select name="age" class="form-control" required id="id_agente">
                      <option value="">Seleccione</option>
                  <?php
                    $mod = new Agente();
                    $r = $mod->fetchAll();

                    while($fm = $r->fetch_assoc()){
                      if(isset($F) && $F['id_agente'] == $fm['id']){
                  ?>
                      <option value="<?php echo $fm['id'];?>" selected><?php echo strtoupper($fm['nom_age']." ".$fm['ape_age']);?> (<?php echo strtoupper($fm['cod_age']);?>) </option>
                  <?php }else{ ?>
                      <option value="<?php echo $fm['id'];?>" ><?php echo strtoupper($fm['nom_age']." ".$fm['ape_age']);?> (<?php echo strtoupper($fm['cod_age']);?>) </option>
                  <?php
                        }
                    }
                  ?>
                  </select>
            </div>
          </div>
          <div class="col-md-6 mb-2">
              <div class="form-group">
                  <label for="Modelo">Cliente</label>
                  <select name="cli" id="id_cliente" class="form-control" required>
                      <option value="">Seleccione</option>
                  <?php
                    $mod = new Cliente();
                    $r = $mod->fetchAll();

                    while($fm = $r->fetch_assoc()){
                  ?>
                      <option value="<?php echo $fm['id'];?>" ><?php echo strtoupper($fm['nom_usu']." ".$fm['ape_usu']);?> (<?php echo strtoupper($fm['ced_usu']);?>) </option>
                  <?php
                    }
                  ?>
                  </select>
            </div>
          </div>
          <div class="col-md-6 mb-2">
              <div class="form-group">
                  <label for="Modelo">Vehículos</label>
                  <select name="" id="id_vehiculo" class="form-control" required>
                      <option value="">Seleccione</option>
                  </select>
            </div>
          </div>
          <div class="col-md-6 mb-2">
              <div class="form-group">
                  <label for="Tipo">Seleccione tipo de cita</label>
                  <select name="tip" class="form-control" required id="tipo_cita">
                          <option value="">Seleccione</option>
                          <option val="mat_ped">Mantenimiento períodico</option>
                          <option val="rev_gen">Revisión general</option>
                          <option val="gar_tdv">Garantía TDV (Vehículo nuevo)</option>
                          <option val="gar_ser">Garantía de servicio (Retorno)</option>
                  </select>
              </div>
          </div>
						<div class="col-md-6 mb-2" id="contenedor_revision">
							<div class="form-group">
								<label for="Modelo">Seleccione revisión</label>
                                <select name="rev" id="rev" class="form-control">
                                    <option value="">Seleccione</option>
                                <?php
                                  $mod = new MiVehiculo();
                                  $r = $mod->fallas();
                                  while($fm = $r->fetch_assoc()){
                                ?>
                                    <option value="<?php echo $fm['id'];?>" ><?php echo strtoupper($fm['falla']);?></option>
                                <?php
                                  }
                                ?>
						        </select>
                          </div>
						</div>
						<div class="col-md-6 mb-2" id="contenedor_kilometraje">
							<div class="form-group">
								<label for="Modelo">Kilometraje actual</label>
										<select name="kil" class="form-control" id="kil">
                                          <option value="">Seleccione</option>
											<?php
												$mod = new MiVehiculo();
												$r = $mod->kilometrajes();
												while($fm = $r->fetch_assoc()){
											?>
													<option class="kilometros" value="<?php echo $fm['id'];?>" tip="<?php echo $fm['tipo_kil'];?>"><?php echo $fm['kilometros'];?></option>
											<?php
												}
											?>
										</select>
							</div>
						</div>
          <div class="col-md-12 mb-2">
              <div class="form-group">
                  <label for="Modelo">¿ Motivo de la cita ?</label>
                  <input type="hidden" id="fecha_cita" value="">
                  <textarea name="" id="motivo_cita" class="form-control form-control-lg form-control-a" style="height:100px;"></textarea>
              </div>
          </div>
          <div class="modal-footer text-center">
              <div style="display:none;float:right;" class="img_cargando" id="img_cargando"><img src="../static/img/cargando.gif" style="width:50px;"></div>
              <button type="submit" id="bt_guardar_cita" name="" class="btn btn-b">Guardar Cita</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="md-cita" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <h5>Solicitud de cita</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
                  <form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
                      <div class="row">
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Estatus </b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group text-center" id="estatus" style="color:#fff;">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Enviada</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group momento" id="enviado">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Cita</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group momento" id="fec_cita">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Asesor</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="agente">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Cliente</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="cliente">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Vehículo</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="vehiculo">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Kilometros </b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="kilometros">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Falla </b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="falla">
                              </div>
                          </div>
                          <div class="col-md-12 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Tipo y motivo de la cita</b></label>
                              </div>
                          </div>
                          <div class="col-md-12 mb-2">
                              <div class="form-group" id="motivo">
                              </div>
                          </div>
                          <div class="col-md-12 mb-2" style="" id="conte_observacion">
                              <label for="Modelo"><b>Observación para ser enviada al cliente.</b></label>
                              <div class="form-group">
                                <textarea class="form-control" id="observacion"></textarea>
                              </div>
                          </div>
                          <div class="col-md-12 mb-2">
                              <div style="display:none;" class="text-center alert alert-info" id="mensajes"></div>
                              <div style="display:none;" class="img_cargando" id="img_cargando"><img src="../static/img/cargando.gif" style="width:50px;"></div>
                          </div>
                          <div class="modal-footer" style="" id="botones">
                            <input type="button" class="btn btop btn-info" value="Aprobar cita" id="bt_aprobar" id_cita="">
                            <input type="button" class="btn btop btn-danger" value="Rechazar" id="bt_rechazar" id_cita="">
                            <input type="button" class="btn btop btn-success" value="Marcar como realizada" id="bt_finalizar" id_cita="">
                            <input type="button" class="btn btop btn-danger" value="Cancelar la cita" id="bt_cancelar" id_cita="">
                          </div>
                      </div>
                  </form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src='../static/js/citas.js'></script>
