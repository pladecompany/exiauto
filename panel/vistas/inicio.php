<?php

  $sql = "SELECT * FROM noticias;";
  $rn = $orm->consultaPersonalizada($sql);
  $nn = $rn->num_rows;

  $sql = "SELECT * FROM vehiculos;";
  $rc = $orm->consultaPersonalizada($sql);
  $nc = $rc->num_rows;

  $sql = "SELECT id FROM agentes;";
  $rc = $orm->consultaPersonalizada($sql);
  $na = $rc->num_rows;

  $sql = "SELECT id FROM admins;";
  $rc = $orm->consultaPersonalizada($sql);
  $nu = $rc->num_rows;

  $sql = "SELECT id FROM reclamos where est_rec=0;";
  $rc = $orm->consultaPersonalizada($sql);
  $nr = $rc->num_rows;

  $sql = "SELECT id FROM solicitud_repuestos where estatus=0;";
  $rc = $orm->consultaPersonalizada($sql);
  $nsr = $rc->num_rows;
?>
<div class="row">
<?php
  if($_SESSION['acceso']==0){
?>
	<div class="col-xl-4 col-md-6 mb-4">
      <a href="?op=solicitud_repuestos">
		<div class="card border-left-danger shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Solicitud de repuestos</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $nsr;?></div>
					</div>
					<div class="col-auto">
						<i class="fas fa-cogs fa-2x"></i>
					</div>
				</div>
			</div>
		</div>
      </a>
	</div>
	<div class="col-xl-4 col-md-6 mb-4">
      <a href="?op=noticias">
		<div class="card border-left-danger shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Noticias</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $nn;?></div>
					</div>
					<div class="col-auto">
						<i class="fas fa-newspaper fa-2x"></i>
					</div>
				</div>
			</div>
		</div>
      </a>
	</div>
<?php } ?>

  <?php
    if($_SESSION['acceso']==0||$_SESSION['acceso']==2){
  ?>
	<div class="col-xl-4 col-md-6 mb-4">
      <a href="?op=vehiculos">
		<div class="card border-left-danger shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Vehículos</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $nc;?></div>
					</div>
					<div class="col-auto">
						<i class="fas fa-car fa-2x"></i>
					</div>
				</div>
			</div>
		</div>
      </a>
	</div>
    <?php }?>


  <?php
    if($_SESSION['acceso']==0||$_SESSION['acceso']==1){
  ?>
	<div class="col-xl-4 col-md-6 mb-4">
      <a href="?op=solicitudes">
		<div class="card border-left-danger shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Solicitudes por procesar</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $cn->solicitudesPendientes();?></div>
					</div>
					<div class="col-auto">
						<i class="fas fa-comments fa-2x"></i>
					</div>
				</div>
			</div>
		</div>
      </a>
	</div>
    <?php }?>

<?php
  if($_SESSION['acceso']==0){
?>
	<div class="col-xl-4 col-md-6 mb-4">
      <a href="?op=reclamos">
		<div class="card border-left-danger shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Sugerencias/Reclamos</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $nr;?></div>
					</div>
					<div class="col-auto">
						<i class="fas fa-envelope fa-2x"></i>
					</div>
				</div>
			</div>
		</div>
      </a>
	</div>
	<div class="col-xl-4 col-md-6 mb-4">
      <a href="?op=asesores">
		<div class="card border-left-danger shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Asesores para citas</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $na;?></div>
					</div>
					<div class="col-auto">
						<i class="fas fa-user fa-2x"></i>
					</div>
				</div>
			</div>
		</div>
      </a>
	</div>
	<div class="col-xl-4 col-md-6 mb-4">
      <a href="?op=admins">
		<div class="card border-left-danger shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Usuarios del sistema</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $nu;?></div>
					</div>
					<div class="col-auto">
						<i class="fas fa-lock fa-2x"></i>
					</div>
				</div>
			</div>
		</div>
      </a>
	</div>
<?php } ?>
</div>
