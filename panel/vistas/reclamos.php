<?php
  include_once("modelo/Reclamo.php");

  include_once("controlador/reclamos.php");
?>
<div class=" mt-5">
  <?php include_once("mensajes.php");?>
	<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Reclamos y sugerencias</h4>
	</div>

	<div class="card-body">
        <form action="" method="GET">
          <input type="hidden" name="op" value="reclamos">
          <div class="row">
            <?php
              //$primero = date('Y-m-')."01";
              if(isset($_GET['fec1']) && isset($_GET['fec2'])){
                $primero = $_GET['fec1'];
                $ultimo = $_GET['fec2'];
              }else{
                $fa = date('Y-m-d');
                $primero = date("Y-m-d", strtotime($fa. "- 4 days"));
                $a_date = date('Y-m-d');
                //$ultimo = date("Y-m-t", strtotime($a_date));
                $ultimo = date('Y-m-d');
              }
            ?>
            <div class="col-md-3">

              <label>Desde: </label>
              <input type="date" name="fec1" class="form-control" value="<?php echo (!isset($_GET['fec1']))?$primero:$_GET['fec1'];?>">
            </div>
            <div class="col-md-3">
              <label>Hasta: </label>
              <input type="date" name="fec2" class="form-control" value="<?php echo (!isset($_GET['fec2']))?$ultimo:$_GET['fec2']?>">
            </div>
            <div class="col-md-3">
              <label>&nbsp;</label>
              <br>
              <input type="submit" name="btf" class="btn btn-danger" value="Filtrar">
            </div>
          </div>
        </form>
        <hr>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Cliente</th>
						<th>Enviado</th>
						<th>Tipo</th>
						<th>Departamento</th>
						<th>Estatus</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
	              <?php
	                $noti = new Reclamo();
	                $r = $noti->reclamosGenerales($primero, $ultimo);
	                $i=0;
	                while($ff = $r->fetch_assoc()){
	                  $i++;
	                  echo "<tr>";
	                  echo "  <td>" . $i . "</td>";
	                  echo "  <td>" . $ff['ced_usu'] . " - ".$ff['nom_usu']." " .$ff['ape_usu']."</td>";
	                  echo "  <td class='momento'>" . $ff['fec_reg_rec'] . "</td>";
	                  echo "  <td>" . (($ff['tipo']=='R')?'Reclamo':'Sugerencia') . "</td>";
	                  echo "  <td>" . $ff['nom_dep'] . "</td>";
	                  echo "  <td>" . (($ff['est_rec']==0)?'Enviado':'Procesado') . "</td>";
	                  echo "<td><a href='?op=ver_reclamo&id=".$ff['idr']."' onclick=''>Ver detalles <i class='mr-2 fa fa-eye'></i></a>";
	                  echo "<a href='?op=reclamos&el=".$ff['idr']."' onclick='return confirm(\"¿ Esta seguro ?\")'><i class='mr-2 fa fa-trash'></i></a>";
	                  echo "</td>";
	                  echo "</tr>";
	                }
	              ?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
</div>

<?php
  if(isset($F)){
?>
  <script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").trigger('click');
    });
  </script>

<?php
  } 
?>

<script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nuevo reclamo ó sugerencia");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Enviar');
        $("input[name='pla']").val('');
      });

      moment.locale('es');         // en
      $(".momento").each(function(){
        $(this).text(moment($(this).text()).format('llll'));
      });

    });

</script>

