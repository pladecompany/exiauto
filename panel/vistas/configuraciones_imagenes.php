<?php
include_once("controlador/configuraciones_imagenes.php");

$R = $orm->consultaPersonalizada("SELECT * FROM imagenes_pagina;");
?>
<div class="card shadow mb-4">
<?php
if($IMG = $R->fetch_assoc()){
?>
	<div class="card-header py-3 text-center">
		<h4 class="m-0 font-weight-bold color-b">Imagenes del slider principal (Tamaño recomendado 1200x500px)</h4>
		
		<div class="text-right">
		</div>
	</div>

	<div class="card-body">
        <form class="form-a" method="POST" action="" enctype="multipart/form-data" id="">
          <div class="row">

              <div class="row" style="margin-top:2em;">
                <div class="col-md-4">
                  <label for="img">Imagén 1</label>
                  <input type="file" id="img" class="form-control" name="img_1" >
                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_1'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-4">
                  <label for="img">Imagén 2</label>
                  <input type="file" id="img" class="form-control" name="img_2" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_2'];?>" style="width:100%;">
                    <?php } ?>
                </div>


                <div class="col-md-4">
                  <label for="img">Imagén 3</label>
                  <input type="file" id="img" class="form-control" name="img_3" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_3'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-4 mt-5">
                  <label for="img">Imagén 4</label>
                  <input type="file" id="img" class="form-control" name="img_4" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_4'];?>" style="width:100%;">
                    <?php } ?>
                </div>

              </div>

                <div class="card-header py-3 row w100 text-center" style="margin:0px;padding:0px;width:100%;margin-top:2em;">
                    <h4 class="m-0 font-weight-bold color-b text-center" style="width:100%;">Imagenes de los servicios pagina principal  (Tamaño recomendado 600x600px)</h4>
                    <div class="text-right">
                    </div>
                </div>


                <div class="col-md-3 mt-5">
                  <label for="img">Servicio mantenimiento</label>
                  <input type="file" id="img" class="form-control" name="img_servicio_1" >
                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_servicio_1'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-3 mt-5">
                  <label for="img">Servicio pintura</label>
                  <input type="file" id="img" class="form-control" name="img_servicio_2" >
                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_servicio_2'];?>" style="width:100%;">
                    <?php } ?>
                </div>


                <div class="col-md-3 mt-5">
                  <label for="img">Servicio repuestos</label>
                  <input type="file" id="img" class="form-control" name="img_servicio_3" >
                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_servicio_3'];?>" style="width:100%;">
                    <?php } ?>
                </div>


                <div class="col-md-3 mt-5">
                </div>

                <div class="col-md-3 mt-5">
                  <label for="img">Banner principal (Recomendado 300x1200px)</label>
                  <input type="file" id="img" class="form-control" name="banner" >
                    <hr>
                    <?php if($IMG['banner']){ ?>
                    <img src="<?php echo $IMG['banner'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-3 mt-5">
                  <label for="img">Banner principal (Recomendado 300x1200px)</label>
                  <input type="file" id="img_2" class="form-control" name="banner_2" >
                    <hr>
                    <?php if($IMG['banner_2']){ ?>
                    <img src="<?php echo $IMG['banner_2'];?>" style="width:100%;">
                    <?php } ?>
                </div>


                <div class="col-md-3 mt-5">
                  <label for="img">Banner principal (Recomendado 300x1200px)</label>
                  <input type="file" id="img_3" class="form-control" name="banner_3" >
                    <hr>
                    <?php if($IMG['banner_3']){ ?>
                    <img src="<?php echo $IMG['banner_3'];?>" style="width:100%;">
                    <?php } ?>
                </div>


        <?php }?>


              <div class="row" style="margin-top:2em;width: 100%;">
                <div class="col-md-12 text-center">
                  <button type="submit" name="bt_promo" value="1" class="btn btn-success pull-right" >Guardar información pagina principal</button>
                </div>
              </div>
        </form>
        <?php
        if($IMG = $R->fetch_assoc()){
        ?>

        <form class="form-a row" method="POST" action="" enctype="multipart/form-data" id="">
                <div class="card-header py-3 row w100 text-center" style="margin:0px;padding:0px;width:100%;margin-top:2em;">
                    <h4 class="m-0 font-weight-bold color-b text-center" style="width:100%;">Imagenes de slider sección servicios (Tamaño recomendado 1200x500px)</h4>
                    <div class="text-right">
                    </div>
                </div>

                <div class="col-md-4">
                  <label for="img">Imagén 1</label>
                  <input type="file" id="img" class="form-control" name="img_1" >
                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_1'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-4">
                  <label for="img">Imagén 2</label>
                  <input type="file" id="img" class="form-control" name="img_2" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_2'];?>" style="width:100%;">
                    <?php } ?>
                </div>


                <div class="col-md-4">
                  <label for="img">Imagén 3</label>
                  <input type="file" id="img" class="form-control" name="img_3" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_3'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-4 mt-5">
                  <label for="img">Imagén 4</label>
                  <input type="file" id="img" class="form-control" name="img_4" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_4'];?>" style="width:100%;">
                    <?php } ?>
                </div>

              <div class="row" style="margin-top:2em;width: 100%;">
                <div class="col-md-12 text-center">
                  <button type="submit" name="bt_promo_servicio" value="2" class="btn btn-success pull-right" >Guardar información sección servicios</button>
                </div>
              </div>
        </form>

        <?php }?>

        <?php
        if($IMG = $R->fetch_assoc()){
        ?>

        <form class="form-a row" method="POST" action="" enctype="multipart/form-data" id="">
                <div class="card-header py-3 row w100 text-center" style="margin:0px;padding:0px;width:100%;margin-top:2em;">
                    <h4 class="m-0 font-weight-bold color-b text-center" style="width:100%;">Imagenes de slider sección latonería (Tamaño recomendado 1200x500px)</h4>
                    <div class="text-right">
                    </div>
                </div>

                <div class="col-md-4">
                  <label for="img">Imagén 1</label>
                  <input type="file" id="img" class="form-control" name="img_1" >
                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_1'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-4">
                  <label for="img">Imagén 2</label>
                  <input type="file" id="img" class="form-control" name="img_2" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_2'];?>" style="width:100%;">
                    <?php } ?>
                </div>


                <div class="col-md-4">
                  <label for="img">Imagén 3</label>
                  <input type="file" id="img" class="form-control" name="img_3" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_3'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-4 mt-5">
                  <label for="img">Imagén 4</label>
                  <input type="file" id="img" class="form-control" name="img_4" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_4'];?>" style="width:100%;">
                    <?php } ?>
                </div>

              <div class="row" style="margin-top:2em;width: 100%;">
                <div class="col-md-12 text-center">
                  <button type="submit" name="bt_promo_servicio" value="3" class="btn btn-success pull-right" >Guardar información sección latonería</button>
                </div>
              </div>
        </form>

        <?php }?>


        <?php
        if($IMG = $R->fetch_assoc()){
        ?>

        <form class="form-a row" method="POST" action="" enctype="multipart/form-data" id="">
                <div class="card-header py-3 row w100 text-center" style="margin:0px;padding:0px;width:100%;margin-top:2em;">
                    <h4 class="m-0 font-weight-bold color-b text-center" style="width:100%;">Imagenes de slider sección repuestos (Tamaño recomendado 1200x500px)</h4>
                    <div class="text-right">
                    </div>
                </div>

                <div class="col-md-4">
                  <label for="img">Imagén 1</label>
                  <input type="file" id="img" class="form-control" name="img_1" >
                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_1'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-4">
                  <label for="img">Imagén 2</label>
                  <input type="file" id="img" class="form-control" name="img_2" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_2'];?>" style="width:100%;">
                    <?php } ?>
                </div>


                <div class="col-md-4">
                  <label for="img">Imagén 3</label>
                  <input type="file" id="img" class="form-control" name="img_3" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_3'];?>" style="width:100%;">
                    <?php } ?>
                </div>

                <div class="col-md-4 mt-5">
                  <label for="img">Imagén 4</label>
                  <input type="file" id="img" class="form-control" name="img_4" >

                    <hr>
                    <?php if($IMG){ ?>
                    <img src="<?php echo $IMG['img_slider_4'];?>" style="width:100%;">
                    <?php } ?>
                </div>

              <div class="row" style="margin-top:2em;width: 100%;">
                <div class="col-md-12 text-center">
                  <button type="submit" name="bt_promo_servicio" value="4" class="btn btn-success pull-right" >Guardar información sección repuestos</button>
                </div>
              </div>
        </form>

        <?php }?>
            </div>
    </div>
</div>

<script>
  $(document).ready(function(){
    $("#cod_video").on('change', function(){
      var url = $("#cod_video").val();
      console.log(url);
      var cod = url.substring(url.indexOf("=")+1, url.length);
      if(cod.length != 0){
        $("#cod_video").val(cod);
      }
    });
  });
</script>
