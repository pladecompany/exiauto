<?php
  include_once("controlador/vehiculos.php");
?>

<div class="title-box-d">
	<?php if(isset($F)){ ?>
	<h3 class="title-d" id="titulo_modulo">Editar vehículo</h3>
	<?php }else{?>
	<h3 class="title-d" id="titulo_modulo">Nuevo vehículo</h3>
	<?php }?>
</div>

<div class="card shadow mb-4">
	<div class="card-body">
		<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_noticia">
			<?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>


			<div class="row mt-5">
				<div class="col-sm-12 col-md-4 mb-2">
					<div class="form-group">
						<label for="Título">Vehículo</label>
						<input type="text" class="form-control form-control-lg form-control-a" placeholder="Escribe el nombre" name="nom" value="<?php echo $F['nom_veh'];?>" required>
					</div>
				</div>

				<div class="col-sm-12 col-md-4 mb-2">
					<div class="form-group">
						<label for="Título">Modelo</label>
						<select name="mod" class="form-control" required>
							<option value="">Seleccione</option>
                        <?php
                          $mod = new Modelo();
                          $r = $mod->fetchAll();
                          while($fm = $r->fetch_assoc()){
                            if(isset($F) && $F['id_modelo'] == $fm['id']){
                        ?>
							<option value="<?php echo $fm['id'];?>" selected><?php echo strtoupper($fm['modelo']);?></option>
                        <?php }else{?>
							<option value="<?php echo $fm['id'];?>"><?php echo strtoupper($fm['modelo']);?></option>
                        <?php
                              }
                          }
                        ?>
						</select>
					</div>
				</div>


				<div class="col-sm-12 col-md-4 mb-2">
					<div class="form-group">
						<label for="Título">Clasificación</label>
						<select name="clasificacion" class="form-control" required>
							<option value="">Seleccione</option>
                            <?php
                              $mod = new Modelo();
                                $mod->tabla = "clasificar_vehiculos";
                              $r = $mod->fetchAllClasi();
                              while($fm = $r->fetch_assoc()){
                                if(isset($F) && $F['id_clasificacion'] == $fm['id']){
                            ?>
                                <option value="<?php echo $fm['id'];?>" selected><?php echo strtoupper($fm['descripcion']);?></option>
                            <?php }else{?>
                                <option value="<?php echo $fm['id'];?>"><?php echo strtoupper($fm['descripcion']);?></option>
                            <?php
                                  }
                              }
                            ?>
						</select>
					</div>
				</div>

				<div class="col-sm-12 col-md-2 mb-2">
					<div class="form-group">
						<label for="Título">Año</label>
						<input type="text" class="form-control form-control-lg form-control-a" placeholder="Escribe el año" name="ano" value="<?php echo $F['ano_veh'];?>" required>
					</div>
				</div>

				<div class="col-sm-12 col-md-2 mb-2">
					<div class="form-group">
						<label for="Título">Precio</label>
						<input type="text" class="form-control form-control-lg form-control-a" placeholder="Escribe el precio" name="pre" value="<?php echo $F['pre_veh'];?>">
					</div>
				</div>

				<div class="col-sm-12 col-md-4 mb-2">
					<div class="form-group">
						<label for="Título">Transmisión</label>
						<select name="tra" class="form-control" required>
							<option value="">Seleccione</option>
							<option <?php echo ($F['tra_veh']=='Manual')?'selected':'';?> >Manual</option>
							<option <?php echo ($F['tra_veh']=='Automático')?'selected':'';?> >Automático</option>
						</select>
					</div>
				</div>

				<div class="col-sm-12 col-md-4 mb-2">
					<div class="form-group">
						<label for="Título">Seleccione el estatus</label>
						<select name="est" class="form-control" required>
							<option value="">Seleccione</option>
							<option value="1" <?php echo ($F['est_veh']==1)?'selected':'';?>>Publicación visible</option>
							<option value="0" <?php echo ($F['est_veh']==0)?'selected':'';?>>Publicación no visible</option>
						</select>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 mb-2">
					<div class="form-group">
						<label for="Título">Seleccione el tipo</label>
						<select name="imp" class="form-control">
							<option value="">Seleccione</option>
							<option value="1" <?php echo ($F['imp_veh']==1)?'selected':'';?>>Vehículo normal</option>
							<option value="0" <?php echo ($F['imp_veh']==0)?'selected':'';?>>Vehículo a consignación</option>
						</select>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 mb-2">
					<div class="form-group">
						<label for="Título">Disponible en exiauto ?</label>
						<select name="sto" class="form-control">
							<option value="">Seleccione</option>
							<option value="1" <?php echo ($F['sto_veh']==1)?'selected':'';?>>Si</option>
							<option value="0" <?php echo ($F['sto_veh']==0)?'selected':'';?>>No</option>
						</select>
					</div>
				</div>

                <div class="col-md-6 mb-2">
                    <div class="form-group">
                        <label for="">Adjuntar documento complementario/informativo. (Opcional)</label>
                        <input type="file" class="form-control" name="doc">
                        <?php
                          if(isset($F)) echo "<div class='text-center' id='cont_img'><a href='".$F['doc']."' target='__blank'>Documento adjunto.</a></div>";
                        ?>
                    </div>
                    <br><br>
                </div>

                <div class="row" style="width:100%;">
					<div class="col-md-6 mb-2">
						<div class="form-group">
							<label for="Contraseña">Escribe la descripción / Presentación</label>
							<textarea class="form-control form-control-lg form-control-a" placeholder="Escribe la descripción" style="height: 200px;" name="des"><?php if(isset($F)) echo $F['des_veh'];?></textarea>
						</div>
					</div>

					<div class="col-md-6 mb-2">
						<div class="form-group">
							<label for="Contraseña">Escribe la flexibilidad</label>
							<textarea class="form-control form-control-lg form-control-a" placeholder="Escribe la flexibilidad" style="height: 200px;" name="fle"><?php if(isset($F)) echo $F['fle_veh'];?></textarea>
						</div>
					</div>

					<div class="col-md-6 mb-2">
						<div class="form-group">
							<label for="Contraseña">Escribe el diseño</label>
							<textarea class="form-control form-control-lg form-control-a" placeholder="Escribe el diseño" style="height: 200px;" name="dis"><?php if(isset($F)) echo $F['dis_veh'];?></textarea>
						</div>
					</div>

					<div class="col-md-6 mb-2">
						<div class="form-group">
							<label for="Contraseña">Escribe la seguridad</label>
							<textarea class="form-control form-control-lg form-control-a" placeholder="Escribe la seguridad" style="height: 200px;" name="seg"><?php if(isset($F)) echo $F['seg_veh'];?></textarea>
						</div>
					</div>
                </div>

                <div class="row" style="width:100%;">
                    <div class="col-sm-12 col-md-6 mb-2 offset-md-3 text-center">
                        <label>Imagen de portada</label>
                        <img id="img_portada" src="<?php echo (isset($F) && $F['img_portada'] != null)?$F['img_portada']:'../static/img/portada_vehiculo.jpeg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen de portada" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFilePortada" onchange="subirImg(this);" name="img_portada" <?php if(isset($F) == false) echo 'required';?> >
                            <label class="custom-file-label" for="customFilePortada" style="width:100%"></label>
                        </div>
                    </div>
                    </div>

                <div class="row">
                    <div class="col-sm-12 col-md-2 mb-2 offset-md-1">
                        <img id="img1" src="<?php echo (isset($F) && $F['img1'] != null)?$F['img1']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 1" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile1" onchange="subirImg1(this);" name="img1" <?php if(isset($F) == false) echo 'required';?> >
                            <label class="custom-file-label" for="customFile1" style="width:100%"></label>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2 mb-2">
                        <img id="img2" src="<?php echo (isset($F) && $F['img2'] != null)?$F['img2']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 2" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile2" onchange="subirImg2(this);" name="img2">
                            <label class="custom-file-label" for="customFile2" style="width:100%"></label>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2 mb-2">
                        <img id="img3" src="<?php echo (isset($F) && $F['img3'] != null)?$F['img3']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 3" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile3" onchange="subirImg3(this);" name="img3">
                            <label class="custom-file-label" for="customFile3" style="width:100%"></label>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2 mb-2">
                        <img id="img4" src="<?php echo (isset($F) && $F['img4'] != null)?$F['img4']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 4" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile4" onchange="subirImg4(this);" name="img4">
                            <label class="custom-file-label" for="customFile4" style="width:100%"></label>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2 mb-2">
                        <img id="img5" src="<?php echo (isset($F) && $F['img5'] != null)?$F['img5']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 5" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile5" onchange="subirImg5(this);" name="img5">
                            <label class="custom-file-label" for="customFile5" style="width:100%"></label>
                        </div>
                    </div>


                    <div class="col-sm-12 col-md-2 mb-2 offset-md-1">
                        <img id="img6" src="<?php echo (isset($F) && $F['img6'] != null)?$F['img6']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 1" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input"  id="customFile6" onchange="subirImg(this);" name="img6">
                            <label class="custom-file-label" for="customFile6" style="width:100%"></label>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2 mb-2">
                        <img id="img7" src="<?php echo (isset($F) && $F['img7'] != null)?$F['img7']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 2" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input"  id="customFile7" onchange="subirImg(this);" name="img7">
                            <label class="custom-file-label" for="customFile7" style="width:100%"></label>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2 mb-2">
                        <img id="img8" src="<?php echo (isset($F) && $F['img8'] != null)?$F['img8']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 3" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input"  id="customFile8" onchange="subirImg(this);" name="img8">
                            <label class="custom-file-label" for="customFile8" style="width:100%"></label>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2 mb-2">
                        <img id="img9" src="<?php echo (isset($F) && $F['img9'] != null)?$F['img9']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 4" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input"  id="customFile9" onchange="subirImg(this);" name="img9">
                            <label class="custom-file-label" for="customFile9" style="width:100%"></label>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2 mb-2">
                        <img id="img10" src="<?php echo (isset($F) && $F['img10'] != null)?$F['img10']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 5" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input"  id="customFile10" onchange="subirImg(this);" name="img10">
                            <label class="custom-file-label" for="customFile10" style="width:100%"></label>
                        </div>
                    </div>
                    <?php

                              for($i=11;$i<=20;$i++){
                                  if($i==11||$i==16)
                                      $offset = " offset-md-1";
                                  else
                                      $offset = "";
                    ?>

                        <div class="col-sm-12 col-md-2 mb-2 <?php echo $offset;?>">
                        <img id="img<?php echo $i;?>" src="<?php echo (isset($F) && $F['img'.$i] != null)?$F['img'.$i]:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen <?php echo $i;?>" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input"  id="customFile<?php echo $i;?>" onchange="subirImg(this);" name="img<?php echo $i;?>">
                            <label class="custom-file-label" for="customFile<?php echo $i;?>" style="width:100%"></label>
                        </div>
                    </div>
                    <?php
                              }

                    ?>

                </div>
			</div>
			<div class="modal-footer">
				<button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-b"><?php echo ((isset($F))?'Guardar Cambios':'Guardar')?></button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

	function subirImg(input) {
            console.log(input.name);
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$("#"+input.name).attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	function subirImg1(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img1')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	function subirImg2(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img2')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	function subirImg3(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img3')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	function subirImg4(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img4')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	function subirImg5(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img5')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
