<?php
  include_once("modelo/Cliente.php");
  $cliente = new Cliente();
  $idc = $_GET['id'];

  
  $CLI = $cliente->findById($idc);
  if($CLI == false){
    echo "<script>window.location = 'index.php';</script>";
    exit(1);
  }else{
    $vehiculos = $cliente->getVehiculos($CLI['id']);
  }

?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Perfil del cliente - <?php echo $CLI['ced_usu'];?></h4>
		
		<div class="text-right">

		</div>
	</div>

	<div class="card-body">
          <form class="form-a" method="POST" action="" enctype="multipart/form-data" id="">
            <div class="row">
              <div class="col-md-2">
                <img src="<?php echo $CLI['img_usu'];?>" style="width:100px;" class="img-profile rounded-circle"> 
              </div>
              <div class="col-md-6">
                <h5><?php echo $CLI['nom_usu']." ".$CLI['ape_usu']." - ".$CLI['cor_usu'];?></h5>
                <hr>
                <b>Télefono: </b>
                <span><?php echo $CLI['tel_usu'];?></span>
                <hr>
                <b>Contraseña de acceso: </b>
                <span><?php echo $CLI['pas_usu'];?></span>
                <hr>
                <b>Fecha de registro: </b>
                <span><?php echo $CLI['fec_reg_usu'];?></span>
              </div>
              <div class="col-md-4">
                <b>Fecha de nacmiento: </b>
                <span><?php echo $CLI['fec_nac_usu'];?></span>
                <hr>

                <b>Facebook: : </b>
                <span><?php echo $CLI['facebook'];?></span>
                <hr>
                <b>Twitter:  </b>
                <span><?php echo $CLI['twitter'];?></span>
                <hr>
                <b>Instagram:  </b>
                <span><?php echo $CLI['instagram'];?></span>
              </div>
            </div>
            <div class="row" style="margin-top:2em;">
              <div class="col-md-12 text-center">
                <h5 style="background:#f44336;color:#fff;padding:0.5em;">Vehículos de este usuario</h5>
                <table class="table table-stripped">
                  <tr>
                    <th># </th>
                    <th>Modelo </th>
                    <th>Placa </th>
                    <th>Serial </th>
                    <th>Color </th>
                    <th>Tipo </th>
                    <th>Año </th>
                  </tr>
                  <?php
                    if($vehiculos->num_rows == 0){
                  ?>
                  <tr>
                    <th colspan="7">Este usuario no tiene ningún vehículo registrado</th>
                  </tr>
                  <?php }?>
                  <?php
                    $n = 0;
                    while($veh = $vehiculos->fetch_assoc()){
                      $n++;
                      echo "<tr>";
                      echo "<td>".$n."</td>";
                      echo "<td>".$veh['modelo']."</th>";
                      echo "<td>".$veh['placa']."</th>";
                      echo "<td>".$veh['serial1']."</th>";
                      echo "<td>".$veh['serial2']."</th>";
                      echo "<td>".$veh['serial3']."</th>";
                      echo "<td>".$veh['ano']."</th>";
                      echo "</tr>";
                    }
                  ?>
                </table>
              </div>
            </div>
          </form>
    </div>
</div>
