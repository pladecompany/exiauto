<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Slider</h4>
		
		<div class="text-right">
			<a href="#md-slider" data-toggle="modal" class="color-b modal-trigger"><b><i class="fa fa-plus-circle"></i> Registrar slider</b></a>
		</div>
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Imagen</th>
						<th>Título</th>
						<th>Descripción</th>
						<th>Acciones</th>
					</tr>
				</thead>

				<tfoot>
					<tr>
						<th>#</th>
						<th>Imagen</th>
						<th>Título</th>
						<th>Descripción</th>
						<th>Acciones</th>
					</tr>
				</tfoot>

				<tbody>
					<tr>
						<td>#</td>
						<td><img src="../static/img/slider.jpg" width="100px"></td>
						<td>Título</td>
						<td>Descripción</td>
						<td>
							<a href="" class="btn btn-b-n"><i class="fa fa-eye"></i></a>
							<a href="" class="btn btn-b-n"><i class="fa fa-edit"></i></a>
							<a href="" class="btn btn-b-n"><i class="fa fa-trash"></i></a>
						</td>
					</tr>

					<tr>
						<td>#</td>
						<td><img src="../static/img/slider.jpg" width="100px"></td>
						<td>Título</td>
						<td>Descripción</td>
						<td>
							<a href="" class="btn btn-b-n"><i class="fa fa-eye"></i></a>
							<a href="" class="btn btn-b-n"><i class="fa fa-edit"></i></a>
							<a href="" class="btn btn-b-n"><i class="fa fa-trash"></i></a>
						</td>
					</tr>

					<tr>
						<td>#</td>
						<td><img src="../static/img/slider.jpg" width="100px"></td>
						<td>Título</td>
						<td>Descripción</td>
						<td>
							<a href="" class="btn btn-b-n"><i class="fa fa-eye"></i></a>
							<a href="" class="btn btn-b-n"><i class="fa fa-edit"></i></a>
							<a href="" class="btn btn-b-n"><i class="fa fa-trash"></i></a>
						</td>
					</tr>

					<tr>
						<td>#</td>
						<td><img src="../static/img/slider.jpg" width="100px"></td>
						<td>Título</td>
						<td>Descripción</td>
						<td>
							<a href="" class="btn btn-b-n"><i class="fa fa-eye"></i></a>
							<a href="" class="btn btn-b-n"><i class="fa fa-edit"></i></a>
							<a href="" class="btn btn-b-n"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="md-slider" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
					<h3 class="title-d">Slider</h3>
				</div>

				<form class="form-a">
					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Imagen">Selecciona la imagen</label>
								<input type="file" class="form-control form-control-lg form-control-a">
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Título">Título</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Escribe el título">
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="descripción">Breve descripción</label>
								<textarea class="form-control form-control-lg form-control-a" placeholder="Escribe una breve descripción" style="height: 100px;"></textarea>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-b">Guardar</button>
			</div>
		</div>
	</div>
</div>
