<?php
  include_once("controlador/clientes.php");
?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Clientes</h4>
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Cédula/Rif</th>
						<th>Nombre y apellido</th>
						<th>Teléfono</th>
						<th>Registro</th>
						<th>Contraseña</th>
						<th>#</th>
					</tr>
				</thead>
				<tbody>
                  <?php
                    $noti = new Cliente();
                    $r = $noti->fetchAll();
                    $i=0;
                    while($ff = $r->fetch_assoc()){
                      $i++;
                      echo "<tr>";
                      echo "  <td>" . $i . "</td>";
                      echo "  <td>" . $ff['ced_usu'] . "</td>";
                      echo "  <td>" . $ff['nom_usu'] . " " . $ff['ape_usu']."<br>".$ff['cor_usu']."</td>";
                      echo "  <td>" . $ff['tel_usu'] . "</td>";
                      echo "  <td>" . $ff['fec_reg_usu'] . "</td>";
                      echo "  <td title='".$ff['pas_usu']."'>****</td>";
                      echo "<td><a target='__blank' href='?op=perfil_cliente&id=".$ff['id']."'><i class='mr-2 fa fa-eye'></i></a>";
                      //echo "<a href='?op=modelos&el=".$ff['id']."' onclick='return confirm(\"¿ Esta seguro ?\")'><i class='mr-2 fa fa-trash'></i></a>";
                      echo "</td>";
                      echo "</tr>";
                    }
                  ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="md-slider" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
                    <?php if(isset($F)){ ?>
					<h3 class="title-d" id="titulo_modulo">Editar modelo</h3>
                    <?php }else{?>
					<h3 class="title-d" id="titulo_modulo">Nuevo modelo</h3>
                    <?php }?>
				</div>

				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
                    <?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Modelo">Modelo</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Escribe el modelo" name="mod" value="<?php echo $F['modelo'];?>">
							</div>
						</div>
					</div>
                    <div class="modal-footer">
                        <button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-b"><?php echo ((isset($F))?'Guardar Cambios':'Guardar')?></button>
                    </div>
				</form>
			</div>


		</div>
	</div>
</div>

<?php
  if(isset($F)){
?>
  <script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").trigger('click');
    });
  </script>

<?php
  } 
?>
<script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nueva modelo");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Guardar');
        $("input[name='tit']").val('');
        $("textarea[name='des']").val('');
        $("select[name='est']").val('');
        $("#cont_img").remove();
      });
    });

</script>

