
<?php
  include_once("controlador/repuestos.php");
?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Repuestos</h4>
		
		<div class="text-right">
			<a href="?op=nuevo_repuesto" class="color-b"><b><i class="fa fa-plus-circle"></i> Registrar repuesto</b></a>
		</div>
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th>Categoría</th>
						<th>precio</th>
						<th>Estatus</th>
						<th>Acciones</th>
					</tr>
				</thead>

				<tbody>
                    <?php
                      $veh = new Repuesto();
                      $rm = $veh->fetchAll();
                      $i = 0;
                      while($fm = $rm->fetch_assoc()){
                        $i++;
                    ?>
                      <tr>
                          <td><?php echo $i;?></td>
                          <td><?php echo $fm['nom_rep'];?></td>
                          <td><?php echo $fm['nom_cat'];?></td>
                          <td><?php echo $fm['pre_rep'];?></td>
                          <td><?php echo ($fm['est_rep']==1)?'Visible':'No visible';?></td>
                          <td>
                              <a href='?op=nuevo_repuesto&id=<?php echo $fm['idr'];?>'><i class='mr-2 fa fa-edit'></i></a>
                              <a href="?op=nuevo_repuesto&el=<?php echo $fm['idr'];?>" onclick="return confirm('¿ Está seguro ?');"><i class='mr-2 fa fa-trash'></i></a>
                          </td>
                      </tr>

                    <?php
                      }
                    ?>

				</tbody>
			</table>
		</div>
	</div>
</div>
