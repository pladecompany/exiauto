<?php
    session_start();
    if(!isset($_SESSION) || isset($_SESSION['login']) != true){
      if(!isset($_SESSION['log'])){
        echo json_encode(Array("error"=>"401"));
        exit(1);
      }
    }
    header('Content-type: application/json');
    include_once("modelo/Agente.php");
    include_once("modelo/Citas.php");
    include_once("modelo/Cliente.php");
    include_once("modelo/Sms.php");
    
    $modulo = $_POST['modulo'];
    $tipo =  $_POST['tipo'];

    if($modulo == 'citas'){
      
      if($tipo == "cambiarEstatus"){
        $cita = new Cita();
        $est = $_POST['valor'];
        $idc = $_POST['idc'];
        $obs = $_POST['obs'];
        $rep = $_POST['reprogramado'];
        $nueva_fecha = $_POST['nueva_fecha'];


        $r = $cita->cambiarEstatus($est, $idc, $obs);
        if($r){

          if(intval($rep) == 1){
            $cita->reprogramarCita($idc, $nueva_fecha, $obs);
          }

          $fcita = $cita->findById($idc);
          $usu = new Cliente();
          $cli = $usu->findById($fcita['id_usuario']);

          $r_citas = $cita->citasPorMes($_POST['est'], $_POST['ida'], $_POST['mes'], $_POST['ano']);
          while($f = $r_citas->fetch_assoc()) $a[] = $f;

          $fh = explode(" ", $fcita['fecha']);
          $f = explode("-", $fh[0]);
          $h = $fh[1];

          if($obs == "") $obs = "Ningúna";
          $sms = "";
          $diacita = $f[2]."/".$f[1]." a las ".$h;
          $full = $cita->findByIdFull($idc);
          if($est == 1){
            $msj = "Su solicitud fue aprobada, debe asistir al concesionario el día <b>".$f[2]."/".$f[1]."</b> del <b>".$f[0]."</b> a las ".$h;
            $sms = "Hola ".$full['nom_usu']." EXIAUTO le informa que su solicitud de cita fue aprobada, asista al concesionario el día $diacita";
            $msj.= "<br><hr><br>";
            $msj.= "<b>Asesor de trabajo: </b>" . $full['nom_age']." ".$full['ape_age']." - (".$full['cod_age'].") <br>";
            $msj.= "<b>Datos del vehículo: </b>" . $full['placa']." ".$full['serial1']." - (".$full['ano'].") - Modelo: " . $full['modelo']." <br>";
            $msj.= "<b>Tipo de la cita: </b>" . $full['tipo_cita'] ." <br>";
            $msj.= "<b>Falla del vehículo: </b>" . $full['falla'] ." <br>";
            $msj.= "<b>Motivo de la cita: </b>" . $full['motivo']." <br>";
            $msj.= "<b>Observación por parte de la administración: </b><br>" . $obs." <br>";
            $msj.= "<br><hr><br>";
            $asunto = "Solicitud aprobada";
            //$rc = $cita->enviarCorreo($cli["cor_usu"], $msj, $asunto);
          }else if($est == -1){
            $msj = "Su solicitud de cita fue cancelada por el administrador, comuniquese al 0212-2030911 para más información. ";;
            $sms = "Hola ".$full['nom_usu']." EXIAUTO le informa que su solicitud de cita para el día $diacita fue cancelada, comuniquese al 0212-2030911 para más información";
            $msj.= "<br><br><b>Observación por parte de la administración: </b><br>" . $obs." <br>";
            $asunto = "Solicitud cancelada";
            //$rc = $cita->enviarCorreo($cli["cor_usu"], $msj, $asunto);
          }else if($est == -2){
            $msj = "Su solicitud fue rechazada por el administrador, comuniquese al 0212-2030911 para más información. ";;
            $sms = "Hola ".$full['nom_usu']." EXIAUTO le informa que su solicitud de cita para el día $diacita fue rechazada, comuniquese al 0212-2030911 para más información";
            $msj.= "<br><br><b>Observación por parte de la administración: </b><br>" . $obs." <br>";
            $asunto = "Solicitud rechazada";
            //$rc = $cita->enviarCorreo($cli["cor_usu"], $msj, $asunto);
          }
          echo json_encode(array("r"=>true, "msj"=>"El estatus de la solicitud se actualizo correctamente", "cita"=>$fcita, "eventos"=>$a));
          $txt=$sms;
          //$sms = new Sms();
          $cita->enviarCorreo($cli["cor_usu"], $msj, $asunto);
          //$sms->enviarSms($cli['tel_usu'], $txt);
        }else
          echo json_encode(array("r"=>false, "msj"=>"No se pudo actualizar el estatus"));
        exit(1);
      }else if($tipo=='obtenerCita'){
        $idc = $_POST['idc'];
        $cita = new Cita();
        $fcita = $cita->findByIdFull($idc);
        if($fcita == false)
          echo json_encode(array("r"=>false, "msj"=>"No se encontro la cita con el ID($idc)"));
        else
          echo json_encode(array("r"=>true, "msj"=>"Cita agendada correctamente", "cita"=>$fcita));
        exit(1);
      }else if($tipo == "nuevaCita"){
          $age = new Agente();
          $ida = $_POST['ida'];
          $idv = $_POST['idv'];
          $idc = $_POST['idc'];
          $mot = $_POST['mot'];
          $fec = $_POST['fec'];
          $tip = $_POST['tip'];
          $kil = $_POST['kil'];
          if($kil == "")
            $kil = 1;
          $rev = $_POST['rev'];
          if($rev == "")
            $rev = "201";

          if($age->validarHorario($ida, $fec)){
            echo json_encode(array("r"=>false, "msj"=>"El asesor seleccionado, no esta disponible en la fecha seleccionada ($fec)", "cita"=>$fcita));
            exit(1);
          }

          $cita = new Cita();
          $cita->data["id"] = '';
          $cita->data["id_usuario"] = $idc;
          $cita->data["id_vehiculo"] = $idv;
          $cita->data["id_agente"] = $ida;
          $cita->data["motivo"] = $mot;
          $cita->data["fecha"] = $fec;
          $cita->data["estatus"] = '1';
          $cita->data["fec_env"] = date('Y-m-d H:i:s');
          $cita->data["fac_res"] = null;
          $cita->data["tipo_cita"] = $tip;
          $cita->data["id_kilometros"] = $kil;
          $cita->data["id_falla"] = $rev;
          $cita->data["observacion"] = "";
          $cita->data["reprogramado"] = "0";

          $r = $cita->save();
          if($r){
            $fcita = $cita->findByIdFull($r->insert_id);
            echo json_encode(array("r"=>true, "msj"=>"Cita agendada correctamente", "cita"=>$fcita));
          }else{
            echo json_encode(array("r"=>false, "msj"=>"No se pudo agendar la cita"));
          }
      }else if($tipo == 'validarHorario'){
        $age = new Agente();
        $ida = $_POST['ida'];
        $fec = $_POST['fec'];
        if($age->validarHorario($ida, $fec)){
          echo json_encode(array("r"=>false, "msj"=>"El asesor seleccionado, no esta disponible en la fecha seleccionada ($fec)"));
          exit(1);
        }else{
          echo json_encode(array("r"=>true));
          exit(1);
        }
      }
    }else if($modulo == 'repuestos'){
      include_once("modelo/Repuesto.php");
      if($tipo == "obtenerSolicitud"){
        $ids = $_POST['ids'];
        $re = new Repuesto();
        $f = $re->findSolicitud($ids);
        if($f!=false){
          echo json_encode(array("r"=>true, "msj"=>"", "sol"=>$f));
        }else{
          echo json_encode(array("r"=>false, "msj"=>"No existe solicitud con el ID($ids)", "sol"=>null));
        }
        exit(1);
      }

    }else if($modulo == 'clientes'){
      include_once("modelo/MiVehiculo.php");
      $veh = new MiVehiculo();
      if($tipo == "consultarVehiculos"){
        $idc = $_POST['idc'];
        $r = $veh->misVehiculos($idc);
        $a = [];
        while($f = $r->fetch_assoc()) $a[] = $f;
        echo json_encode(array("r"=>true, "vehiculos"=>$a));
      }
    }else if($modulo=='vehiculos'){
      $placa = $_POST['placa'];
      if($tipo == "validarPlaca"){
        include_once("modelo/MiVehiculo.php");
        $veh = new MiVehiculo();
        $r = $veh->findByPlaca($placa);
        if($r == false){
          echo json_encode(array("r"=>false));
        }else{
          echo json_encode(array("r"=>true, "vehiculo" => $r));
        }
      }
    }
?>
