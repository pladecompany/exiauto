<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	include_once('ruta.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<title>EXIAUTO</title>

	<link href="../static/img/favicon.png" rel="icon">
	<link href="../static/img/apple-touch-icon.png" rel="apple-touch-icon">

	<link href="../static/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="../static/css/panel.css" rel="stylesheet" type="text/css">
	<link href="../static/css/style.css" rel="stylesheet" type="text/css">
</head>


<body class="bg-gradient-primary">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-12 col-md-9">
				<div class="card o-hidden border-0 shadow-lg my-5">
					<div class="card-body p-0">
						<div class="row">
							<div class="col-lg-12">
								<div class="p-5">
									<form class="form-a">
										<div class="row">
											<div class="col-md-12 mb-2">
												<div class="title-box-d">
													<h3 class="title-d">Recuperar</h3>
												</div>
											</div>

											<div class="col-md-12 mb-2">
												<div class="form-group">
													<label for="Contraseña">Ingresa tu correo electrónico</label>
													<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo electrónico">
												</div>
											</div>

											<div class="col-md-12">
												<button type="submit" class="btn btn-b">Enviar</button>
											</div>
										</div>
									</form>

									<hr>
									<div class="text-center">
										<a class="small" href="login.php">Iniciar sesión</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="../static/js/jquery.min.js"></script>
	<script src="../static/js/bootstrap.bundle.min.js"></script>
	<script src="../static/js/jquery.easing.min.js"></script>
	<script src="../static/js/sb-admin-2.min.js"></script>
</body>
</html>
