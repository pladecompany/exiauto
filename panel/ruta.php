<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	
	$opcion = $_GET['op'];
	switch ($opcion) {
		case "reclamos":
			$ruta="vistas/reclamos.php";
		break;
		case "ver_reclamo":
			$ruta="vistas/ver_reclamo.php";
		break;
		case "asesores":
			$ruta="vistas/asesores.php";
		break;
		case "admins":
			$ruta="vistas/admins.php";
		break;
		case "inicio":
			$ruta="vistas/inicio.php";
		break;
		case "citas":
			$ruta="vistas/citas.php";
		break;
		case "solicitudes":
			$ruta="vistas/solicitudes.php";
		break;
		case "configuraciones":
			$ruta="vistas/configuraciones.php";
		break;

		case "configuraciones_imagenes":
			$ruta="vistas/configuraciones_imagenes.php";
		break;

		case "noticias":
			$ruta="vistas/noticias.php";
		break;
		case "clientes":
			$ruta="vistas/clientes.php";
		break;
		case "perfil_cliente":
			$ruta="vistas/perfil_cliente.php";
		break;
		case "vehiculos":
			$ruta="vistas/vehiculos.php";
		break;
		case "repuestos":
			$ruta="vistas/repuestos.php";
		break;
		case "nuevo_repuesto":
			$ruta="vistas/nuevo_repuesto.php";
		break;
		case "modelos":
			$ruta="vistas/modelos.php";
		break;
		case "categorias":
			$ruta="vistas/categorias.php";
		break;

		case "vehiculo":
			$ruta="vistas/vehiculo.php";
		break;
		case "solicitud_repuestos":
			$ruta="vistas/solicitud_repuestos.php";
		break;

		case "slider":
			$ruta="vistas/slider.php";
		break;

		case "cambiarcontraseña":
			$ruta="vistas/cambiarcontraseña.php";
		break;

		default:
			header('Location: ?op=inicio');
		break;
	}
?>
