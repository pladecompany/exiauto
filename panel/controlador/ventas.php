<?php
  include_once("modelo/Factura.php"); 
  include_once("modelo/Orm.php"); 
  include_once("modelo/Conexion.php"); 

  if(!isset($_GET['id'])){
    $fac = new Factura(); 
    $all = $fac->fetchAll();
  }

  if(isset($_GET['id'])){
    $fac = new Factura();
    $FF = $fac->findById($_GET['id']);
    if($FF == false){
      echo "<script>window.location ='?op=ventas&err&msj=Ésta factura no existe.';</script>";
      exit(1);
    }
    $all = $fac->fetchDetalles($_GET['id']);
  }


?>
