<?php
  include_once("modelo/Orm.php"); 

  if(isset($_POST['bt_promo'])){

    if(!empty($_FILES['img_1'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_1']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['img_1']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET img_slider_1='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }


    if(!empty($_FILES['img_2'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_2']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['img_2']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET img_slider_2='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }

    if(!empty($_FILES['img_3'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_3']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['img_3']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET img_slider_3='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }

    if(!empty($_FILES['img_4'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_4']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['img_4']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET img_slider_4='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }


    if(!empty($_FILES['img_servicio_1'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_servicio_1']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['img_servicio_1']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET img_servicio_1='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }


    if(!empty($_FILES['img_servicio_2'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_servicio_2']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['img_servicio_2']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET img_servicio_2='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }


    if(!empty($_FILES['img_servicio_3'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_servicio_3']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['img_servicio_3']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET img_servicio_3='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }


    if(!empty($_FILES['banner'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['banner']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['banner']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET banner='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }


    if(!empty($_FILES['banner_2'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['banner_2']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['banner_2']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET banner_2='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }


    if(!empty($_FILES['banner_3'])){
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['banner_3']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

      if(move_uploaded_file($_FILES['banner_3']['tmp_name'], $nombre)) {
          $sql = "UPDATE imagenes_pagina SET banner_3='$nf' WHERE id=1;";
          $r = $orm->editarPersonalizado($sql);
      }
    }

    $err = "¡Cambios realizados correctamente!";
    echo "<script>window.location ='?op=configuraciones_imagenes&info&msj=$err';</script>";
    exit(1);
  }else if(isset($_POST['bt_promo_servicio'])){

      $id_seccion = $_POST['bt_promo_servicio'];

      if(!empty($_FILES['img_1'])){
          $ruta = getcwd() . "/../static/img/files/";
          $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_1']['name']); 
          $nom1 = str_replace(" ", "_", $nom1);
          $nombre = $ruta . $nom1;
          $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

          if(move_uploaded_file($_FILES['img_1']['tmp_name'], $nombre)) {
              $sql = "UPDATE imagenes_pagina SET img_slider_1='$nf' WHERE id=$id_seccion;";
              $r = $orm->editarPersonalizado($sql);
          }
      }


      if(!empty($_FILES['img_2'])){
          $ruta = getcwd() . "/../static/img/files/";
          $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_2']['name']); 
          $nom1 = str_replace(" ", "_", $nom1);
          $nombre = $ruta . $nom1;
          $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

          if(move_uploaded_file($_FILES['img_2']['tmp_name'], $nombre)) {
              $sql = "UPDATE imagenes_pagina SET img_slider_2='$nf' WHERE id=$id_seccion;";
              $r = $orm->editarPersonalizado($sql);
          }
      }

      if(!empty($_FILES['img_3'])){
          $ruta = getcwd() . "/../static/img/files/";
          $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_3']['name']); 
          $nom1 = str_replace(" ", "_", $nom1);
          $nombre = $ruta . $nom1;
          $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

          if(move_uploaded_file($_FILES['img_3']['tmp_name'], $nombre)) {
              $sql = "UPDATE imagenes_pagina SET img_slider_3='$nf' WHERE id=$id_seccion;";
              $r = $orm->editarPersonalizado($sql);
          }
      }

      if(!empty($_FILES['img_4'])){
          $ruta = getcwd() . "/../static/img/files/";
          $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img_4']['name']); 
          $nom1 = str_replace(" ", "_", $nom1);
          $nombre = $ruta . $nom1;
          $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;

          if(move_uploaded_file($_FILES['img_4']['tmp_name'], $nombre)) {
              $sql = "UPDATE imagenes_pagina SET img_slider_4='$nf' WHERE id=$id_seccion;";
              $r = $orm->editarPersonalizado($sql);
          }
      }

    $err = "¡Cambios realizados correctamente!";
    echo "<script>window.location ='?op=configuraciones_imagenes&info&msj=$err';</script>";
    exit(1);
  }
?>
