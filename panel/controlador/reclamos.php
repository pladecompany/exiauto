<?php
  include_once("panel/modelo/Reclamo.php"); 
  include_once("panel/modelo/Modelo.php"); 
  include_once("panel/modelo/ModeloCitas.php"); 
  include_once("panel/modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $tip = $_POST['tip'];
    $des = $_POST['des'];
    $dep = $_POST['dep'];

    if(strlen($tip) == 0){
      $err = "Tipo: Debe llenar el campo tipo.";
    }else if(strlen($des) < 10){
      $err = "Descripción: Debe tener minímo 10 caracteres.";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=reclamos&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Reclamo();
    $cliente->data["id"] = "";
    $cliente->data["id_usuario"] = $_SESSION['idu'];
    $cliente->data["fec_reg_rec"] = date('Y-m-d H:i:s');
    $cliente->data["mensaje"] = $des;
    $cliente->data["est_rec"] = "0";
    $cliente->data["tipo"] = $tip;
    $cliente->data["id_departamento"] = $dep;

    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=reclamos&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "No se pudo registrar su reclamo/sugerencia";
      echo "<script>window.location ='?op=reclamos&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btg_comentario_admin'])){
    $cliente = new Reclamo();
    $idr = $_POST['idr'];
    $com = $_POST['des'];
    $ida = $_SESSION['idu'];
    $idu = null;

    $r = $cliente->nuevoComentario($idr, $idu, $ida, $com);
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      $err = "¡Registró el comentario correctamente!";
      echo "<script>window.location ='?op=ver_reclamo&id=$idr&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "No se pudo registrar su reclamo/sugerencia";
      echo "<script>window.location ='?op=ver_reclamo&id=$idr&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btg_comentario'])){
    $cliente = new Reclamo();
    $idr = $_POST['idr'];
    $com = $_POST['des'];
    $idu = $_SESSION['idu'];
    $ida = null;

    $r = $cliente->nuevoComentario($idr, $idu, $ida, $com);
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      $err = "¡Registró el comentario correctamente!";
      echo "<script>window.location ='?op=ver_reclamo&id=$idr&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "No se pudo registrar su reclamo/sugerencia";
      echo "<script>window.location ='?op=ver_reclamo&id=$idr&err&msj=$err';</script>";
      exit(1);
    }



  }else if(isset($_POST) && isset($_POST['btc'])){

    $idn = $_POST['idn'];
    $tip = $_POST['tip'];
    $des = $_POST['des'];

    if(strlen($tip) == 0){
      $err = "Tipo: Debe llenar el campo tipo.";
    }else if(strlen($des) < 10){
      $err = "Descripción: Debe tener minímo 10 caracteres.";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=reclamos&err&msj=$err';</script>";
      exit(1);
    }

    $id = $idn;
    if(isset($err)){
      echo "<script>window.location ='?op=reclamos&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Reclamo();

    $cliente->data["id_modelo"] = $mod;
    $cliente->data["placa"] = $pla;
    $cliente->data["serial1"] = $se1;
    $cliente->data["serial2"] = $se2;
    $cliente->data["serial3"] = $se3;
    $cliente->data["serial4"] = $se4;
    $cliente->data["ano"] = $ano;

    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=reclamos&id=$id&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=reclamos&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Reclamo();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=reclamos&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el_com'])){
    $cliente = new Reclamo();
    $idr = $_GET['idr'];
    $id = $_GET['el_com'];
    if($cliente->removeComentarioById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=ver_reclamo&id=$idr&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=ver_reclamo&id=$idr&err&msj=$err';</script>";
    }
    exit(1);
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Reclamo();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=reclamos&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=reclamos&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
