<?php
  include_once("modelo/Asesor.php"); 
  include_once("modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $cod = $_POST['cod'];
    $nom = $_POST['nom'];
    $ape = $_POST['ape'];
    $tlf = $_POST['tlf'];
    $est = $_POST['est'];

    if(strlen($cod) == 0){
      $err = "Debe llenar el campo titulo.";
    }else if(strlen($nom) < 2){
      $err = "El campo nombre debe tener al menos 2 carácteres";
    }else if($est == ''){
      $err = "Debe seleccionar el estatus";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=asesores&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Asesor();

    $cliente->data["id"] = "";
    $cliente->data["cod_age"] = $cod;
    $cliente->data["nom_age"] = $nom;
    $cliente->data["ape_age"] = $ape;
    $cliente->data["tlf_age"] = $tlf;
    $cliente->data["img_age"] = "";
    $cliente->data["est_age"] = $est;

    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      
      if(!empty($_FILES['img'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
        $nom1 = str_replace(" ", "_", $nom1);
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
          $producto = new Asesor();
          $producto->data['img_age'] = $nf;
          $producto->edit($id);
        }
      }
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=asesores&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=asesores&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $idn = $_POST['idn'];
    $cod = $_POST['cod'];
    $nom = $_POST['nom'];
    $ape = $_POST['ape'];
    $tlf = $_POST['tlf'];
    $est = $_POST['est'];

    if(strlen($cod) == 0){
      $err = "Debe llenar el campo codígo.";
    }else if(strlen($nom) < 2){
      $err = "El campo nombre debe tener al menos 2 carácteres";
    }else if($est == ''){
      $err = "Debe seleccionar el estatus";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=asesores&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Asesor();
    $cliente->data["id"] = $idn;
    $cliente->data["cod_age"] = $cod;
    $cliente->data["nom_age"] = $nom;
    $cliente->data["ape_age"] = $ape;
    $cliente->data["tlf_age"] = $tlf;
    $cliente->data["est_age"] = $est;

    if(!empty($_FILES['img'])){
      $orm = new Orm(new Conexion());
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
      if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
        $cliente->data["img_age"] = $nf;
      }
    }

    $id = $_POST['idn'];
    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=asesores&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=asesores&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Asesor();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=asesores&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Asesor();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=asesores&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=asesores&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
