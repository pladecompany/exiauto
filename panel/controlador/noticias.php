<?php
  include_once("modelo/Noticia.php"); 
  include_once("modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $tit = $_POST['tit'];
    $des = $_POST['des'];
    $est = $_POST['est'];

    if(strlen($tit) == 0){
      $err = "Debe llenar el campo titulo.";
    }else if(strlen($des) < 10){
      $err = "El campo descripción debe tener al menos 10 carácteres";
    }else if(strlen($est) == 0){
      $err = "Debe seleccionar el estatu";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=noticias&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Noticia();

    $cliente->data["id"] = "";
    $cliente->data["titulo"] = $tit;
    $cliente->data["descripcion"] = htmlentities($des);
    $cliente->data["img"] = "";
    $cliente->data["fec_reg_noti"] = date("Y-m-d H:i:s");
    $cliente->data["est_noti"] = $est;

    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      
      if(!empty($_FILES['img'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
        $nom1 = str_replace(" ", "_", $nom1);
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
          $producto = new Noticia();
          $producto->data['img'] = $nf;
          $producto->edit($id);
        }
      }
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=noticias&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=noticias&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $idn = $_POST['idn'];
    $tit = $_POST['tit'];
    $des = $_POST['des'];
    $est = $_POST['est'];

    if(strlen($tit) == 0){
      $err = "Debe llenar el campo titulo.";
    }else if(strlen($des) < 10){
      $err = "El campo descripción debe tener al menos 10 carácteres";
    }else if(strlen($est) == 0){
      $err = "Debe seleccionar el estatu";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=noticias&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Noticia();

    $cliente->data["id"] = $idn;
    $cliente->data["titulo"] = $tit;
    $cliente->data["descripcion"] = htmlentities($des);
    $cliente->data["est_noti"] = $est;

    if(!empty($_FILES['img'])){
      $orm = new Orm(new Conexion());
      $ruta = getcwd() . "/../static/img/files/";
      $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img']['name']); 
      $nom1 = str_replace(" ", "_", $nom1);
      $nombre = $ruta . $nom1;
      $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
      if(move_uploaded_file($_FILES['img']['tmp_name'], $nombre)) {
        $cliente->data["img"] = $nf;
      }
    }

    $id = $_POST['idn'];
    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=noticias&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=noticias&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Noticia();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=noticias&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Noticia();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=noticias&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=noticias&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
