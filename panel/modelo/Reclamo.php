<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Reclamo{
    private $tabla = "reclamos";
    public $data = [];
    public $orm = null;

    public function Reclamo(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function removeComentarioById($id){
      return $this->orm->eliminar('id', $id, "comentarios_reclamos");
    }

    public function findById($id){
      $sql = "SELECT *, R.id as idr from ".$this->tabla." R, usuarios U, departamentos D WHERE R.id_departamento=D.id AND R.id_usuario=U.id AND R.id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT *, V.id as idv FROM ".$this->tabla." V, modelos_citas M WHERE V.id_modelo=M.id  ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function listarDepartamentos(){
      $sql = "SELECT * FROM departamentos ORDER BY nom_dep";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function listarComentarios($idr, $idu){
      $sql = "SELECT * FROM comentarios_reclamos WHERE id_reclamo='$idr' ORDER BY fec_com;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function misReclamos($idu){
      $sql = "SELECT *, R.id as idr from ".$this->tabla." R, usuarios U WHERE R.id_usuario=U.id AND R.id_usuario='$idu' ORDER BY R.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function reclamosGenerales($primero, $ultimo){
      $sql = "SELECT *, R.id as idr from ".$this->tabla." R, usuarios U, departamentos D WHERE R.id_departamento=D.id AND R.id_usuario=U.id AND (R.fec_reg_rec>='$primero 00:00:00' AND R.fec_reg_rec<='$ultimo 23:59:59') ORDER BY R.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function verificarVehiculo($idu, $idv){
      $sql = "SELECT *, V.id as idv FROM " . $this->tabla . " V, modelos_citas M WHERE V.id='$idv' AND V.id_usuario='$idu' AND V.id_modelo=M.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function nuevoComentario($idr, $idu, $ida, $com){
      if($idu == null) $idu = "null";
      if($ida == null) $ida = "null";
      $fec = date("Y-m-d H:i:s");
      $sql = "INSERT INTO comentarios_reclamos VALUES(null, '$idr', $idu, $ida, '$com', '$fec');";
      return $this->orm->insertarPersonalizado($sql);
    }


    public function fetchAllActivas(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_noti='1' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
