<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Cliente{
    private $tabla = "usuarios";
    public $data = [];
    public $orm = null;

    public function Cliente(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE (id='$id' OR ced_usu='$id');";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function getVehiculos($id){
      $sql = "SELECT * FROM misvehiculos V, modelos_citas M WHERE V.id_usuario='$id' AND V.id_modelo=M.id;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function findByCod($id, $idc){
      if($idc != "")
        $aux = " AND id<>'$idc'";
      else $auto = "";

      $sql = "SELECT * FROM " . $this->tabla . " WHERE cod_cli='$id'$aux;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    
    
    public function login($u, $p){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE ced_usu='$u' AND pas_usu='$p';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function enviarCorreo($para, $mensaje, $asunto){
      return $this->orm->enviarCorreo($para, $mensaje, $asunto);
    }

    public function validarOlvido($ced, $cor){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE ced_usu='$ced' AND cor_usu='$cor';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }

    }

    public function cambiarClave($act, $nue, $idu){
      $sql = "UPDATE usuarios SET pas_usu='$nue' WHERE id='$idu' AND pas_usu='$act';";
      return $this->orm->editarPersonalizado($sql);
    }

    public function findByEmail($id, $idc){
      if($idc != "")
        $aux = " AND id<>'$idc'";
      else $auto = "";
      $sql = "SELECT * FROM " . $this->tabla . " WHERE cor_cli='$id'$aux;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT * FROM ".$this->tabla." ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id=$id;";
      return $this->orm->editarPersonalizado($sql);
    }

  }
?>
