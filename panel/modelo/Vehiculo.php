<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Vehiculo{
    private $tabla = "vehiculos";
    public $data = [];
    public $orm = null;

    public function Vehiculo(){
      $this->orm = new Orm(new Conexion());
      $this->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, modelos M WHERE V.id_modelo=M.id ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchVehiculosByModelo($idm){
      $sql = "SELECT * FROM ".$this->tabla." WHERE id_modelo='$idm' AND est_veh='1' ORDER BY nom_veh;";
      return $this->orm->consultaPersonalizada($sql);
    }


    public function fetchVehiculosByClasificacion($idm, $arreglo){
        $sql_ext = "";
        if(isset($arreglo['tipo'])){
            $sql_ext.= " AND imp_veh=".$arreglo['tipo'];
        }
        $sql = "SELECT * FROM ".$this->tabla." WHERE id_clasificacion='$idm' AND est_veh='1' $sql_ext ORDER BY nom_veh;";
        return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchVehiculosByImportado($idm){
      $sql = "SELECT * FROM ".$this->tabla." WHERE imp_veh='$idm' AND est_veh='1' ORDER BY nom_veh;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchVehiculosActivos(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_veh='1' ORDER BY nom_veh;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchVehiculosActivosExiauto(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE sto_veh='1' AND est_veh='1' ORDER BY nom_veh;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $n = count($this->data);
      $sql = "INSERT INTO ".$this->tabla ." (";
      $j=0;


      foreach($this->data as $key => $index){
        $j++;
        $sql.= "$key";
        if($j < $n){
          $sql.= ",";
        }
      }

      $sql.= ") VALUES(";
      $i = 0;

      foreach($this->data as $key => $index){

        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
