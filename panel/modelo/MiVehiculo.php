<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class MiVehiculo{
    private $tabla = "misvehiculos";
    public $data = [];
    public $orm = null;

    public function MiVehiculo(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function findByPlaca($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE placa='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT *, V.id as idv FROM ".$this->tabla." V, modelos_citas M WHERE V.id_modelo=M.id  ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fallas(){
      $sql = "SELECT * FROM fallas ORDER by falla;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function kilometrajes(){
      $sql = "SELECT * FROM kilometrajes ORDER by id;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function misVehiculos($idu){
      $sql = "SELECT *, V.id as idv FROM ".$this->tabla." V, modelos_citas M WHERE V.id_usuario='$idu' AND  V.id_modelo=M.id  ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function verificarVehiculo($idu, $idv){
      $sql = "SELECT *, V.id as idv FROM " . $this->tabla . " V, modelos_citas M WHERE V.id='$idv' AND V.id_usuario='$idu' AND V.id_modelo=M.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAllActivas(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_noti='1' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
