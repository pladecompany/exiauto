<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Agente{
    private $tabla = "agentes";
    public $data = [];
    public $orm = null;

    public function Agente(){
      $this->orm = new Orm(new Conexion());
      $this->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT * FROM ".$this->tabla." ORDER BY cod_age;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAllActivas(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_noti='1' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function validarHorario($ida, $fec){
      $sql = "SELECT * FROM citas WHERE id_agente=$ida AND fecha='$fec';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return true;
      }else{
        return false;
      }
    }

    public function citasPendientes($ida){
      $sql = "SELECT * FROM citas WHERE id_agente='$ida' and fecha > '" . date('Y-m-d H:i:s') . "' and estatus >= 0;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
