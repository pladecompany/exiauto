<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Cita{
    private $tabla = "citas";
    public $data = [];
    public $orm = null;
    public $txt = "txts/citas.txt";

    public function Cita(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        $ff = $r->fetch_assoc();
        $ff["txt"] = $this->estatus($ff['estatus'])["txt"];
        $ff["color"] = $this->estatus($ff['estatus'])["color"];
        return $ff;
      }else{
        return false;
      }
    }

    public function findByIdFull($id){
      $sql = "SELECT *, C.id as idc FROM " . $this->tabla . " C, agentes A, misvehiculos V, usuarios U, fallas F, modelos_citas M, kilometrajes K WHERE C.id_kilometros=K.id AND C.id_falla=F.id AND C.id_usuario=U.id AND C.id_agente=A.id AND C.id_vehiculo=V.id AND C.id='$id' AND V.id_modelo=M.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        $ff = $r->fetch_assoc();
        $ff["txt"] = $this->estatus($ff['estatus'])["txt"];
        $ff["color"] = $this->estatus($ff['estatus'])["color"];
        return $ff;
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT *, C.id as idc FROM ".$this->tabla." C, agentes U, misvehiculos V WHERE C.id_agente=U.id AND C.id_vehiculo=V.id ORDER BY C.fec_env DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function findFalla($id){
      $sql = "SELECT * FROM fallas where id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $ff;
      }else{
        return false;
      }
    }

    public function citasPorMes($est, $ida, $mes, $ano){
      if($ida != null)
        $age = " AND C.id_agente='$ida'";
      else
        $age="";
      if($est != null)
        $ess = " AND C.estatus='$est'";
      else
        $ess = "";

      $sql = "SELECT *, C.id as idc FROM ".$this->tabla." C, agentes A, misvehiculos V, usuarios U, fallas F, modelos_citas M, kilometrajes K WHERE C.id_kilometros=K.id AND C.id_falla=F.id AND C.id_usuario=U.id $age$ess AND C.fec_env like '$ano-$mes-%' AND  C.id_agente=A.id AND C.id_vehiculo=V.id AND V.id_modelo=M.id ORDER BY C.fec_env DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function misCitas($idu){
      $sql = "SELECT *, C.id as idc FROM ".$this->tabla." C, agentes U, misvehiculos V, modelos_citas M  WHERE C.id_usuario='$idu' AND C.id_agente=U.id AND C.id_vehiculo=V.id AND V.id_modelo=M.id ORDER BY C.fec_env DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function enviarCorreo($para, $mensaje, $asunto){
      return $this->orm->enviarCorreo($para, $mensaje, $asunto);
    }

    public function solicitudesPendientes(){
      $sql = "SELECT id FROM " . $this->tabla . " WHERE estatus = 0;";
      $r = $this->orm->consultaPersonalizada($sql);
      return $r->num_rows;
    }

    public function citasGenerales($f1, $f2){

      $sql = "SELECT *, C.id as idc FROM ".$this->tabla." C, agentes A, misvehiculos V, usuarios U, modelos_citas M WHERE (C.fec_env >= '$f1 00:00:00' AND C.fec_env <= '$f2 23:59:59') AND C.id_usuario=U.id AND C.id_agente=A.id AND C.id_vehiculo=V.id AND V.id_modelo=M.id ORDER BY C.fec_env DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function cambiarEstatus($est, $idc, $obs){
      $sql = "UPDATE citas set estatus='$est', observacion='$obs' WHERE id='$idc';";
      return $this->orm->editarPersonalizado($sql);
    }

    public function reprogramarCita($idc, $fec, $obs){
      $sql = "UPDATE citas set estatus='1', observacion='$obs', reprogramado='1' WHERE id='$idc';";
      return $this->orm->editarPersonalizado($sql);
    }

    public function cancelarCita($id, $idu){
      $sql = "UPDATE citas SET estatus=-1 WHERE id='$id' AND id_usuario='$idu';";
      return $this->orm->editarPersonalizado($sql);
    }

    public function estatus($num){
      if($num == 0){
        return Array("txt"=>"Pendiente por confirmación", "color"=>"#e79d42");
        return 'Pendiente por confirmación';
      }else if($num == -1){
        return Array("txt"=>"Cancelada", "color"=>"#D71A21");
      }else if($num == -2){
        return Array("txt"=>"Rechazada", "color"=>"#e75142");
      }else if($num == 1){
        return Array("txt"=>"Cita agendada", "color"=>"#1959cc");
      }else if($num == 2){
        return Array("txt"=>"Cita finalizada", "color"=>"#56cc19");
      }
    }

    public function fetchAllActivas(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_noti='1' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function verificarCitaAgendada($idu, $idv){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id_usuario='$idu' and id_vehiculo='$idv' and fecha > '" . date('Y-m-d H:i:s') . "' and (estatus >= 0 and estatus<=1);";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }
    
    public function grabarArchivo($idcita){
      $cita = $this->findByIdFull($idcita);
      if($cita == false){
        return false;
      }else{
        $linea = $cita['idc'].",".$cita['fec_env'].",".$cita['fecha'].",".$cita['placa'].",".$cita['modelo'].",".$cita['serial1'].",".$cita['serial2'].",".$cita['transmision'].",".$cita['ano'].",".$cita['kilometros'].",".$cita['ced_usu'].",".$cita['nom_usu'].",".$cita['ape_usu'].",".$cita['cor_usu'].",".$cita['tel_usu'].",".$cita['cod_age'].",".$cita['nom_age'].",".$cita['ape_age']."\n";
        $archivo = "txts/".$cita['idc']."_cita.txt";
        $file =fopen($archivo, "wb");
        fwrite($file, $linea);
        fclose($file);
      }
    }


    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
