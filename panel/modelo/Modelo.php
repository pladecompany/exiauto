<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Modelo{
    public $tabla = "modelos";
    public $data = [];
    public $orm = null;

    public function Modelo(){
      $this->orm = new Orm(new Conexion());
      $this->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT * FROM ".$this->tabla." ORDER BY modelo;";
      return $this->orm->consultaPersonalizada($sql);
    }


    public function fetchAllClasi(){
      $sql = "SELECT * FROM ".$this->tabla." ORDER BY descripcion;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAllActivas(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_noti='1' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
        $n = count($this->data);
        $sql = "INSERT INTO ".$this->tabla ." (";
        $j=0;


        foreach($this->data as $key => $index){
            $j++;
            $sql.= "$key";
            if($j < $n){
                $sql.= ",";
            }
        }

        $sql.= ") VALUES(";
        $i = 0;

        foreach($this->data as $key => $index){

            $i++;
            if($index == "")
                $sql.= "null";
            else
                $sql.= "'$index'";
            if($i < $n){
                $sql.= ",";
            }
        }
        $sql.= ");";
        return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
