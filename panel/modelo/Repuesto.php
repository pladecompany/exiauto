<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Repuesto{
    private $tabla = "repuestos";
    public $data = [];
    public $orm = null;

    public function Repuesto(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function removeModelos($id){
      return $this->orm->eliminar('id_repuesto', $id,"asignar_modelos_repuestos");
    }

    public function findSolicitud($id){
      $sql = "SELECT * FROM solicitud_repuestos WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }
    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function asignarModelos($idr, $modelos, $anos){
      $this->removeModelos($idr);
      for($i=0;$i<count($modelos);$i++){
        $sql = "INSERT INTO asignar_modelos_repuestos values(null, $idr, ".$modelos[$i].",'".$anos[$i]."');";
        $this->orm->insertarPersonalizado($sql);
      }
    }

    public function getModelosAsignados($idr){
      $sql = "SELECT *, A.id as idm FROM asignar_modelos_repuestos A, modelos_repuestos M WHERE A.id_modelo=M.id AND A.id_repuesto=$idr;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getModelosRepuestos(){
      $sql = "SELECT * FROM modelos_repuestos ORDER BY modelo;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function cambiarEstatus($est, $id){
      $sql = "UPDATE solicitud_repuestos SET estatus=1 WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);
    }

    public function insertarSolicitud(){
      $sql = "INSERT INTO solicitud_repuestos VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

    public function solicitudesGenerales($f1, $f2){
      $sql = "SELECT * FROM solicitud_repuestos WHERE fec_reg_sol>='$f1 00:00:00' AND fec_reg_sol<='$f2 23:59:59' ORDER BY fec_reg_sol DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }


    public function fetchAll(){
      $sql = "SELECT *, R.id as idr FROM ".$this->tabla." R, categorias_repuestos C WHERE R.id_categoria=C.id ORDER BY R.id;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAllActivas(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_noti='1' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
