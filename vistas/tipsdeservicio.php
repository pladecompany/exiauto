<section class="section-about ">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about-img-box">
				<img src="static/img/7.jpg" alt="" class="img-fluid q10" style="width: 100%;">
				</div>
				<div class="sinse-box">
				<h3 class="sinse-title color-c">Tips de <br> Servicio</h3>
				</div>
			</div>

			<div class="col-md-12 section-t8">
				<div class="row">
					<div class="col-sm-12 col-md-10 offset-md-1">
						<div class="title-box-d">
						<h3 class="title-d">Tips de <span class="color-b">Servicio</span></h3>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							<strong>ACEITE MULTIGRA:</strong>
							Un aceite multigrado brinda protección dentro de un amplio rango de temperaturas, contiene aditivos mejorados del índice de viscosidad que permiten buena protección tanto al momento del arranque en temperaturas muy frías, como en su operación normal a temperaturas de trabajo del motor, se identifica este tipo de aceite al observar en el envase las siglas SAE, seguidas de un número, la letra W y luego otro número, por ejemplo: SAE 15W-50. SAE indica la norma utilizada, 15W es la habilidad de protección en frío, mientras más bajo protege a más bajas temperaturas. 50 indica la habilidad de protección en caliente, mientras más alto este valor mayor habilidad de protección a más alta temperatura de trabajo. Los fabricantes de vehículos toman en consideración las condiciones ambientales, condiciones de operación, tipo de combustible, tecnología del motor, etc., y definen con ello a través de pruebas y experiencias las recomendaciones del tipo de lubricante a utilizar en cada vehículo para un país o una zona determinada.
							</p>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							<strong>NIVEL DE SERVICIO:</strong>
							Nivel de servicio SE recomendado para motores 1989 Nivel de servicio SF, Motores a Gasolina fabricados en1980 - 88:.- Nivel de servicio SG, Motores a Gasolina fabricados en 1989- 93:.- Nivel de Servicio SH, Motores a Gasolina fabricados en1994 - 96:.- Nivel de Servicio SJ, Motores fabricados del año 96 en adelante.
							</p>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							<strong>ADITIVOS O MEJORADORES DEL ACEITE LUBRICANTE:</strong>
							Uno de los compuestos químicos resultantes de la oxidación del aceite son los barnices que generan lodos dentro del motor, los barnices se forman por la oxidación del aceite por lo que los aditivos antioxidantes juegan un papel muy importante. Los aditivos detergentes reaccionan con los materiales oxidados, ayudan a mantener pistones, anillos, taquetes y otros componentes limpios. Atrapan los residuos de la combustión que se generan por el desprendimiento de depósitos, con los cambios bruscos de temperatura en motores que cambian continuamente de régimen de temperatura y RPM. Este aditivo puede causar detonación por lo que generalmente los aceites tipo Rancing Oil tienen bajo contenido de detergente. Los dispersantes ayudan a los detergentes a mantener limpio el motor, las partículas en suspensión tienen tamaños moleculares por lo que no tapan los filtros de aceite que permiten el paso de partículas entre los 10 micrones.
							<br><br>
							Toda esta química en el lubricante está perfectamente balanceada, por lo que no se recomienda el uso de aditivos suplementarios que podrían anular o modificar la efectividad de los aditivos originales, al producirse reacciones químicas entre ellos. Tampoco se recomienda mezclar aceites de diferentes marcas, ya que cada fabricante tiene su propia formulación química que podría no ser compatible con la de otro fabricante.
							</p>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							<strong>CAMBIO DEL ACEITE:</strong>
							El aceite debe ser removido, si es de buena calidad, generalmente cada 5.000 Kms. o 3 meses. Si se observan depósitos de lodo se debe aumentar la frecuencia de cambio; el aceite, para cumplir su función, sacrifica su efectividad ya que sus aditivos se van saturando, envejeciendo y su función deja de tener efectividad. Los aditivos van atrapando y manteniendo en suspensión los depósitos para que no se sedimenten y se incrusten, por lo que también es importante tomar en cuenta el uso que se le da al vehículo y el nivel de desgaste que este tenga. Si su uso es con frecuencia en caminos polvorientos o pantanosos, donde se transporta carga o se arrastran trailers, tal como sucede con nuestros vehículos Todo Terreno, se recomienda el cambio del aceite cada 3.000 kilómetros o después de un viaje extremo.
							<br><br>
							En todo caso es importante seguir las recomendaciones del fabricante que aparecen en el manual del propietario. Si no se tiene a mano esta información, dadas nuestras condiciones climáticas, de una estable temperatura sin marcadas variaciones entre una época y otra del año, o entre la temperatura del mediodía y la de la mañana, donde muy rara vez llegamos a niveles de congelación, se puede usar un aceite de regular viscosidad en frío, como por ejemplo, 15W o 20W. Adicionalmente, motivado a nuestro clima cálido el punto de operación normal del motor es en un entorno ambiental caliente, por lo que se pueden usar aceites de grados 30 a 50 para motores con más de 50.000 Kms.
							</p>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							<strong>ACEITES LUBRICANTES SINTÉTICOS:</strong>
							Estas bases son más estables en un amplio rango de temperatura, generan menos subproductos al ser sometidas a altas temperaturas y contienen menos contaminantes por ser altamente puras. En estas condiciones permiten una mejor mezcla con los diferentes aditivos que se usan en la fabricación de aceites lubricantes y por su alta estabilidad química estos aditivos tienen un período de envejecimiento mucho más largo que en los aceites minerales, obtenidos a partir de subproductos refinados del petróleo. Por su alto poder de detergencia se recomienda su uso en motores con poco o ningún desgaste. La estabilidad en el tiempo de sus aditivos permite a su vez un período de recambio más largo, con lo que se logra en algunos casos elevar el tiempo de servicio a más de 10.000 Kms.
							<br><br>
							Los resultados de su uso en motores son excepcionales, con lo que se logra una larga vida de sus partes vitales.
							<br><br>
							La utilización de estos lubricantes en vehículos Todo Terreno usados con frecuencia en la práctica del turismo de aventura, tiene el inconveniente de que en éstos se pierde la bondad de su prolongado tiempo de recambio, ya que indistintamente del tipo de aceite que se use el período de recambio no es recomendable excederlo de los 7.500 Kms., o recambiar luego de una excursión donde se sospecha que el motor fue contaminado con polvo, agua y estuvo sometido prolongadamente a esfuerzos extremos.
							<br><br>
							Existen en el mercado aceites sintéticos puros al 100% y los mezclados (mix). Estas mezclas son producidas por diferentes fabricantes de marcas reconocidas, los cuales mezclan el aceite sintético con aceites minerales para obtener productos de calidad intermedia. Esta información viene especificada en el envase, sus beneficios son intermedios dependiendo de la proporción de la mezcla y su precio es generalmente menor.
							</p>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							<strong>RECOMENDACION ECOLÓGICA:</strong>
							Los aceites lubricantes, nuevos o usados, son contaminantes del medio ambiente, no los derrames en las alcantarillas, ni sobre la tierra que pueda ser arrastrada posteriormente por las aguas de la lluvia. Ello implica un grave daño ecológico, contribuyamos a mantener nuestro ambiente limpio.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
