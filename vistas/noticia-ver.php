<?php
  include_once("panel/modelo/Noticia.php");
  $noti_leer = new Noticia();
  
  $idn = $_GET['id'];
  $NOTI = $noti_leer->findById($idn);
  if($NOTI == false){
    echo "<script>window.location ='index.php';</script>";
    exit(1);
  }
?>
<br><br>
<br><br>
<br><br>
<section class="section-about">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about-img-box">
				<img src="<?php echo $NOTI['img'];?>" alt="<?php echo $NOTI['titulo'];?>" class="img-fluid">
				</div>
				<div class="sinse-box">
				<h3 class="sinse-title color-c"><?php echo $NOTI['titulo'];?></h3>
				</div>
			</div>

			<div class="col-md-12 section-t8">
				<div class="row">
					<div class="col-sm-12 col-md-8 offset-md-2">
						<div class="title-box-d">
							<h3 class="title-d"><?php echo $NOTI['titulo'];?></h3>
						</div>
						<h5 class="text-justify color-text-a">
                          <?php echo nl2br($NOTI['descripcion']); ?>
						</h5>
					</div>
				</div>
			</div>
            <br>
			<div class="col-md-12 section-t8">
				<div class="row">
					<div class="col-sm-12 col-md-8 offset-md-2">
						<div class="title-box-d">
							<h6 class="title-d">Publicado el <?php echo $NOTI['fec_reg_noti'];?></h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

