<?php
  if(!isset($_SESSION['log'])){
    session_start();
    session_destroy();
    echo "<script>window.location ='index.php';</script>";
    exit(1);
  }else{
    include_once("panel/controlador/misvehiculos.php");
    include_once("panel/modelo/Cliente.php");
    include_once("panel/controlador/clientes.php");

    $cli = new Cliente();
    $F = $cli->findById($_SESSION['idu']);

    if($F == false){
      echo "<script>window.location ='salir.php';</script>";
      exit(1);
    }
    $ced = $F['ced_usu'];
    $a = explode("-", $ced);
    $ced1 = $a[0];
    $ced2 = $a[1];
  }
?>
<br>
<br>
<br>

<div class="row">
<div class="col-md-6">
  <div class="container mt-5">
      <div class="card shadow mb-4">
      <div class="card-header py-3">
          <h4 class="m-0 font-weight-bold color-b">Información personal</h4>
      </div>
      <div class="card-body">
          <?php 
            if(!isset($_GET['cc'])) {
              include_once("mensajes.php");
            }
          ?>

          <form class="form-a" action="panel/controlador/clientes.php" id="formulario_registro_cliente" method="POST">
              <div clas="col-md-12 mb-2 text-center" style="margin-bottom:0px !important;display:none;text-align:center !important;" id="cont_msj">
                <span id="txt_msj" style="color:red;text-align:center;"></span>
                <br><br>
              </div>
              <div class="row">

                  <div class="col-md-2 mb-2" style="margin-bottom:0px !important;">
                      <label for="Correo">V/E/J</label>
                      <select class="form-control form-control-lg form-control-a" name="cod1" required>
                          <option <?php if($ced1 == 'V') echo 'selected';?>>V</option>
                          <option <?php if($ced1 == 'E') echo 'selected';?>>E</option>
                          <option <?php if($ced1 == 'J') echo 'selected';?>>J</option>
                      </select>
                  </div>

                  <div class="col-md-4 mb-2" style="margin-bottom:0px !important;">
                      <label for="Correo">Cédula/rif</label>
                      <div class="form-group">
                          <input type="text" class="form-control form-control-lg form-control-a" placeholder="Cédula/Rif" name="cod2" required minlength="3" value="<?php echo $ced2;?>">
                      </div>
                  </div>
                  <?php if($ced1 == 'J') { ?>
                  <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Correo">Razón social o empresa</label>
                          <input type="text" class="form-control form-control-lg form-control-a" placeholder="Razón social o empresa" name="emp"  value="<?php echo $F['emp_usu'];?>">
                      </div>
                  </div>
                  <?php } ?>

                  <div class="col-md-4 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Nombres">Nombres</label>
                          <input type="text" class="form-control form-control-lg form-control-a" placeholder="Nombres" required minlength="3" name="nom" value="<?php echo $F['nom_usu'];?>">
                      </div>
                  </div>

                  <div class="col-md-4 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Apellidos">Apellidos</label>
                          <input type="text" class="form-control form-control-lg form-control-a" placeholder="Apellidos" required minlength="3" name="ape" value="<?php echo $F['ape_usu'];?>">
                      </div>
                  </div>
                  <div class="col-md-4 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Fecha de nacimiento">Fecha de nacimiento</label>
                          <input type="date" class="form-control form-control-lg form-control-a" required name="fec" value="<?php echo $F['fec_nac_usu'];?>">
                      </div>
                  </div>
                    <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                        <label for="Teléfono">Teléfono de contacto</label>
                      <div class="row">
                        <div class="form-group col-md-5">
                            <select name="tlf1" class="form-control" required>
                              <option value="">--</option>
                              <option <?php if(substr($F['tel_usu'], 0, 4) =='0412') echo 'selected';?>>0412</option>
                              <option <?php if(substr($F['tel_usu'], 0, 4) =='0424') echo 'selected';?>>0424</option>
                              <option <?php if(substr($F['tel_usu'], 0, 4) =='0414') echo 'selected';?>>0414</option>
                              <option <?php if(substr($F['tel_usu'], 0, 4) =='0416') echo 'selected';?>>0416</option>
                              <option <?php if(substr($F['tel_usu'], 0, 4) =='0426') echo 'selected';?>>0426</option>
                            </select>
                        </div>
                        <div class="form-group col-md-7">
                            <input type="text" class="form-control form-control-lg form-control-a" placeholder="Teléfono" name="tlf" required maxlength="7" minlength="7" value="<?php echo substr($F['tel_usu'], 4, strlen($F['tel_usu']));?>">
                        </div>
                      </div>
                    </div>
                  
                  <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Correo">Correo</label>
                          <input type="email" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo" required minlength="5" name="cor" value="<?php echo $F['cor_usu'];?>">
                      </div>
                  </div>

                  <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Nombres">Twitter</label>
                          <input type="text" class="form-control form-control-lg form-control-a" placeholder="Twitter" minlength="2" name="twi" maxlength="30" value="<?php echo $F['twitter'];?>">
                      </div>
                  </div>

                  <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Nombres">Facebook</label>
                          <input type="text" class="form-control form-control-lg form-control-a" placeholder="Facebook" minlength="2" name="face" maxlength="50" value="<?php echo $F['facebook'];?>">
                      </div>
                  </div>

                  <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Nombres">Instagram</label>
                          <input type="text" class="form-control form-control-lg form-control-a" placeholder="Instagram" minlength="2" name="inst" maxlength="30" value="<?php echo $F['instagram'];?>">
                      </div>
                  </div>

                  <div class="col-md-4 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Contraseña">&nbsp;</label>
                          <button type="submit" name="btc" class="btn btn-b" style="width:100%;">Guardar </button>
                      </div>
                  </div>
              </div>
          </form>
      </div>
  </div>
</div>
</div>


<div class="col-md-6">
  <div class="container mt-5">
      <div class="card shadow mb-4">
      <div class="card-header py-3">
          <h4 class="m-0 font-weight-bold color-b">Cambiar contraseña</h4>
      </div>
      <div class="card-body">
          <?php 
            if(isset($_GET['cc'])) {
              include_once("mensajes.php");
            }
          ?>

          <form class="form-a" action="panel/controlador/clientes.php" id="formulario_registro_cliente" method="POST">
              <div clas="col-md-12 mb-2 text-center" style="margin-bottom:0px !important;display:none;text-align:center !important;" id="cont_msj">
                <span id="txt_msj" style="color:red;text-align:center;"></span>
                <br><br>
              </div>
              <div class="row">

                  <div class="col-md-12 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Contraseña">Contraseña actual</label>
                          <input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" required minlength="5" name="act">
                      </div>
                  </div>
                  <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Contraseña">Nueva contraseña</label>
                          <input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" required minlength="5" name="pas">
                      </div>
                  </div>

                  <div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Contraseña">Confirmar contraseña</label>
                          <input type="password" class="form-control form-control-lg form-control-a" placeholder="Confirma tu contraseña" required minlength="5" name="cpa">
                      </div>
                  </div>


                  <div class="col-md-12 mb-2 text-center" style="margin-bottom:0px !important;">
                      <div class="form-group">
                          <label for="Contraseña">&nbsp;</label>
                          <button type="submit" name="bt_clave" class="btn btn-b" style="width:100%;">Cambiar Contraseña</button>
                      </div>
                  </div>
              </div>
          </form>
      </div>
  </div>
</div>
</div>
