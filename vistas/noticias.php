<?php 
  include_once("panel/modelo/Noticia.php");
  $notit = new Noticia();
  $rnn = $notit->fetchAllActivas();
?>


<section class="intro-single">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-8">
				<div class="title-single-box">
					<h1 class="title-single">Noticias</h1>

				</div>
			</div>
			<div class="col-md-12 col-lg-4">
				<nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="?op=inicio">Exiauto</a>
						</li>
						<li class="breadcrumb-item active" aria-current="page"> Noticias </li>
					</ol>
				</nav>
			</div>
		</div>
		<br><br>

		<div class="row">
	        <?php
	          while($ffn = $rnn->fetch_assoc()){
	        ?>
				<div class="col-md-4 mb-4">
					<div class="card-box-b card-shadow news-box">
						<div class="img-box-b">
							<img src="<?php echo $ffn['img'];?>" alt="" class="img-a img-fluid img-noticia">
						</div>
						<div class="card-overlay">
							<div class="card-overlay-a-content">
								<div class="card-header-a">
									<h2 class="card-title-a">
										<a href="?op=noticia-ver&id=<?php echo $ffn['id'];?>"><?php echo $ffn['titulo'];?></a>
									</h2>
								</div>
								<div class="card-body-a">
									<a href="?op=noticia-ver&id=<?php echo $ffn['id'];?>" class="link-a">Leer más
										<span class="ion-ios-arrow-forward"></span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
	        <?php
	          }
	        ?>
        </div>
    </div>
</section>
