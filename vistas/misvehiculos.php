<?php
  if(!isset($_SESSION['log'])){
    session_start();
    session_destroy();
    echo "<script>window.location ='index.php';</script>";
    exit(1);
  }

  include_once("panel/controlador/misvehiculos.php");

?>

<div class="card shadow mb-4 ml-5 mr-5" style="margin-top:10em;">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Mis Vehículos</h4>
		
		<div class="text-right">
			<a href="#md-slider" data-toggle="modal" class="color-b modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Agregar vehículo</b></a>
		</div>
	</div>

	<div class="card-body">
    <?php include_once("mensajes.php");?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Modelo</th>
						<th>Placa</th>
						<th>Color</th>
						<th>Transmisión</th>
						<th>Serial de carrocería</th>
						<th>Año</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
	              <?php
	                $noti = new MiVehiculo();
	                $r = $noti->misVehiculos($_SESSION['idu']);
	                $i=0;
	                while($ff = $r->fetch_assoc()){
	                  $i++;
	                  echo "<tr>";
	                  echo "  <td>" . $i . "</td>";
	                  echo "  <td>" . $ff['modelo'] . "</td>";
	                  echo "  <td>" . $ff['placa'] . "</td>";
	                  echo "  <td>" . $ff['serial2'] . "</td>";
	                  echo "  <td>" . $ff['serial3'] . "</td>";
	                  echo "  <td>" . $ff['serial1'] . "</td>";
	                  echo "  <td>" . $ff['ano'] . "</td>";
	                  echo "<td><a href='?op=cita&idv=".$ff['idv']."&nuevacita' onclick=''>Tomar cita <i class='mr-2 fa fa-check'></i></a>";
	                  echo "<a href='?op=misvehiculos&id=".$ff['idv']."'>Editar <i class='mr-2 fa fa-edit'></i></a>";
	                  echo "<a href='?op=misvehiculos&el=".$ff['idv']."' onclick='return confirm(\"¿ Esta seguro ?\")'><i class='mr-2 fa fa-trash'></i></a>";
	                  echo "</td>";
	                  echo "</tr>";
	                }
	              ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="md-slider" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
                    <?php if(isset($F)){ ?>
					<h3 class="title-d" id="titulo_modulo">Editar vehículo</h3>
                    <?php }else{?>
					<h3 class="title-d" id="titulo_modulo">Nuevo vehículo</h3>
                    <?php }?>
				</div>

				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
                    <?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
					<div class="row">
						<div class="col-md-12 mb-2" id="mensaje_cambio" style="display:none;">
                          <label>Este vehículo pertenece a otro cliente, ¿ es usted el nuevo dueño ? </label>
                          <a href="" class="btn btn-b-n" id="bt_si_cliente" onclick="return comfirn('Esta seguro?')">Si</a>
                          <a href="#" class="btn btn-b-n" id="bt_no_cliente">No</a>
                        </div>
						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Modelo">Placa</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="" name="pla" value="<?php echo $F['placa'];?>" id="placa_vehiculo">
							</div>
						</div>
						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Modelo">Modelo</label>
                                <select name="mod" class="form-control" required>
                                    <option value="">Seleccione</option>
                                <?php
                                  $mod = new ModeloCitas();
                                  $r = $mod->fetchAll();
                                  while($fm = $r->fetch_assoc()){
                                    if(isset($F) && $F['id_modelo'] == $fm['id']){
                                ?>
                                    <option value="<?php echo $fm['id'];?>" selected><?php echo strtoupper($fm['modelo'] );?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $fm['id'];?>"><?php echo strtoupper($fm['modelo'] );?></option>
                                <?php
                                      }
                                  }
                                ?>
						</select>
							</div>
						</div>
						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Modelo">Serial de carrocería</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="" name="ser1" value="<?php echo $F['serial1'];?>" required minlength="15">
							</div>
						</div>
						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Modelo">Color</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="" name="ser2" value="<?php echo $F['serial2'];?>" required minlength="2">
							</div>
						</div>
						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Modelo">Transmisión</label>
                                <select name="ser3" class="form-control" required>
                                    <option value="">Seleccione</option>
                                    <option <?php if(isset($F) && $F['serial3'] =='Automatico') echo 'selected';?>>Automatico</option>
                                    <option <?php if(isset($F) && $F['serial3'] =='Manual') echo 'selected';?>>Manual</option>
                                </Select>
							</div>
						</div>
						<div class="col-md-6 mb-2">
							<div class="form-group">
								<label for="Modelo">Año</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="" minlength="4" maxlength="4" name="ano" value="<?php echo $F['ano'];?>" required>
							</div>
						</div>
					</div>
                    <div class="modal-footer">
                        <button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-b"><?php echo ((isset($F))?'Guardar Cambios':'Guardar')?></button>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
  if(isset($F)){
?>
  <script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").trigger('click');
    });
  </script>

<?php
  } 
?>
<script>
    $(document).ready(function(){
      var ids = '<?php echo $_SESSION['idu'];?>';
      console.log(ids);
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nuevo vehículo");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Guardar');
        $("input[name='pla']").val('');
        $("input[name='ser1']").val('');
        $("input[name='ser2']").val('');
        $("input[name='ser3']").val('');
        $("input[name='ser4']").val('');
        $("input[name='ano']").val('');
        $("select[name='mod']").val('');
      });

      $("#bt_no_cliente").click(function(){
        $("#mensaje_cambio").hide();
        $("#bt_si_cliente").attr('href', '#');
        $("#placa_vehiculo").val("");
      });

      $("#placa_vehiculo").keyup(function(){
        var placa = $(this).val();
        if(placa.length>=6 ){
          $("body").attr('style', 'cursor:wait');
          $.post('panel/ajax_php.php', {modulo: 'vehiculos', tipo: 'validarPlaca', placa: placa}, function(data){
              $("body").attr('style', 'cursor:normal');
            if(data.r && data.id_usuario != ids){
              $("#mensaje_cambio").show();
              $("#bt_si_cliente").attr("href", "?op=misvehiculos&cambio=" + data.vehiculo.placa);
            }
          });
        }
      });
    });

</script>

