<?php
	$fechaInicial = date('1993-01-01');
	$fechaActual = date('Y');

    $tiempo = ($fechaActual - $fechaInicial);
?>

<?php include_once("slider.php"); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="pt-0 pb-5 d-flex justify-content-between">
				<div class="title-box">
					<h2 class="title-a clr_red">Bienvenido a Corporación Exiauto C.A </h2>
					<h5 class="text-justify">Tenemos más de <strong><?php echo $tiempo; ?> años</strong> ininterrumpidos en el mercado automotriz prestando el mejor <strong><a href="?op=mantenimiento" target="_blank" class="clr_red">servicio</a> y productos Toyota</strong>, conformado por los departamentos de Ventas, <a href="?op=latoneria" target="_blank" class="clr_red">Latonería y Pintura,</a> <a href="?op=mantenimiento" target="_blank" class="clr_red">Servicio,</a> y <a href="?op=repuestos" target="_blank">Repuestos</a> caracterizado por el capital humano capacitado y con la experiencia para <strong>brindar la mejor asesoría del mercado</strong>, contamos con aproximadamente 11.860 metros cuadrados para satisfacer las necesidades y exigencias de nuestros clientes.</h5>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include_once("servicios.php"); ?>

<?php include_once("testimonios.php"); ?>

<div class="box-video">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-10 offset-md-1">
					<div class="title-box">
						<h2 class="title-a"><?php echo $video['titulo'];?></h2>
					</div>
				<iframe width="100%" height="400" src="https://www.youtube.com/embed/<?php echo $video['video'];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

<section class="section-services box-llamado">
	<div class="col-md-12">
		<div class="container text-center">
			<h2 class="title-a color-c">¿ Tienes una sugerencia o reclamo ?</h2>
			<br><br>
			<div class="animate__animated animate__headShake animate__infinite">
				<a href="#md-ingresar"  data-toggle="modal" data-target="#md-ingresar" class="color-b modal-trigger btn-linea-wh"><b>Hazlo ahora</b></a>
			</div>
		</div>
	</div>
</section>


<?php
  include_once("promo.php");
?>
