<div class="intro intro-carousel">
	<div id="carousel" class="owl-carousel owl-theme">
    <div class="carousel-item-a intro-item bg-image-slider" style="background-image: url('<?php echo (($IMG['img_slider_1']!=null)?$IMG['img_slider_1']:'static/img/slider/img5.jpg');?>')">
			<div class="overlay overlay-a"></div>
			<div class="intro-content display-table">
				<div class="table-cell">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 opacity">
								<div class="intro-body">
									<p class="intro-title-top">Caracas, Venezuela
										<br> </p>
									<h1 class="intro-title mb-4">
										<span class="color-b" style="text-shadow: 1px 1px 1px #000;">Corporación exiauto C.A </span> <br> </h1>
                                    <h5 class="intro-title mb-4">Su concesionario del Este</h5>
									<p class="intro-subtitle intro-price">
										<a href="?op=contacto"><span class="price-a animate__animated animate__bounce">Contáctanos </span></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="carousel-item-a intro-item bg-image-slider" style="background-image: url('<?php echo (($IMG['img_slider_2']!=null)?$IMG['img_slider_2']:'static/img/slider/img1.jpg');?>');">
			<div class="overlay overlay-a"></div>
			<div class="intro-content display-table">
				<div class="table-cell">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 opacity">
								<div class="intro-body">
									<p class="intro-title-top">Caracas, Venezuela
										<br> </p>
									<h1 class="intro-title mb-4">
										<span class="color-b" style="text-shadow: 1px 1px 1px #000;">¿ Necesitas ayuda ? </span> Nuestros 
										<br> profesionales pueden atender todas tus necesidades.</h1>
									<p class="intro-subtitle intro-price">
										<a data-target="#md-ingresar" class="modal-trigger" data-toggle="modal"><span class="price-a">Solicitar cita</span></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="carousel-item-a intro-item bg-image-slider" style="background-image: url('<?php echo (($IMG['img_slider_3']!=null)?$IMG['img_slider_3']:'static/img/slider/img2.jpg');?>')">
			<div class="overlay overlay-a"></div>
			<div class="intro-content display-table">
				<div class="table-cell">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 opacity">
								<div class="intro-body">
									<p class="intro-title-top">Caracas, Venezuela
										<br> </p>
									<h1 class="intro-title mb-4">
										<span class="color-b" style="text-shadow: 1px 1px 1px #000;"> Tenemos lo que necesitas </span> 
										<br> Vehículos, repuestos y servicios</h1>
									<p class="intro-subtitle intro-price">
										<a data-target="#md-solicitud" class="modal-trigger" data-toggle="modal"><span class="price-a">Consulta tus dudas</span></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="carousel-item-a intro-item bg-image-slider" style="background-image: url('<?php echo (($IMG['img_slider_4']!=null)?$IMG['img_slider_4']:'static/img/slider/img0.png');?>')">
			<div class="overlay overlay-a"></div>
			<div class="intro-content display-table">
				<div class="table-cell">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 opacity">
								<div class="intro-body">
									<p class="intro-title-top">Caracas, Venezuela
										<br> </p>
									<h1 class="intro-title mb-4">
										<span class="color-b" style="text-shadow: 1px 1px 1px #000;">Su seguridad</span> <br> </h1>
                                    <h5 class="intro-title mb-4">Es nuestra prioridad</h5>
									<p class="intro-subtitle intro-price">
										<a href=" https://apps.toyota.com.ve/genzai/campana" target="_blank"><span class="price-a">Infórmate </span></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
