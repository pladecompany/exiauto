<section class="section-testimonials section-t8 nav-arrow-a" id="testimonios">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title-wrap d-flex justify-content-between">
					<div class="title-box">
						<h2 class="title-a">Lo que dicen nuestros clientes</h2>
					</div>
				</div>
			</div>
		</div>
		<div id="testimonial-carousel" class="owl-carousel owl-arrow">
			<div class="carousel-item-a">
				<div class="testimonials-box">
					<div class="row">
						<div class="col-sm-12 col-md-8 offset-md-2">
							<div class="testimonial-ico">
								<span class="ion-ios-quote"></span>
							</div>
							<div class="testimonials-content">
								<p class="testimonial-text">
									Excelente consecionario, tanto por atención, disponibilidad de repuestos y taller
									<br><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i>
								</p>
							</div>
							<div class="testimonial-author-box">
								<img src="static/img/testimonios/1.png" alt="" class="testimonial-avatar">
								<h5 class="testimonial-author">Jesús T.</h5>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="carousel-item-a">
				<div class="testimonials-box">
					<div class="row">
						<div class="col-sm-12 col-md-8 offset-md-2">
							<div class="testimonial-ico">
								<span class="ion-ios-quote"></span>
							</div>
							<div class="testimonials-content">
								<p class="testimonial-text">
									Servicio profesional, cordial y valoran la relación con sus clientes
									<br><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i>
								</p>
							</div>
							<div class="testimonial-author-box">
								<img src="static/img/testimonios/2.png" alt="" class="testimonial-avatar">
								<h5 class="testimonial-author">Roberto Gabizon</h5>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="carousel-item-a">
				<div class="testimonials-box">
					<div class="row">
						<div class="col-sm-12 col-md-8 offset-md-2">
							<div class="testimonial-ico">
								<span class="ion-ios-quote"></span>
							</div>
							<div class="testimonials-content">
								<p class="testimonial-text">
									Desde que ingresas la atención es magnífica, te hacen un chequeo previo de como llevas u entregas tu auto para protección de ambas partes. Te llaman si hay que cambiar una pieza. Previo al servicio te hacen una entrevista para valorar como sentiste el servicio, cosa que me parece maravilloso. Me gustó los procesos de atención al público y el auto está en perfecto estado
									<br><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i>
								</p>
							</div>
							<div class="testimonial-author-box">
								<img src="static/img/testimonios/1.png" alt="" class="testimonial-avatar">
								<h5 class="testimonial-author">Planeta Respiratorio</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="carousel-item-a">
				<div class="testimonials-box">
					<div class="row">
						<div class="col-sm-12 col-md-8 offset-md-2">
							<div class="testimonial-ico">
								<span class="ion-ios-quote"></span>
							</div>
							<div class="testimonials-content">
								<p class="testimonial-text">
									Muy bueno respuestos originales.
									<br><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i>
								</p>
							</div>
							<div class="testimonial-author-box">
								<img src="static/img/testimonios/4.png" alt="" class="testimonial-avatar">
								<h5 class="testimonial-author">William Lopez</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="carousel-item-a">
				<div class="testimonials-box">
					<div class="row">
						<div class="col-sm-12 col-md-8 offset-md-2">
							<div class="testimonial-ico">
								<span class="ion-ios-quote"></span>
							</div>
							<div class="testimonials-content">
								<p class="testimonial-text">
									La atención es excelente, el espacio es pulcro, excelente iluminación y muestra buena distribución de los espacios. Los técnicos siempre están dispuestos a atender a los visitantes es. Nadie creería que es un taller mecánicos. Como todo en estos días, los precios están en dólares, si tienes un Toyota, sabes lo que cuesta mantenerlo, aquí es económico y confiable.
									<br><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i>
								</p>
							</div>
							<div class="testimonial-author-box">
								<img src="static/img/testimonios/5.png" alt="" class="testimonial-avatar">
								<h5 class="testimonial-author">Ader Fernández</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="carousel-item-a">
				<div class="testimonials-box">
					<div class="row">
						<div class="col-sm-12 col-md-8 offset-md-2">
							<div class="testimonial-ico">
								<span class="ion-ios-quote"></span>
							</div>
							<div class="testimonials-content">
								<p class="testimonial-text">
									Excelente servicio Toyota en Caracas con las metodología de trabajo japonesa y la cordialidad venezolana. Hay que llamar antes para programar el día en el cual hay que llevar el carro. Altamente recomendado.
									<br><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i>
								</p>
							</div>
							<div class="testimonial-author-box">
								<img src="static/img/testimonios/6.png" alt="" class="testimonial-avatar">
								<h5 class="testimonial-author">Fernando Torre Chalbaud</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="carousel-item-a">
				<div class="testimonials-box">
					<div class="row">
						<div class="col-sm-12 col-md-8 offset-md-2">
							<div class="testimonial-ico">
								<span class="ion-ios-quote"></span>
							</div>
							<div class="testimonials-content">
								<p class="testimonial-text">
									Excelente taller de la Toyota la atención al público me hizo sentir estar en otro país. Muy buena atención y rápidos con sus servicios.
									<br><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i><i class="fa fa-star color-y"></i>
								</p>
							</div>
							<div class="testimonial-author-box">
								<img src="static/img/testimonios/7.png" alt="" class="testimonial-avatar">
								<h5 class="testimonial-author">José Pérez Cote</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>