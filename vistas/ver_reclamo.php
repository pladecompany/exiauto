<?php
  if(!isset($_SESSION['log'])){
	session_start();
	session_destroy();
	echo "<script>window.location ='index.php';</script>";
	exit(1);
  }
  include_once("panel/modelo/Admin.php");
  include_once("panel/modelo/Cliente.php");
  include_once("panel/controlador/reclamos.php");
  
?>
<div class="container mt-5" style="padding-top:5em;">
	<?php include_once("mensajes.php");?>
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="row">
				<div class="col-sm-6 text-left">
					<a href="?op=reclamos" class="color-b"><b><i class="fa fa-arrow-left"></i> Volver</b></a>
				</div>

				<div class="col-sm-6 text-left">
					<div class="text-right">
						<a href="#md-slider" data-toggle="modal" class="color-b modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Nuevo comentario</b></a>
					</div>
				</div>
			</div>

			<hr>
			<div class="row">
				<div class="col-md-4">
					<h6 class="m-0 font-weight-bold color-b">Enviado por el cliente: <br><b style="color: #000;"><?php echo $F['ced_usu'] ." - ".$F['nom_usu']." ".$F['ape_usu'];?></b></h6>
				</div>
				
				<div class="col-md-2">
					<h6 class="m-0 font-weight-bold color-b">Tipo: <br><b style="color: #000;"><?php echo (($F['tipo']=='R')?'Reclamo':'Sugerencia');?></b></h6>
				</div>
				
				<div class="col-md-3">
					<h6 class="m-0 font-weight-bold color-b">Enviado el: <br><b style="color: #000;" class="momento"><?php echo $F['fec_reg_rec'];?></b></h6>
				</div>
				<div class="col-md-3">
					<h6 class="m-0 font-weight-bold color-b">Departamento: <br><b style="color: #000;" class=""><?php echo $F['nom_dep'];?></b></h6>
				</div>
			</div>
			<hr>
			
			<p style="text-align:justify;color:#000;">
				<b class="color-b">Comentario: </b><?php echo nl2br($F['mensaje']);?>
			</p>
		</div>

	<div class="card-body">
	  <?php
		$reclamo = new Reclamo();
		$lr = $reclamo->listarComentarios($F['idr'], $_SESSION['idu']);
		while($fr = $lr->fetch_assoc()){
	  ?>
		<?php
		  if($fr['id_admin']==null){
			$usu = new Cliente();
			$FUSU = $usu->findById($fr['id_usuario']);
		?>

		<div class="row">
			<div class="col-md-2 text-center">
				<img src="static/img/user.png" style="width: 50px;">
				<div><b class="text-center">Yo</b></div>
				<div class="text-center color-b"><span class="momento_amigable" title="<?php echo $fr['fec_com'];?>"><?php echo $fr['fec_com'];?></span></div>
				</div>

				<div class="col-md-6">
					<p class="globo" style="text-align: justify;"><?php echo nl2br($fr['comentario']);?></p>
				</div>
			</div> 
		<hr>

		<?php
		  }else{
			$usu = new Admin();
			$FUSU = $usu->findById($fr['id_admin']);
		?>

		<div class="row">
			<div class="col-md-6 offset-md-4 col-sm-10">
				<p class="globo-admin" style="text-align: justify;"><?php echo nl2br($fr['comentario']);?></p>
				</div>
				<div class="col-md-2 col-md-2 text-center">
				<img src="static/img/user.png" style="width: 50px;">
				<div><b class="text-center"><?php echo $FUSU['usuario'];?></b></div>
				<div class="text-center color-b"><span class="momento_amigable" title="<?php echo $fr['fec_com'];?>"><?php echo $fr['fec_com'];?></span></div>
				</div>
			</div> 

		<hr>
	  <?php 
		  }
		}
        if($lr->num_rows == 0) echo '<b>No hay comentarios.</b>';
	  ?>
	</div>
  </div>
</div>

<div id="md-slider" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header">
			<h3 class="title-d" id="titulo_modulo">Agregar un nuevo comentario</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">


				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
		  <?php if(isset($F)) echo "<input type='hidden' name='idr' value='".$F['idr']."'>";?>
					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Modelo">Comentario</label>
				<textarea class="form-control" name="des" style="width:100%;height:200px;" required minlength="5"></Textarea>
							</div>
						</div>
					</div>
			<div class="modal-footer">
			  <button type="submit" id="bt_modulo" name="btg_comentario" class="btn btn-b"><?php echo ((isset($F))?'Guardar Cambios':'Enviar')?></button>
			</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
	  $('.timeago').livequery(function(){
		  $(this).timeago();
	  });

	  $("#bt_nueva_noticia").click(function(){
		$("#titulo_modulo").text("Nuevo comentario");
		$("#bt_modulo").attr('name', 'btg_comentario');
		$("#bt_modulo").text('Enviar');
		$("input[name='pla']").val('');
	  });
	  moment.locale('es');         // en
	  $(".momento").each(function(){
		$(this).text(moment($(this).text()).format('llll'));
	  });
	  $(".momento_amigable").each(function(){
		$(this).text("Hace " + moment($(this).text()).fromNow(true));
	  });



	});
</script>

