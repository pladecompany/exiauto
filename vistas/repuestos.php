<?php include_once("sliderservicios.php"); ?>

<section class="section-about ">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			</div>

			<div class="col-md-12 mt-3">
				<div class="row">
					<div class="col-sm-12 col-md-10 offset-md-1">
						<div class="title-box-d">
							<h3 class="title-d">Venta de repuestos y accesorios</h3>
						</div>

						<div class="row mb-5">
							<div class="col-md-4">
								<div class="row">
									<div class="card-box-c foo">
										<div class="col-xs-4 col-lg-12">
											<div class="card-header-c d-flex">
												<div class="card-box-ico">
													<span class="fa fa-car"></span>
												</div>
											</div>
										</div>
										<div class="col-xs-6 col-lg-12">
											<div class="card-body-c">
												<h4>Almacén de <a href="?op=latoneria" target="_blank"><strong>Repuestos Toyota</strong></a> más grande del país</h4>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="card-box-c foo">
									<div class="card-header-c d-flex">
										<div class="card-box-ico">
											<span class="fa fa-star"></span>
										</div>
									</div>
									<div class="card-body-c">
										<h4><strong>Calidad</strong> de productos y <a href="?op=mantenimiento" target="_blank">servicios.</a></h4>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="card-box-c foo">
									<div class="card-header-c d-flex">
										<div class="card-box-ico">
											<span class="fa fa-comments"></span>
										</div>
									</div>
									<div class="card-body-c">
										<a href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp['tlf'];?>&text=Hola me gustaria una asesoría acerca de..." target="_blank"><h4>Ofrececemos la <strong>asesoría</strong> que usted necesita.</h4></a>
									</div>
								</div>
							</div>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							En <strong>Exiauto</strong> contamos con 800 m2 que conforma el almacén de <strong>repuestos Toyota</strong>, nuestros vendedores están entrenados y calificados para ofrecer la asesoría que usted necesita a la hora de <strong><a href="#md-solicitud" class="modal-trigger">solicitar un repuesto para su vehículo</a></strong> dependiendo de tus exigencias y  la variedad de modelos de la marca Toyota, demostrando así nuestra <strong>calidad de productos y <a href="?op=mantenimiento" target="_blank">servicios.</a></strong>

							<br><br>
							Consulta con nuestro personal la disponibilidad de los repuestos que requiere tu vehículo, pulsa <a href="#md-solicitud" data-toggle="modal" class="modal-trigger clr_red">aquí.</a>
							</p>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-services box-llamado">
	<div class="col-md-12">
		<div class="container text-center">
			<h2 class="title-a color-c">¿Necesitas solicitar o preguntar por un repuesto?</h2>
			<br><br>
			<div class="animate__animated animate__pulse animate__infinite animate__slow">
				<a href="#md-solicitud" data-toggle="modal" class="color-b modal-trigger btn-linea-wh"><b>Consulta tus dudas aquí</b></a>
			</div>
		</div>
	</div>
</section>



