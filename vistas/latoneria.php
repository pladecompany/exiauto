<?php include_once("sliderservicios.php"); ?>

<section class="section-about ">
	<div class="container">
		<div class="row">
			<div class="col-md-12 mt-3">
				<div class="row">
					<div class="col-sm-12 col-md-10 offset-md-1">
						<div class="title-box-d">
							<h3 class="title-d">Enderezado, latonería y pintura</h3>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
								Un área de 4.000 m2 conforma el equipo de <strong>enderezado, latonería y pintura</strong> constituido por maquinarias, cuartos de pintura, sala de espera entre otros son espacios dedicados para atenderte, ofrecer <strong>el mejor servicio</strong> en  restaurar y reparar piezas afectadas y mejorar la estética del vehículo. 
								<br><br>
								
								</b>Nuestro equipo está entrenado y certificado por <strong>Toyota</strong>, Sikkens, 3M y Blackhawk.

								<br><br>
								También contamos con equipos de avanzada para garantizarle una alta <strong>calidad</strong> en la reparación en lo que tiene que ver con enderezado, latonería y pintura, a saber: <br><br>

								<strong> - </strong>Sistema de medición por ultrasonido marca Shark.<br>
								<strong> - </strong>Banco de enderezado Power-pro 360.<br>
								<strong> - </strong>Sistema de enderezado Korek.<br>
								<strong> - </strong>Soldadura Mig y electro punto.<br>
								<strong> - </strong>Aplicamos sólo materiales de primera marca Sikkens.<br>
								<strong> - </strong>Contamos con dos cabinas - horno.<br>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-services box-llamado">
	<div class="col-md-12">
		<div class="container text-center">
			<h2 class="title-a color-c">¿Necesitas solicitar una cita?</h2>
			<br><br>
			<div class="animate__animated animate__pulse animate__infinite animate__slow">
				<a href="#md-ingresar" data-toggle="modal" class="color-b modal-trigger btn-linea-wh"><b>Agenda una cita en línea</b></a>
			</div>
		</div>
	</div>
</section>



