<?php
  include_once("panel/modelo/Vehiculo.php");
  include_once("panel/modelo/Modelo.php");
  $veh = new Vehiculo();
  
  $id = $_GET['id'];
  $VEH = $veh->findById($id);
  if($VEH == false){
	echo "<script>window.location ='index.php';</script>";
	exit(1);
  }

  $mod = new Modelo();
  $modelo = $mod->findById($VEH['id_modelo']);
  $modelo = $modelo['modelo'];
?>

<script type="text/javascript" src="static/js/pdt360DegViewer.js"></script>

<script>
document.write('<style>h1,h3,h4{text-align:center;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}.viewer{position:relative;margin:20px 0}#pdtViewer{width:100%;}#pdtViewer img{padding:40px 20px;border:.5px solid #ddd;user-select:none;-moz-user-select:none}#dummy{display:none}.loader{width:100%;height:100%;position:absolute;background:rgba(0,0,0,.5);z-index:1;top:0}.three-bounce{text-align:center;font-size:26px;position:absolute;top:50%;left:50%}.three-bounce div{display:inline-block;width:18px;height:18px;border-radius:100%;background-color:#fff;-webkit-animation:bouncedelay 1.4s infinite ease-in-out both;animation:bouncedelay 1.4s infinite ease-in-out both}.three-bounce .one{-webkit-animation-delay:-0.32s;animation-delay:-0.32s}.three-bounce .two{-webkit-animation-delay:-0.16s;animation-delay:-0.16s}.btnDiv{text-align:center;margin:10px 0}.btnDiv button{margin:4px 8px;padding:20px;border:0;background-color:#d71a21;color:#fff;border-radius:50%;font-size:16px;position:relative;cursor:pointer}.btnDiv button:active{background-color:#d71a21}.btnDiv button:hover{box-shadow:0 0 8px 1px #1b8eff}.btnDiv button:focus{outline:0}.btnDiv button:before,.btnDiv button.pause:after,.btnDiv button.plus:after,.btnDiv button.right:after,.btnDiv button.left:after{position:absolute;content:""}.btnDiv button.play:before,.btnDiv button.left:before,.btnDiv button.right:after{border:11px solid transparent;border-left-color:inherit;left:16px;top:24%}.btnDiv button.pause:before,.btnDiv button.pause:after{left:12px;top:28%;width:6px;height:19px;background:#fff}.btnDiv button.pause:after{right:12px;left:auto}.btnDiv button.stop:before{left:11px;top:25%;width:45%;height:50%;background:#fff}.btnDiv button.plus:before{left:17px;top:27%;background:#fff;width:6px;height:20px}.btnDiv button.plus:after,.btnDiv button.minus:before,.btnDiv button.right:before,.btnDiv button.left:after{left:9px;top:44%;background:#fff;width:22px;height:6px}.btnDiv button.left:after{left:11px;width:21px}.btnDiv button.right:before{width:20px}.btnDiv button.left:before,.btnDiv button.play.leftNav:before{border-right-color:inherit;border-left-color:transparent;left:-4px}.btnDiv button.right:after{left:22px}.btnDiv button.play.leftNav:before{left:1px;top:21%}.btnDiv.navDiv{text-align:left}.btnDiv.navDiv button{position:absolute;top:50%}.btnDiv.navDiv button.rightNav{right:0}img.draggable{cursor:e-resize;width:100%;}img.keys{cursor:pointer}@keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0);transform:scale(0)}40%{transform:scale(1);-webkit-transform:scale(1)}}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0);transform:scale(0)}40%{transform:scale(1);-webkit-transform:scale(1)}}@media screen and (max-width:1030px){#pdtViewer img{width:100%}}@media screen and (max-width:992px){.hidePhone{display:none}.btnDiv{zoom:.7}}@media screen and (max-width:1250px){#pdtViewer img{padding:40px 0}#pdtViewer{width:95%}}@media screen and (max-width:767px){.fork{width:100px;position:absolute!important}h1{margin-top:60px}}</style>');
</script>


<section class="intro-single" style="background-image: url('<?php echo (($VEH['img_portada']!=null)?$VEH['img_portada']:"static/img/banner-vehiculos/corolla.jpeg");?>'); background-repeat: no-repeat;background-size: 100% 100%;">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12 left">
				<div class="title-single-box">
					<h1 class="title-single"><?php echo strtoupper($VEH['nom_veh']);?></h1>
					<a href="#md-interesa" class="btn btn-b p-2 link-a" data-toggle="modal" data-target="#md-interesa">Me interesa</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!--	
<div class="row">
	<div class="col-md-12">
		<div class="card_vehiculo">
			<div class="container">
				<div class="row text-center">
					<div class="col-xs-12 col-lg-2">
						<h3 class="color-c">Especificaciones</h3>
					</div>

					<div class="col-xs-12 col-lg-2 card_vehiculo--opc">
						<h5 class="color-c bold">Modelo</h5>
						<span class="color-c"><?php echo strtoupper($modelo);?></span>
					</div>

					<div class="col-xs-12 col-lg-2 card_vehiculo--opc">
						<h5 class="color-c bold">Año</h5>
						<span class="color-c"><?php echo strtoupper($VEH['ano_veh']);?></span>
					</div>
					
					<div class="col-xs-12 col-lg-2 card_vehiculo--opc">
						<h5 class="color-c bold">Tipo</h5>
                        <span class="color-c" ><?php echo ($VEH['imp_veh']==1)?'NORMAL':'A CONSIGNACIÓN';?></span>
					</div>
					
					<div class="col-xs-12 col-lg-2 card_vehiculo--opc">
						<h5 class="color-c bold">Transmisión</h5>
						<span class="color-c"><?php echo strtoupper($VEH['tra_veh']);?></span>
					</div>
					
					<div class="col-xs-12 col-lg-2">
						<a href='<?php echo ($VEH['doc']);?>' class="btn btn-b-n" target="__blank">Ver ficha técnica</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
-->

<div id="pdtViewer">
	<div class="container">
		<div id="car3" style="text-align: center;"></div>
	</div>
</div>

<div id="dummy"></div>

<script>
    var imgs = [];
<?php
  for($i=0;$i<20;$i++){
      if($VEH["img".($i+1)]!=null&&$VEH["img".($i+1)]!="")
          echo "imgs[$i]='".$VEH["img".($i+1)]."';";
  }

?>
  console.log(imgs);
	var path = 'static/img/img/';
	pdt360DegViewer('car3', imgs.length, path, 'png', false, false, true, true, true, false, false, imgs);
</script>

<section class="property-single nav-arrow-b">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
<!--
				<div id="property-single-carousel" class="owl-carousel owl-arrow gallery-property">
					<div class="carousel-item-b">
						<img src="<?php echo $VEH['img1'];?>" alt="" class="img-vehiculo-gl">
					</div>
					<?php if($VEH['img2'] != null) { ?>
					  <div class="carousel-item-b">
						  <img src="<?php echo $VEH['img2'];?>" alt="" class="img-vehiculo-gl">
					  </div>
					<?php }?>
					<?php if($VEH['img3'] != null) { ?>
					  <div class="carousel-item-b">
						  <img src="<?php echo $VEH['img3'];?>" alt="" class="img-vehiculo-gl">
					  </div>
					<?php }?>
					<?php if($VEH['img4'] != null) { ?>
					  <div class="carousel-item-b">
						  <img src="<?php echo $VEH['img4'];?>" alt="" class="img-vehiculo-gl">
					  </div>
					<?php }?>
					<?php if($VEH['img5'] != null) { ?>
					  <div class="carousel-item-b">
						  <img src="<?php echo $VEH['img5'];?>" alt="" class="img-vehiculo-gl">
					  </div>
					<?php }?>
				</div>
-->
				<div class="row justify-content-between">
					<div class="col-md-5 col-lg-4">
						<div class="property-summary">

							<div class="summary-list" >
								<div class="row">
									<div class="col-sm-12">
										<div class="title-box-d mb-0">
											<h3 class="title-d">Precio</h3>
										</div>
									</div>
								</div>
								<ul class="list">
									<li class="d-flex justify-content-between">
										<h5><?php if($VEH['pre_veh'] == null || $VEH['pre_veh'] == 0) echo 'A consultar';else echo $VEH['pre_veh']." USD";?></h5>
									</li>
								</ul>

								<div class="text-center">
									<a href="#md-interesa" class="btn btn-b p-2 link-a" data-toggle="modal" data-target="#md-interesa">Me interesa</a>
								</div><br>

								<div class="text-center">
									<a href='<?php echo ($VEH['doc']);?>' class="btn btn-b p-2 link-a" target="__blank">Ver ficha técnica</a>
								</div>
							</div>


							<div class="summary-list">
								<div class="row mt-4">
									<div class="col-sm-12">
										<div class="title-box-d">
											<h3 class="title-d">Especificaciones</h3>
										</div>
									</div>
								</div>

								<ul class="list">
									<li class="d-flex justify-content-between">
										<strong>Modelo:</strong>
										<span><?php echo strtoupper($modelo);?></span>
									</li>
									<li class="d-flex justify-content-between">
										<strong>Transmisión:</strong>
										<span><?php echo strtoupper($VEH['tra_veh']);?></span>
									</li>
                                    <?php if($VEH['sto_veh'] == 1) { ?> 
									<li class="d-flex justify-content-between">
										<strong>¿ Disponible en exiauto ?</strong>
										<span><?php echo ($VEH['sto_veh']==1)?'Si':'No';?></span>
									</li>
									<?php }?>
									<li class="d-flex justify-content-between">
										<strong>Año:</strong>
										<span><?php echo ($VEH['ano_veh']);?></span>
									</li>
									<li class="d-flex justify-content-between">
										<strong>Tipo:</strong>
										<span><?php echo ($VEH['imp_veh']==1)?'NORMAL':'A CONSIGNACIÓN';?></span>
									</li>
									 <?php if($VEH['doc'] != null) { ?> 
									<li class="d-flex justify-content-between">
										<strong>Documento detallado </strong>
										<span><a href='<?php echo ($VEH['doc']);?>' style="color:red;" target="__blank">Url</a></span>
									</li>
									<?php } ?>
									<br>
								</ul>
							</div>
						</div>
					</div>

					<div class="col-md-7 col-lg-7 section-md-t3">
						<div class="summary-list">
							<div class="row">
								<div class="col-sm-12">
									<div class="title-box-d">
										<h3 class="title-d">Descripción</h3>
									</div>
								</div>
							</div>
							<div class="property-description">
								<p class="description color-text-a text-justify">
								  <?php echo nl2br($VEH['des_veh']);?>
								</p>
							</div>
						</div>
						
						<div class="summary-list">
							<div class="row section-t3">
								<div class="col-sm-12">
									<div class="title-box-d">
										<h3 class="title-d">Flexibilidad</h3>
									</div>
								</div>
							</div>
							<div class="amenities-list color-text-a">
								<p class="description color-text-a text-justify">
								  <?php echo nl2br($VEH['fle_veh']);?>
								</p>
							</div>
						</div>

						<div class="summary-list">
							<div class="row section-t3">
								<div class="col-sm-12">
									<div class="title-box-d">
										<h3 class="title-d">Diseño</h3>
									</div>
								</div>
							</div>

							<div class="amenities-list color-text-a">
								<p class="description color-text-a text-justify">
								  <?php echo nl2br($VEH['dis_veh']);?>
								</p>
							</div>
						</div>

						<div class="summary-list">
							<div class="row section-t3">
								<div class="col-sm-12">
									<div class="title-box-d">
										<h3 class="title-d">Seguridad</h3>
									</div>
								</div>
							</div>
							<div class="amenities-list color-text-a">
								<p class="description color-text-a text-justify">
								  <?php echo nl2br($VEH['seg_veh']);?>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div id="md-interesa" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Me interesa</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="a consultarmodal-body" style="text-align:center;">
                <p>Para mas información comunícate con nosotros al:</p>
                <br><br>
				<h4><i class="fa fa-phone clr_red"></i> 0212-2030911</h4><br>
				
				<h4 class="icon-title"><i class="fa fa-paper-plane clr_red"></i> Escríbenos</h4>
				<h4><a href="mailto:ventas@exiauto.com?Subject=Me interesa este vehículo">ventas@exiauto.com</a></h4>
			</div>
		</div>
	</div>
</div>




<!-- Modal -->
<div class="modal fade" id="md-interesa" tabindex="-1" role="dialog" aria-labelledby="md-interesa" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="md-interesa">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
