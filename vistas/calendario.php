<div class="card shadow mb-4" style="padding-top:6em;">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b text-center">Calendario de disponibilidad del asesor <?php echo $fage['nom_age']." ".$fage['ape_age']. " (".$fage['cod_age']. ")";?></h4>
	</div>
    <div class="row">
      <div class="col-md-3 text-right">
        Leyenda: 
      </div>
      <div class="col-md-2 text-center fc-past" style="color:#fff;">
        Días pasados
      </div>
      <div class="col-md-2 text-center fc-today" style="color:#fff;">
        Día actual
      </div>
      <div class="col-md-2 text-center fc-future" style="color:#fff;">
        Días disponible
      </div>
      <div class="col-md-2 text-center" style="background:#f44336;color:#fff;">
        No disponible
      </div>
    </div>
	<div class="card-body">
      <link href= 'fullcalendar/packages/core/main.css' rel='stylesheet' />
      <link href= 'fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
      <link href= 'fullcalendar/packages/timegrid/main.css' rel='stylesheet' />
      <script src='fullcalendar/packages/core/main.js'></script>
      <script src='fullcalendar/packages/interaction/main.js'></script>
      <script src='fullcalendar/packages/daygrid/main.js'></script>
      <script src='fullcalendar/packages/timegrid/main.js'></script>
      <script src='fullcalendar/packages/core/locales-all.js'></script>
      <script src='fullcalendar/packages/rrule/main.js'></script>
      <script src='fullcalendar/packages/list/main.js'></script>
      <script>

        $(document).on('ready', function(){
          moment.locale('es');         // en
          var calendarEl = document.getElementById('calendar');
          var initialTimeZone = 'America/Caracas';

          var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
            header: {
              left: 'prev,next today',
              center: 'title',
              right: 'timeGridWeek'
            },
            dayRender: function (datex, cell) {
            },
            hiddenDays: [0,6],
            defaultView: 'timeGridWeek',
            defaultDate: '<?php echo date('Y-m-d');?>',
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectMirror: true,
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            minTime: '07:30:00',
            maxTime: '16:00:00',
            allDaySlot: false,
            defaultTimedEventDuration: '00:15:00',
            slotDuration: '00:15:00',
            select: function(arg) {
              if(moment(arg.start).isBefore(moment("2020-01-20"))){
                alert("Fecha incorrecta, las citas estaran disponibles a partir del 20 de enero del 2019");
                  calendar.unselect()
                  return false;
              }
              $("body").attr('style', 'cursor:wait');
              validarEvento(arg, function(rrr){
              $("body").attr('style', 'cursor:normal');
                if(rrr.r ==  false){
                  alert("El horario seleccionado, ya ha sido reservado");
                  calendar.unselect()
                  return false;
                }else{
                  var cita = calendar.getEventById('mi_cita');
                  if(cita!=null)
                    cita.remove();
                  var title = 'Solicitud de cita'
                  if (title) {
                    calendar.addEvent({
                      id: 'mi_cita',
                      title: title,
                      start: arg.start,
                      end: arg.end,
                      allDay: arg.allDay
                    })
                    var cita = calendar.getEventById('mi_cita');
                    $("#fec_cita").text(moment(cita.start).format('LLLL'));
                    $("#fec_env").val(moment(cita.start).format('YYYY-MM-DD H:mm'));
                    $("#md-cita").modal('show');
                    calendar.unselect()
                  }
                }
                });

            },
            selectAllow: function(selectInfo) {
                  return moment().diff(selectInfo.start) <= 0
            },
            eventRender: function( event, element, view ) {
              console.log(element);
            },
            events: [
                {
                title: 'No disponible',
                startTime: '10:45:00',
                endTime: '13:30:00',
                color: '#f44336',
                daysOfWeek: [1,2,3,4,5],
                eventOverlap: false
                },
                //{
                //title: 'No disponible',
                //startTime: '15:00:00',
                //endTime: '23:59:59',
                //color: '#ff9f89',
                //daysOfWeek: [1,2,3,4,5]
                //},
                {
                  title: 'No disponible',
                  startTime: '00:00:00',
                  endTime: '23:59:59',
                  color: '#f44336',
                  daysOfWeek: [6,0]
                },
                <?php
                  if($_GET['tipo_cita'] == 'mat_ped' && $_GET['kil_ent'] > 50000){
                ?>
                {
                title: 'Este servicio no lo realizamos en este horario',
                startTime: '13:30:00',
                endTime: '16:00:00',
                color: '#f44336',
                daysOfWeek: [1,2,3,4,5]
                },


                <?php
                  }
                ?>
                <?php
                  if($_GET['tipo_cita'] == 'rev_gen'){
                ?>
                {
                title: 'Este servicio no lo realizamos en este horario',
                startTime: '13:30:00',
                endTime: '16:00:00',
                color: '#f44336',
                daysOfWeek: [1,2,3,4,5]
                },


                <?php
                  }
                ?>
            ]
          });
          calendar.setOption('locale', 'es');
          calendar.render();

          <?php
            while($fcita = $r_citas->fetch_assoc()){
          ?>
            calendar.addEvent({
                id: 'ocupado_<?php echo $fcita['id'];?>',
                title: 'Reservado',
                start: '<?php echo $fcita['fecha'];?>',
                allDay: null,
                color: '#f44336'
            });
          <?php
            }

          ?>


          function validarEvento(evento, fn){

            var fec = moment(evento.start).format('YYYY-MM-DD H:mm')
            console.log(fec);
            $.post('panel/ajax_php.php', {modulo:'citas', tipo:'validarHorario', fec:fec, ida: <?php echo $fage['id'];?>}, function(data){
              fn(data);
            });
          }
        });

      </script>
      <style>

        #calendar {
          max-width: 90%;
          width:100%;
          margin: 0 auto;
          height:auto;
        }

        .fc-content{
          color:#fff;
        }

      </style>

      <div id='calendar'></div>
  </div>
</div>
<div id="md-cita" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <h5>Enviar solicitud de cita</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
                  <form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
                      <div class="row">
                          <input type="hidden" name="ida" value="<?php echo $_GET['age'];?>">
                          <input type="hidden" name="idv" value="<?php echo $_GET['veh'];?>">
                          <input type="hidden" name="tip" value="<?php echo $_GET['tip'];?>">
                          <input type="hidden" name="mot" value="<?php echo $_GET['des'];?>">
                          <input type="hidden" id="fec_env" name="fec" value="">
                          <input type="hidden" id="kil" name="kil" value="<?php echo $_GET['kil'];?>">
                          <input type="hidden" id="rev" name="rev" value="<?php echo $_GET['rev'];?>">
                          <div class="col-md-4 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Fecha y hora</b></label>
                              </div>
                          </div>
                          <div class="col-md-8 mb-2">
                              <div class="form-group" id="fec_cita">
                              </div>
                          </div>
                          <div class="col-md-4 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Asesor</b></label>
                              </div>
                          </div>
                          <div class="col-md-8 mb-2">
                              <div class="form-group" id="">
                                <?php echo $fage['nom_age']." ".$fage['ape_age']." (".$fage['cod_age'].")";?>
                              </div>
                          </div>
                          <div class="col-md-4 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Vehículo</b></label>
                              </div>
                          </div>
                          <div class="col-md-8 mb-2">
                              <div class="form-group" id="">
                                <?php echo $fveh['placa']." - ".$fveh['serial']." (".$fveh['ano'].") - ".$fveh['modelo'];?>
                              </div>
                          </div>
                          <div class="col-md-12 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Motivo de la cita</b></label>
                              </div>
                          </div>
                          <div class="col-md-12 mb-2">
                              <div class="form-group" id="">
                                  <div id=""><?php echo $_GET['des'];?></div>
                              </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'bt_agendar')?>" class="btn btn-b pull-right"><?php echo ((isset($F))?'Guardar Cambios':'Enviar solicitud de cita')?></button>
                          </div>
                      </div>
                  </form>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
.fc-today{
  background: #2fcac8 !important;
}

.fc-future{
  background: #85de56b5;
}
.fc-past{
  background: #ff9c8e;
}
.fc-day-header{
  background: #fff !important;
}
</style>
<script>
<?php
  $dia= strftime("%A");
  if($dia =="Saturday"||$dia=="Sunday"){
  ?>
  $(document).ready(function(){
    setTimeout(function(){$(".fc-next-button").trigger('click');}, 200);
  });
  <?php
  }
?>
  $(document).on('ready', function(){
    $(".fc-title").each(function(){
      console.log($(this).html());
      var t = $(this).html();
      if(t == "Reservado")
        $(this).html("Reservado &nbsp;<img src='static/img/icon-car.jpg' style='float:right;width:35px;background:red;'>");
    });
  });
</script>
