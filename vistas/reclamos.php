<?php
  if(!isset($_SESSION['log'])){
    session_start();
    session_destroy();
    echo "<script>window.location ='index.php';</script>";
    exit(1);
  }

  include_once("panel/controlador/reclamos.php");

?>

<div class="card shadow mb-4 ml-5 mr-5" style="margin-top:5em;">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Reclamos y sugerencias</h4>
		
		<div class="text-right">
			<a href="#md-slider" data-toggle="modal" class="color-b modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Nuevo </b></a>
		</div>
	</div>

	<div class="card-body">
      <?php include_once("mensajes.php");?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Enviado</th>
						<th>Tipo</th>
						<th>Descripción</th>
						<th>Estatus</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
	              <?php
	                $noti = new Reclamo();
	                $r = $noti->misReclamos($_SESSION['idu']);
	                $i=0;
	                while($ff = $r->fetch_assoc()){
	                  $i++;
	                  echo "<tr>";
	                  echo "  <td>" . $i . "</td>";
	                  echo "  <td class='momento'>" . $ff['fec_reg_rec'] . "</td>";
	                  echo "  <td>" . (($ff['tipo']=='R')?'Reclamo':'Sugerencia') . "</td>";
	                  echo "  <td>" . $ff['mensaje'] . "</td>";
	                  echo "  <td>" . (($ff['est_rec']==0)?'Enviado':'Procesado') . "</td>";
	                  echo "<td><a href='?op=ver_reclamo&id=".$ff['idr']."' onclick=''>Ver detalles <i class='mr-2 fa fa-eye'></i></a>";
	                  echo "<a href='?op=reclamos&el=".$ff['idr']."' onclick='return confirm(\"¿ Esta seguro ?\")'><i class='mr-2 fa fa-trash'></i></a>";
	                  echo "</td>";
	                  echo "</tr>";
	                }
	              ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="md-slider" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header">
            <?php if(isset($F)){ ?>
            <h3 class="title-d" id="titulo_modulo">Editar vehículo</h3>
            <?php }else{?>
            <h3 class="title-d" id="titulo_modulo">Nuevo reclamo ó sugerencia</h3>
            <?php }?>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">


				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
                    <?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Modelo">Tipo</label>
                                <select name="tip" class="form-control" required>
                                    <option value="">Seleccione</option>
                                    <option>Reclamo</option>
                                    <option>Sugerencia</option>
                                </Select>
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Modelo">Seleccione departamento</label>
                                <select name="dep" class="form-control" required>
                                    <option value="">Seleccione</option>
                                <?php
                                  $mod = new Reclamo();
                                  $r = $mod->listarDepartamentos();
                                  while($fm = $r->fetch_assoc()){
                                ?>
                                    <option value="<?php echo $fm['id'];?>" ><?php echo strtoupper($fm['nom_dep']);?></option>
                                <?php
                                  }
                                ?>
						        </select>
                          </div>
                        </div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Modelo">Describa de forma detallada su reclamo ó sugerencia</label>
                                <textarea class="form-control" name="des" style="width:100%;height:200px;" required minlength="10"></Textarea>
							</div>
						</div>

					</div>
                    <div class="modal-footer">
                        <button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-b"><?php echo ((isset($F))?'Guardar Cambios':'Enviar')?></button>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
  if(isset($F)){
?>
  <script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").trigger('click');
    });
  </script>

<?php
  } 
?>

<script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nuevo reclamo ó sugerencia");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Enviar');
        $("input[name='pla']").val('');
      });

      moment.locale('es');         // en
      $(".momento").each(function(){
        $(this).text(moment($(this).text()).format('llll'));
      });

    });

</script>

