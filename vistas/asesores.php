
	<section class="section-news section-t8">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title-wrap d-flex justify-content-between">
						<div class="title-box">
							<h2 class="title-a">Nuestros asesores</h2>
						</div>
					</div>
				</div>
			</div>

			<div id="new-carousel" class="owl-carousel owl-theme">
				<div class="carousel-item-c">
					<div class="card-box-d">
						<div class="card-img-d">
							<img src="static/img/agentes/agent-2.jpg" alt="" class="img-d img-fluid">
						</div>
						<div class="card-overlay card-overlay-hover">
							<div class="card-header-d">
								<div class="card-title-d align-self-center">
									<h3 class="title-d">
										<a href="#" class="link-a" style="font-size: 25px;">Omar Bermúdez</a>
									</h3>
								</div>
							</div>
							<div class="card-body-d">
								<div class="info-agents color-a">
									<p class="link-a"><strong>Teléfono directo: </strong> 20.30.891</p>
									<p class="link-a"><strong>Correo: </strong> obermudez@exiauto.com</p>
								</div>
							</div>

							<div class="card-footer-d">
								<div class="socials-footer d-flex justify-content-center">
									<ul class="list-inline">
										<li class="list-inline-item">
											<a href="#" class="link-one">
												<i class="fa fa-facebook" aria-hidden="true"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a href="#" class="link-one">
												<i class="fa fa-instagram" aria-hidden="true"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="carousel-item-c">
					<div class="card-box-d">
						<div class="card-img-d">
							<img src="static/img/agentes/agent-1.jpg" alt="" class="img-d img-fluid">
						</div>
						<div class="card-overlay card-overlay-hover">
							<div class="card-header-d">
								<div class="card-title-d align-self-center">
									<h3 class="title-d">
										<a href="#" class="link-a" style="font-size: 25px;">Gilberto Angel</a>
									</h3>
								</div>
							</div>

							<div class="card-body-d">
								<div class="info-agents color-a">
									<p class="link-a"><strong>Teléfono directo: </strong> 20.30.891</p>
									<p class="link-a"><strong>Correo: </strong> gangel@exiauto.com</p>
								</div>
							</div>

							<div class="card-footer-d">
								<div class="socials-footer d-flex justify-content-center">
									<ul class="list-inline">
										<li class="list-inline-item">
											<a href="#" class="link-one">
												<i class="fa fa-facebook" aria-hidden="true"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a href="#" class="link-one">
												<i class="fa fa-instagram" aria-hidden="true"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="carousel-item-c">
					<div class="card-box-d">
						<div class="card-img-d">
							<img src="static/img/agentes/agent-3.jpg" alt="" class="img-d img-fluid">
						</div>
						<div class="card-overlay card-overlay-hover">
							<div class="card-header-d">
								<div class="card-title-d align-self-center">
									<h3 class="title-d">
										<a href="#" class="link-a" style="font-size: 25px;">Luis Delgado:</a>
									</h3>
								</div>
							</div>

							<div class="card-body-d">
								<div class="info-agents color-a">
									<p class="link-a"><strong>Teléfono directo: </strong> 20.30.891</p>
									<p class="link-a"><strong>Correo: </strong> ldelgado@exiauto.com</p>
								</div>
							</div>

							<div class="card-footer-d">
								<div class="socials-footer d-flex justify-content-center">
									<ul class="list-inline">
										<li class="list-inline-item">
											<a href="#" class="link-one">
												<i class="fa fa-facebook" aria-hidden="true"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a href="#" class="link-one">
												<i class="fa fa-instagram" aria-hidden="true"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="carousel-item-c">
					<div class="card-box-d">
						<div class="card-img-d">
							<img src="static/img/agentes/agent-6.jpg" alt="" class="img-d img-fluid">
						</div>
						<div class="card-overlay card-overlay-hover">
							<div class="card-header-d">
								<div class="card-title-d align-self-center">
									<h3 class="title-d">
										<a href="#" class="link-a" style="font-size: 25px;">Juan Quintero</a>
									</h3>
								</div>
							</div>

							<div class="card-body-d">
								<div class="info-agents color-a">
									<p class="link-a"><strong>Teléfono directo: </strong> 20.30.891</p>
									<p class="link-a"><strong>Correo: </strong> jquintero@exiauto.com</p>
								</div>
							</div>

							<div class="card-footer-d">
								<div class="socials-footer d-flex justify-content-center">
									<ul class="list-inline">
										<li class="list-inline-item">
											<a href="#" class="link-one">
												<i class="fa fa-facebook" aria-hidden="true"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a href="#" class="link-one">
												<i class="fa fa-instagram" aria-hidden="true"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>