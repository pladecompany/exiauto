<section class="section-services " id="servicios">
	<!--<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title-wrap d-flex justify-content-between">
					<div class="title-box">
						<h2 class="title-a">Nuestros servicios</h2>
						
						<h5 class="text-justify">Nuestro Departamento de Servicio cuenta con más de 3.000 m2 dedicados exclusivamente a la atención de vehículos Toyota, con los más avanzados Equipos y Herramientas para solucionar cualquier problema referente a: Aire Acondicionado, Entonación, Rectificación de Frenos, Motores y otros. <br><br> Todo el personal técnico está calificado y certificado por Toyota Motors de Venezuela, garantizando así el mejor servicio y respuesta a la hora de reparar o diagnosticar su vehículo.</h5>
						
					</div>
				</div>
			</div>
		</div>
	</div>-->

	<div class="row box-Mantenimiento" style="margin-left: 0px !important;margin-right: 0px !important;">
		<div class="col-md-6 p-0">
			<img src="<?php echo (($IMG['img_servicio_1']!=null)?$IMG['img_servicio_1']:'static/img/mantenimiento.jpg');?>" width="100%" class="img-servicios">
		</div>

		<div class="col-md-6 pt-4">
			<div class="card-box-c foo">
				<div class="card-header-c flexCenter">
					<div class="card-box-ico">
						<span class="fa fa-cogs"></span>
					</div>
				</div>
				<div class="card-header-c flexCenter">
					<div class="card-title-c align-self-center">
						<h2 class="title-c"> Mantenimiento Express</h2>
						<h5><i class="fa fa-check"></i> Técnicos especializado.</h5>
						<h5><i class="fa fa-check"></i> Lavado incluido.</h5>
						<h5><i class="fa fa-check"></i> Equipos y herramientas adecuados.</h5>
						<h5><i class="fa fa-check"></i> Menos de una hora.</h5>
					</div>
				</div>
				<div class="card-footer-c text-center">
					<div class="animate__animated animate__pulse animate__infinite animate__slow">
						<a href="#md-ingresar" data-toggle="modal" class="color-b modal-trigger btn-linea"><b>Agenda una cita en línea</b></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row" style="margin-left: 0px !important;margin-right: 0px !important;">

		<div class="col-md-6 p-0 lp-display">
			<img src="<?php echo (($IMG['img_servicio_2']!=null)?$IMG['img_servicio_2']:'static/img/latoneria.jpg');?>" width="100%" class="img-servicios">
		</div>

		<div class="col-md-6 pt-4">
			<div class="card-box-c foo">
				<div class="card-header-c flexCenter">
					<div class="card-box-ico">
						<span class="fa fa-paint-brush"></span>
					</div>
				</div>
				<div class="card-header-c flexCenter">
					<div class="card-title-c align-self-center">
						<h2 class="title-c"> Enderezado, latonería y pintura</h2>
						<h5><i class="fa fa-check"></i> Sistema de medición por ultrasonido marca Shark. </h5>
						<h5><i class="fa fa-check"></i> Banco de enderezado Power-pro 360.</h5>
						<h5><i class="fa fa-check"></i> Sistema de enderezado Korek.</h5>
						<h5><i class="fa fa-check"></i> Soldadura Mig y electro punto.</h5>
						<h5><i class="fa fa-check"></i> Aplicamos solo materiales de primera marca Sikkens.</h5>
						<h5><i class="fa fa-check"></i> Contamos con dos cabinas- horno.</h5>
					</div>
				</div>
				<div class="card-footer-c text-center">
					<div class="animate__animated animate__pulse animate__infinite animate__slow">
						<a href="#md-ingresar" data-toggle="modal" class="color-b modal-trigger btn-linea"><b>Agenda una cita en línea</b></a>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6 p-0 mv-display">
			<img src="<?php echo (($IMG['img_servicio_2']!=null)?$IMG['img_servicio_2']:'static/img/latoneria.jpg');?>" width="100%" class="img-servicios">
		</div>
	</div>

	<div class="row box-Mantenimiento" style="margin-left: 0px !important;margin-right: 0px !important;">
		<div class="col-md-6 p-0">
			<img src="<?php echo (($IMG['img_servicio_3']!=null)?$IMG['img_servicio_3']:'static/img/repuestos.jpg');?>" width="100%" class="img-servicios">
		</div>

		<div class="col-md-6 pt-4">
			<div class="card-box-c foo">
				<div class="card-header-c flexCenter">
					<div class="card-box-ico">
						<span class="fa fa-car"></span>
					</div>
				</div>
				<div class="card-header-c flexCenter">
					<div class="card-title-c align-self-center">
						<h2 class="title-c"> Repuestos</h2>
						<h5><i class="fa fa-check"></i> Más de 80.000 renglones en repuestos</h5>
						<h5><i class="fa fa-check"></i> Repuestos originales TOYOTA</h5>
						<h5><i class="fa fa-check"></i> Equipos y herramientas adecuados</h5>
						<h5><i class="fa fa-check"></i> Asesoría garantizada</h5>
					</div>
				</div>
				<div class="card-footer-c text-center">
					<div class="animate__animated animate__pulse animate__infinite animate__slow">
						<a href="#md-solicitud" data-toggle="modal" class="color-b modal-trigger btn-linea"><b>Necesito un repuesto</b></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-services box-llamado">
	<div class="col-md-12">
		<div class="container text-center">
			<h2 class="title-a color-c">¿Deseas adquirir tu Vehículo <br> ensamblado en Venezuela o importado?</h2>
			<br><br>
			<div class="animate__animated animate__headShake animate__infinite">
				<a href="?op=vehiculos" class="color-b modal-trigger btn-linea-wh"><b>Estoy interesado</b></a>
			</div>
		</div>
	</div>
</section>
