<?php
	if(!isset($_SESSION['log'])){
		session_start();
		session_destroy();
		echo "<script>window.location ='index.php';</script>";
		exit(1);
	}

	include_once("panel/controlador/miscitas.php");
	include_once("panel/controlador/misvehiculos.php");
	include_once("panel/modelo/Cliente.php");

?>

<?php if(!isset($_GET['calendario'])) { ?>
<div class="card shadow mb-4 ml-5 mr-5 mt-5" style="margin-top:10em !important;">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Mis citas</h4>
		
		<div class="text-right">
			<a href="#md-slider" data-toggle="modal" class="color-b modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Nueva cita</b></a>
		</div>
	</div>
		
	<div class="card-body">
				<?php include_once("mensajes.php");?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Enviado</th>
						<th>Vehículo</th>
						<th>Modelo</th>
						<th>Asesor</th>
						<th>Tipo de cita</th>
						<th>Cita</th>
						<th>Estatus</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
                    <?php
                        $noti = new Cita();
                        $r = $noti->misCitas($_SESSION['idu']);
                        $i=0;
                        while($ff = $r->fetch_assoc()){
                            $i++;
                            if($ff['estatus']==1&&$ff['reprogramado']==1)
                              $st = "Reprogramada";
                            else
                            $st = $noti->estatus($ff['estatus'])["txt"];
                            echo "<tr>";
                            echo "  <td>" . $i . "</td>";
                            echo "  <td class='momento'>" . $ff['fec_env'] . "</td>";
                            echo "  <td>" . $ff['placa'] . "</td>";
                            echo "  <td>" . $ff['modelo'] . "</td>";
                            echo "  <td>" . $ff['nom_age'] . " " .$ff['ape_age']."</td>";
                            echo "  <td>" . $ff['tipo_cita'] . "</td>";
                            echo "  <td class='momento' >" . $ff['fecha'] . "</td>";
                            echo "  <td style='background: ".$noti->estatus($ff['estatus'])["color"].";color:#fff;'>" . $st . "</td>";
                            echo "<td class='text-center'>";
                            if($ff['estatus'] == 0 || $ff['estatus'] == 1){
                                echo "<a href='?op=cita&el=".$ff['idc']."' title='Cancelar' onclick='return confirm(\"¿ Esta seguro que desea cancelar la cita ?\")'><i class='mr-2 fa fa-times'></i></a>";
                            }
                            echo "<a href='#' title='Ver detalles' class='bt_detalle' id='".$ff['idc']."'><i class='mr-2 fa fa-eye'></i></a>";
                            echo "</td>";
                            echo "</tr>";
                        }
                    ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="md-slider" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document" style="max-width: 100% !important; width: 90% !important;margin-top: 5px;">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="title-d" id="titulo_modulo">Solicitar cita</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="row">
					<form class="form-a" method="GET" action="" enctype="multipart/form-data" id="">
						<div class="col-md-9 col-sm-12">
							<input type="hidden" name="op" value="cita">
							<input type="hidden" name="calendario" value="1">
							<input type="hidden" name="tipo_cita" value="">
							<input type="hidden" name="kil_ent" value="">
							<?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
							<div class="row">
								<div class="col-md-6 mb-2">
									<div class="form-group">
										<label for="Modelo">Vehículo</label>
											<select name="veh" class="form-control" required style="height:2.5rem !important;">
													<option value="">Seleccione</option>
											<?php
												$mod = new MiVehiculo();
												$r = $mod->misVehiculos($_SESSION['idu']);
												while($fm = $r->fetch_assoc()){
													if(isset($_GET['idv']) && $_GET['idv'] == $fm['idv']){
											?>
													<option value="<?php echo $fm['idv'];?>" selected>(<?php echo strtoupper($fm['placa']);?>) - <?php echo $fm['modelo'] . " - " . $fm['serial1'];?> </option>
											<?php }else{ ?>
													<option value="<?php echo $fm['idv'];?>" >(<?php echo strtoupper($fm['placa']);?>) - <?php echo $fm['modelo'] . " - " . $fm['serial1'];?> </option>
											<?php
														}
												}
											?>
										</select>
									</div>
								</div>

								<div class="col-md-6 mb-2">
									<div class="form-group">
										<label for="Tipo">Tipo de cita</label>
										<select name="tip" class="form-control" required id="tipo_cita" style="height:2.5rem !important;">
												<option value="">Seleccione</option>
												<option val="mat_ped">Mantenimiento períodico</option>
												<option val="rev_gen">Revisión general</option>
												<option val="gar_tdv">Garantía TDV (Vehículo nuevo)</option>
												<option val="gar_ser">Garantía de servicio (Retorno)</option>
										</select>
									</div>
								</div>
								
								<div class="col-md-6 mb-2" id="contenedor_revision">
									<div class="form-group">
										<label for="Modelo">Revisión</label>
											<select name="rev" class="form-control" id="revision" style="height:2.5rem !important;">
													<option value="">Seleccione</option>
											<?php
												$mod = new MiVehiculo();
												$r = $mod->fallas();
												while($fm = $r->fetch_assoc()){
											?>
													<option value="<?php echo $fm['id'];?>" ><?php echo $fm['falla'];?></option>
											<?php
												}
											?>
										</select>
									</div>
								</div>

								<div class="col-md-6 mb-2" id="contenedor_kilometraje">
									<div class="form-group">
										<label for="Modelo">Kilometraje actual</label>
										<select name="kil" class="form-control" id="kilometraje" style="height:2.5rem !important;">
                                          <option value="">Seleccione</option>
											<?php
												$mod = new MiVehiculo();
												$r = $mod->kilometrajes();
												while($fm = $r->fetch_assoc()){
											?>
													<option class="kilometros" value="<?php echo $fm['id'];?>" tip="<?php echo $fm['tipo_kil'];?>"><?php echo $fm['kilometros'];?></option>
											<?php
												}
											?>
										</select>
									</div>
								</div>

								<div class="col-md-12 mb-2">
									<div class="form-group">
										<label for="Modelo">Información adicional sobre su cita</label>
										<textarea name="des" class="form-control form-control-lg form-control-a" style="height:100px;" required></textarea>
									</div>
								</div>

								<div class="col-md-12 text-center">
									<label for="Modelo">Seleccione el asesor de trabajo</label>
									<div class="row">
									<?php
										$mod = new Agente();
										$r = $mod->fetchAll();
										while($fm = $r->fetch_assoc()){
									?>
									<div class="col-md-2 text-center">
										<div class="text-center">
											<div class="col-md-12 text-center">
												<a title="click para ver la imagén completa" href="<?php echo $fm['img_age'];?>" target="__blank" ><img src="<?php echo $fm['img_age'];?>" class="img-profile rounded-circle" style="width:60px; border: 2px solid #D71A21;"></a></div>
											<h6 style="font-size: 14px;"><b><?php echo $fm['cod_age'];?></b><br>
											<?php echo strtoupper($fm['nom_age'] . " " . $fm['ape_age']);?></h6>
											<input type="radio" name="age" value="<?php echo $fm['id'];?>" required>
											<br>
										</div>
									</div>
									<?php
										}
									?>
									</div>
								</div>


							</div>
						</div>

						<div class="col-md-3 col-sm-12">
						</div>

						<div class="modal-footer">
								<div class="col-md-8 text-center">
									<?php 
										$usu = new Cliente();
										$fu = $usu->findById($_SESSION['idu']);
									?>
									<label>Para cualquier información extra que necesitemos, te contactaremos a: <br><a href="?op=perfil" style="color:red;"><?php echo $fu['tel_usu']. " - ".$fu['cor_usu'];?></a></label>
								</div>
								<div class="col-md-4 text-center">
							<button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-b"><?php echo ((isset($F))?'Siguiente':'Siguiente')?></button>
								</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="md-cita-detalles" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Detalles de la solicitud</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="title-box-d">
					<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label for="Modelo"><b>Estatus </b></label>
								</div>
							</div>

							<div class="col-md-9">
								<div class="form-group text-center" id="estatus" style="color:#fff;">
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label for="Modelo"><b>Enviada</b></label>
								</div>
							</div>

							<div class="col-md-9">
								<div class="form-group momento2" id="enviado">
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label for="Modelo"><b>Cita</b></label>
								</div>
							</div>

							<div class="col-md-9">
								<div class="form-group momento2" id="fec_cita">
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label for="Modelo"><b>Asesor</b></label>
								</div>
							</div>

							<div class="col-md-9">
								<div class="form-group" id="agente">
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label for="Modelo"><b>Vehículo</b></label>
								</div>
							</div>
							<div class="col-md-9">
								<div class="form-group" id="vehiculo">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="Modelo"><b>Kilometros </b></label>
								</div>
							</div>

							<div class="col-md-9">
								<div class="form-group" id="kilometros">
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label for="Modelo"><b>Falla </b></label>
								</div>
							</div>
							<div class="col-md-9">
								<div class="form-group" id="falla">
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="Modelo"><b>Tipo y motivo de la cita</b></label>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group" id="motivo">
								</div>
							</div>

							<div class="col-md-12 " style="" id="conte_observacion">
								<label for="Modelo"><b>Observación enviada por la administración.</b></label>
								<div class="form-group">
									<div class="form-group" id="observacion">
								</div>
							</div>

							<div class="col-md-12 mb-2">
								<div style="display:none;" class="text-center alert alert-info" id="mensajes"></div>
								<div style="display:none;" class="img_cargando" id="img_cargando"><img src="static/img/cargando.gif" style="width:50px;"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



<?php }

	if(isset($_GET['calendario'])){
		include_once("calendario.php");
	}
?>


<?php
	if(isset($_GET['solicitar'])){
?>
	<script>
		$(document).ready(function(){
			$("#bt_nueva_noticia").trigger('click');
		});
	</script>

<?php
	} 
?>
<script>
		$(document).ready(function(){
          $(".kilometros").hide();

          $("#kilometraje").change(function(){
            var kll = $("#kilometraje option:selected").text();
            if(kll != ""){
              kll = kll.substring(0, kll.indexOf(" "));
              kll = kll.replace(".", "");
              $("input[name='kil_ent']").val(kll);
            }
          });
          $("#tipo_cita").change(function(){
              $(".kilometros").hide();
              var val = $("#tipo_cita option:selected").attr('val');
              $("input[name='tipo_cita']").val(val);
              $("#contenedor_kilometraje").show();
              $("#contenedor_revision").show();
              $(".kilo").remove();

            if(val == "mat_ped"){
              $("#contenedor_revision").hide();
              $(".kilometros").each(function(index){
                var tip = $(this).attr('tip');
                if(tip == 'per')
                  $(this).show();
              });
            }else if(val == "rev_gen"){
              $("#contenedor_kilometraje").hide();
            }else if(val == "gar_tdv"){
              $("#contenedor_revision").hide();
              $(".kilometros").each(function(index){
                var tip = $(this).attr('tip');
                if(tip == 'gar')
                  $(this).show();
              });
            }else if(val == "gar_ser"){
              $("#contenedor_kilometraje").hide();
            }
          });

			$("#bt_nueva_noticia").click(function(){
				$("#titulo_modulo").text("Agendar cita");
				$("#bt_modulo").attr('name', 'btg');
				$("#bt_modulo").text('Siguiente');
				$("input[name='pla']").val('');
				$("input[name='ser1']").val('');
				$("input[name='ser2']").val('');
				$("input[name='ser3']").val('');
				$("input[name='ser4']").val('');
				$("input[name='ano']").val('');
				$("select[name='mod']").val('');
			});

			moment.locale('es');         // en
			$(".momento").each(function(){
				$(this).text(moment($(this).text()).format('llll'));
			});

			<?php if(isset($_GET['nuevacita'])){
			?>
				$("#md-slider").modal("show");
			<?php
			}
			?>
			$(document).on('click', '.bt_detalle', function(){
				var idd = this.id;
				$("#md-cita-detalles").modal("show");
				$(".img_cargando").show();
				$.post('panel/ajax_php.php', {modulo: 'citas', tipo: 'obtenerCita', idc: idd}, function(data){
					$(".img_cargando").hide();
					if(data.r == false) {
						alert(data.msj);
						return;
					}
					var dataCita = data.cita;
							$("#estatus").text(dataCita.txt).css('background', dataCita.color);
							$("#fec_cita").text(dataCita.fecha)
							$("#agente").text("("+dataCita.cod_age+") " + dataCita.nom_age + " " + dataCita.ape_age);
							$("#vehiculo").text("("+dataCita.placa+") " + dataCita.serial1 + " AÑO: " + dataCita.ano + " - " + dataCita.modelo);
							$("#enviado").text(dataCita.fec_env);
							$("#kilometros").text(dataCita.kilometros);
							$("#falla").text(dataCita.falla);
							$("#observacion").text(dataCita.observacion);
							$("#motivo").text(dataCita.tipo_cita+": " + dataCita.motivo);
							$("#md-cita").modal('show');
							$("#mensajes").hide();
							$(".btop").each(function(){
								$(this).attr('id_cita', dataCita.idc);
							});
							$(".momento2").each(function(){
								$(this).text(moment($(this).text()).format('llll'));
							});
							$(".btop").show();
							if(dataCita.estatus == 0){
							}else if(dataCita.estatus == 1)
								$("#bt_aprobar").hide();
							else if(dataCita.estatus == -1)
								$("#bt_cancelar").hide();
							else if(dataCita.estatus == -2)
								$("#bt_rechazar").hide();
							else if(dataCita.estatus == 2){
								$("#bt_finalizar").hide();
								$("#bt_aprobar").hide();
							}
				});
			});
		});

</script>
