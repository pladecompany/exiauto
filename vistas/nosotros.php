<section class="section-about ">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about-img-box">
				<img src="static/img/6.jpg" alt="" class="img-fluid">
				</div>
				<div class="sinse-box">
				<h3 class="sinse-title color-c">Acerca de
					<br> Nosotros </h3>
				</div>
			</div>

			<div class="col-md-12 section-t8">
				<div class="row">
					<div class="col-sm-12 col-md-10 offset-md-1">
						<div class="title-box-d">
						<h3 class="title-d">EXIAUTO
							<span class="color-b">servicio y tecnología</span> del nuevo
							<br> milenio.</h3>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							Corporación EXIAUTO forma parte de una reducida élite del 0.8% de los concesionarios Toyota en todo el mundo que han obtenido el "World Best Selection of Toyota Dealer Facilities", otorgado por Toyota Motor Corparation (TMC).
							<br><br>
							Contamos con el soporte humano, especializadas en diferentes áreas, con certificados y cursos autorizados por Toyota de Venezuela, que nos permite ofrecer la garantía, confiabilidad y seguridad del mejor y más moderno concesionario autorizado Toyota de Venezuela.
							<br><br>
							Venga, visítenos y compruebe que el mejor lugar para comprar, mejorar o actualizar su vehículo, se encuentra en aquí.
							</p>
						</div>

						<div class="post-content color-text-a mb-5">
							<div class="title-box-d">
								<h3 class="title-d">MISIÓN</h3>
							</div>
							
							<p class="post-intro text-justify">
							La misión principal de Corporación Exiauto, C.A.  Es   “ Desempeñar Ventas y trabajos con honestidad ofreciendo calidad en nuestros servicios, de forma que tengamos siempre clientes satisfechos  y  poder así hacer ganancias.”
							<br><br>
							Los clientes son  la razón central del trabajo que día a día se realiza para obtener calidad y excelencia en servicios,  se preocupa por ofrecer un servicio integral de características éticas y técnicas del más alto nivel. Está dirigida hacia un liderazgo en cuanto a la prestación de servicios postventa y asesoramiento al cliente, apoyados en los últimos adelantos tecnológicos.
							</p>
						</div>

						<div class="post-content color-text-a mb-5">
							<div class="title-box-d">
								<h3 class="title-d">VISIÓN</h3>
							</div>
							
							<p class="post-intro text-justify">
							Corporación Exiauto, C.A. visualiza ser el concesionario líder en calidad, tecnología,  infraestructura, capital humano y utilidad financiera de la red oficial de Toyota a nivel nacional. Superando las expectativas de los clientes y empleados generando fidelización.
							</p>
						</div>


						<div class="post-content color-text-a mb-5">
							<div class="title-box-d">
								<h3 class="title-d">VALORES</h3>
							</div>
							
							<p class="post-intro text-justify">
								<i class="fa fa-check-circle"></i> Proveer en todo momento un servicio de óptima calidad  y excelencia a los clientes.<br><br>
								<i class="fa fa-check-circle"></i> Trabajo de equipo coordinado, comprometido en el cumplimiento de la misión de la institución.<br><br>
								<i class="fa fa-check-circle"></i> Orientar el esfuerzo diario al beneficio de la comunidad, a través del fortalecimiento del desarrollo humano.<br><br>
								<i class="fa fa-check-circle"></i> Realizar el trabajo con claros preceptos morales, que conserven una verdadera ética empresarial.<br><br>
								<i class="fa fa-check-circle"></i> Contar con los mejores recursos tecnológicos, para ofrecer una atención de servicio de vanguardia tecnológica.<br><br>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

