<section class="section-about ">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about-img-box">
				<img src="static/img/5.jpg" alt="" class="img-fluid">
				</div>
				<div class="sinse-box">
				<h3 class="sinse-title color-c">CrediCompra</h3>
				</div>
			</div>

			<div class="col-md-12 section-t8">
				<div class="row">
					<div class="col-sm-12 col-md-10 offset-md-1">
						<div class="title-box-d">
						<h3 class="title-d">Credi<span class="color-b">Compra</span></h3>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							Es un producto de financiamiento de consumo a mediano plazo para adquirir servivios o productos en comercios afiliados al Banco de Venezuela.
							<br><br>

							<strong>Características:</strong>
							Dirigido a los clientes que posean tarjetas Visa y Master Card del Banco de Venezuela. Línea de crédito adicional, la cual no afecta el límite de crédito rotativo de la tarjeta de crédito.
							Crédito hasta el 75% del límite de la tarjeta de crédito.
							<br><br>

							Plazo de financiamiento desde 3 meses hasta 36 meses, seleccionado por el tarjetahabiente.
							Tasa de interés promocional 19%, por 6 meses, variable e independiente de la tasa del crédito rotativo (cuotas variables con base en los cambios de la tasa de interés).
							<br><br>
							Sin inicial y cero comisión durante el período de la promoción. 
							Antigüedad de 6 meses para los clientes Clásicos. Se exonera a los clientes Dorados y Platinum
							En caso de mora con la segunda cuota vencida, se cancela el Credicompra completo y pasa la totalidad del saldo a la TDC.
							Autorización telefónica inmediata en los comercios afiliados.
							<br><br>

							Es un producto de financiamiento de consumo a mediano plazo para adquirir servivios o productos en comercios afiliados al Banco de Venezuela.
							</p>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							<strong>Ventajas para el Cliente: </strong>
							Dirigido a los clientes que posean tarjetas Visa y Master Card del Banco de Venezuela. Línea de crédito adicional, la cual no afecta el límite de crédito rotativo de la tarjeta de crédito.
							Crédito hasta el 75% del límite de la tarjeta de crédito.
							<br><br>

							Aprobación inmediata con tan sólo una llamada telefónica. Sin inicial ni recargo. No afecta el límite de su tarjeta de crédito.
							</p>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							<strong>Requisitos:</strong>
							Ser tarjetahabiente del Banco de Venezuela/Grupo Santander. El cliente debe estar en situación normal y no puede haber estado en mora, atrasos y sobregiros en los últimos 6 meses. 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>