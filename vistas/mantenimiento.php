<?php include_once("sliderservicios.php"); ?>

<section class="section-about ">
	<div class="container">
		<div class="row">
			<div class="col-md-12 mt-3">
				<div class="row">
					<div class="col-sm-12 col-md-10 offset-md-1">
						<div class="title-box-d">
							<h3 class="title-d">Mantenimiento express</h3>
						</div>

						<div class="row mb-5">
							<div class="col-md-4">
								<div class="card-box-c foo">
									<div class="card-header-c d-flex">
										<div class="card-box-ico">
											<span class="fa fa-car"></span>
										</div>
									</div>
									<div class="card-body-c">
										<h4>Puede <strong>esperar</strong> su vehículo</h4>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="card-box-c foo">
									<div class="card-header-c d-flex">
										<div class="card-box-ico">
											<span class="fa fa-shower"></span>
										</div>
									</div>
									<div class="card-body-c">
										<h4>Incluye <strong>lavado</strong></h4>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="card-box-c foo">
									<div class="card-header-c d-flex">
										<div class="card-box-ico">
											<span class="fa fa-laptop"></span>
										</div>
									</div>
									<div class="card-body-c">
										<h4>
											<a href="#md-ingresar" data-toggle="modal" class="modal-trigger">Puede solicitar una cita <strong>online</strong></a>
										</h4>
									</div>
								</div>
							</div>
						</div>

						<div class="post-content color-text-a mb-5">
							<p class="post-intro text-justify">
							En <strong>Exiauto</strong> contarás con la asistencia a tu vehículo, un personal técnico especializado,  además tendrá el asesoramiento profesional de acuerdo a las exigencias que presenta la diversidad de modelos de la marca. <br><br>
							Con este novedoso concepto de <strong>Mantenimiento Express</strong>, usted podrá esperar su vehículo mientras realizan dicha labor en menos de una hora, <strong>con el lavado incluido</strong>. Dos técnicos estarán realizando esta función de una forma detallada y estandarizada con los equipos y herramientas adecuados para seguir el patrón de mantenimiento periódico creado por el fabricante. 

							Tips de servicio, para más información, pulsa <a href="?op=tipsdeservicio" class="clr_red link-b">Aquí.</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="section-services box-llamado">
	<div class="col-md-12">
		<div class="container text-center">
			<h2 class="title-a color-c">¿Necesitas una cita?</h2>
			<br><br>
			<div class="animate__animated animate__pulse animate__infinite animate__slow">
				<a href="#md-ingresar" data-toggle="modal" class="color-b modal-trigger btn-linea-wh"><b>Agenda una cita en línea</b></a>
			</div>
		</div>
	</div>
</section>
