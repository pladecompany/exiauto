<link rel="stylesheet" type="text/css" href="static/lib/owlcarousel/assets/docs.theme.min.css">

<?php
   include_once("panel/modelo/Vehiculo.php");
   include_once("panel/modelo/Modelo.php");
?><section class="intro-single">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-8">
				<div class="title-single-box">
					<h1 class="title-single">Vehículos</h1>

				</div>
			</div>
			<div class="col-md-12 col-lg-4">
				<nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="?op=inicio">Exiauto</a>
						</li>
						<li class="breadcrumb-item active" aria-current="page"> Vehículos </li>
					</ol>
				</nav>
			</div>
		</div>
		<div class="row">
		  <div class="col-md-12 col-lg-12">
			  <nav aria-label="breadcrumb" class="breadcrumb-box justify-content-lg-end">
				  <ul class="breadcrumb text-left" style="padding:0px;font-size:12px;">
					  <li class="breadcrumb-item">
						<b>FILTRAR POR: </b>
					  </li>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos">TODOS</a>
					  </li>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos&sto=1">EN NUESTRO CONCESIONARIO</a>
					  </li>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos&imp=0">NACIONALES</a>
					  </li>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos&imp=1">IMPORTADOS</a>
					  </li>
					  <?php
						$modelo = new Modelo();
						$rmo = $modelo->fetchAll();
						while($fmo = $rmo->fetch_assoc()){
					  ?>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos&cate=<?php echo $fmo['id'];?>"><b><?php echo strtoupper($fmo['modelo']);?></b></a>
					  </li>
					  <?php }?>
				  </ul>
			  </nav>
		  </div>
		</div>
	</div>
</section>

<!--
<section class="news-grid grid">
	<div class="container">
		<section id="demos">
			<div class="row">
				<div class="large-12 columns">
					<div class="owl-carousel owl-theme">
						<div class="item">
							<h4>1</h4>
						</div>

						<div class="item">
							<h4>2</h4>
						</div>
						
						<div class="item">
							<h4>3</h4>
						</div>
					</div>
					<h3 id="overview"></h3>
			
				
				</div>
			</div>
		</section>
		-->	

		<section id="demos">
		<div class="row">
			<?php
			$veh = new Vehiculo();
			if(isset($_GET['sto'])){
				$rv = $veh->fetchVehiculosActivosExiauto();
			}else if(isset($_GET['imp'])){
				$idm = $_GET['imp'];
				$rv = $veh->fetchVehiculosByImportado($idm);
			}else if(isset($_GET['cate'])){
				$idm = $_GET['cate'];
				$rv = $veh->fetchVehiculosByModelo($idm);
			}else{
				$rv = $veh->fetchVehiculosByImportado(0);
				echo "<div class='col-md-12'><h3>Nacionales</h3></div>";
				$OK = true;
			}

?>

<?php while($fv = $rv->fetch_assoc()){ ?>
			<div class="col-md-4">
				<div class="card-box-b card-shadow news-box">
					<div class="img-box-b">
						<img src="<?php echo $fv['img1'];?>" alt="" class="img-b img-fluid img-vehiculo">
					</div>
					<div class="card-overlay">
						<div class="card-header-b">
							<div class="card-category-b">
								<span class="category-b"><?php echo $fv['ano_veh'];?></span>
							</div>
							<div class="card-title-b">
								<h2 class="title-2">
									<a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>"><?php echo strtoupper($fv['nom_veh']);?></a>
								</h2>
							</div>
							<div class="card-date">
								<a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>" class="link-a">Ver detalles <span class="fa fa-angle-double-right"></span></a><br>
							</div>
							<?php if($fv['sto_veh'] ==1){ ?>
							<br>
							<div class="pull-right card-category-b">
								<span class="category-b" style="color:#fff;">Disponible en nuestro concesionario</span>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		  <?php }?>
<?php

			if(isset($OK)){
			  $rv = $veh->fetchVehiculosByImportado(1);
			  echo "<div class='col-md-12'><h3>Importados</h3></div>";

			while($fv = $rv->fetch_assoc()){
		  ?>
			<div class="col-md-4">
				<div class="card-box-b card-shadow news-box">
					<div class="img-box-b">
						<img src="<?php echo $fv['img1'];?>" alt="" class="img-b img-fluid img-vehiculo">
					</div>
					<div class="card-overlay">
						<div class="card-header-b">
							<div class="card-category-b">
								<span class="category-b"><?php echo $fv['ano_veh'];?></span>
							</div>
							<div class="card-title-b">
								<h2 class="title-2">
									<a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>"><?php echo strtoupper($fv['nom_veh']);?></a>
								</h2>
							</div>
							<div class="card-date">
								<a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>" class="link-a">Ver detalles <span class="fa fa-angle-double-right"></span></a><br>
							</div>
							<?php if($fv['sto_veh'] ==1){ ?>
							<br>
							<div class="pull-right card-category-b">
								<span class="category-b" style="color:#fff;">Disponible en nuestro concesionario</span>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		  <?php
			}
			}else{
			  if($rv->num_rows == 0){
				echo '<center><b>No hay resultados para el filtro seleccionado</b></center>';
			  }
			}
		  ?>
		</div>
</section>



<script>
$(document).ready(function() {
	var owl = $('.owl-carousel');
	owl.owlCarousel({
	stagePadding: 50,
	margin: 10,
	nav: true,
	loop: true,
	responsive: {
		0: {
		items: 1
		},
		600: {
		items: 3
		},
		1000: {
		items: 5
		}
	}
	})
})
</script>
