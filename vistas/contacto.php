<section class="intro-single">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-8">
				<div class="title-single-box">
					<h1 class="title-single">Contáctanos</h1>
				</div>
			</div>

			<div class="col-md-12 col-lg-4">
				<nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="?op=inicio">Exiauto</a>
						</li>
						<li class="breadcrumb-item active" aria-current="page">
							Contacto
						</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>


<section class="contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="icon-box section-b2">
					<div class="icon-box-icon">
						<span class="ion-ios-paper-plane"></span>
					</div>
					<div class="icon-box-content table-cell">
						<div class="icon-box-title">
							<h4 class="icon-title">Escríbenos</h4>
						</div>
						<div class="icon-box-content">
							<p class="mb-1">Correo electrónico
								<span class="color-a">info@exiauto.com</span>
							</p>
							<p class="mb-1">Teléfono:
								<span class="color-a">0212-2030911</span>
							</p>
							<p class="mb-1">WhatsApp
								<span class="color-a"><a href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp['tlf'];?>&text=Hola me gustaria que me contacten, estoy interesado en obtener información." class="link-one" target="_blank">+58 412-3003550 </a></span>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="icon-box section-b2">
					<div class="icon-box-icon">
						<span class="ion-ios-pin"></span>
					</div>
					<div class="icon-box-content table-cell">
						<div class="icon-box-title">
							<h4 class="icon-title">Encuéntranos</h4>
						</div>
						<div class="icon-box-content">
							<p class="mb-1">
								Avenida Principal de Los Ruices, cruce con la 1ra. <br>
								Transversal, (Calle Bernardette). <br>
								Caracas - Venezuela. 
							</p>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="icon-box">
					<div class="icon-box-icon">
						<span class="ion-ios-redo"></span>
					</div>
					<div class="icon-box-content table-cell">
						<div class="icon-box-title">
							<h4 class="icon-title">Redes sociales</h4>
						</div>
						<div class="icon-box-content">
							<div class="socials-footer">
								<ul class="list-inline">
									<li class="list-inline-item">
										<a href="https://www.facebook.com/profile.php?id=100063734011239" target="_blank" class="link-one">
											<i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a href="https://twitter.com/Exiauto_Toyota?s=17" target="_blank" class="link-one">
											<i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
										</a>
									</li>
									<li class="list-inline-item">
										<a href="https://instagram.com/exiauto?igshid=19ik0ppx6cec5" target="_blank" class="link-one">
											<i class="fa fa-instagram fa-2x" aria-hidden="true"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-md-7">
						<form class="form-a contactForm" action="" method="post" role="form">
							<div id="sendmessage">Tu mensaje será enviado. ¡Gracias!</div>
							<div id="errormessage"></div>
							<div class="row">
								<div class="col-md-6 mb-3">
									<div class="form-group">
										<input type="text" name="name" class="form-control form-control-lg form-control-a" placeholder="Nombre" data-rule="minlen:4" data-msg="Escribe mínimo 3 carácteres">
										<div class="validation"></div>
									</div>
								</div>

								<div class="col-md-6 mb-3">
									<div class="form-group">
										<input name="email" type="email" class="form-control form-control-lg form-control-a" placeholder="Correo electrónico" data-rule="email" data-msg="Escribe un email válido">
										<div class="validation"></div>
									</div>
								</div>

								<div class="col-md-12 mb-3">
									<div class="form-group">
										<input type="url" name="subject" class="form-control form-control-lg form-control-a" placeholder="Asunto" data-rule="minlen:4" data-msg="Escribe mínimo 5 carácteres">
										<div class="validation"></div>
									</div>
								</div>
								
								<div class="col-md-12 mb-3">
									<div class="form-group">
										<textarea name="message" class="form-control" name="message" cols="45" rows="8" data-rule="required" data-msg="Escribe mínimo 3 carácteres" placeholder="Mensaje"></textarea>
										<div class="validation"></div>
									</div>
								</div>
								<div class="col-md-12 mb-4">
									<button type="submit" class="btn btn-a">Enviar mensaje</button>
								</div>
							</div>
						</form>
					</div>

					<div class="col-md-5">
						<div class="contact-map box">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3923.1685314605534!2d-66.83278092503443!3d10.487377289644357!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c2a5835bf2bfa87%3A0xb7e153c89f0d4c37!2sExiauto!5e0!3m2!1ses!2sve!4v1687882852967!5m2!1ses!2sve" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
